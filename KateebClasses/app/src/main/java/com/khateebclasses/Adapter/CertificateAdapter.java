package com.khateebclasses.Adapter;

    import androidx.recyclerview.widget.RecyclerView;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.ViewGroup;
    import android.widget.TextView;

    import com.khateebclasses.App;
    import com.khateebclasses.Fragment.DetailsFragment;
    import com.khateebclasses.Model.Certification;
    import com.khateebclasses.R;

    import java.util.ArrayList;

    import static com.khateebclasses.DashboardActivity.changeFragment;

    /**
     * Created by empiere-vaibhav on 4/7/2018.
     */

    public class CertificateAdapter extends RecyclerView.Adapter<CertificateAdapter.MyViewHolder> {

        public ArrayList<Certification> certificationArrayList;

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_abroad_study_raw, parent, false);

            return new MyViewHolder(itemView);
        }


        @Override
        public int getItemCount() {
            return certificationArrayList.size();
        }

        public CertificateAdapter(ArrayList<Certification> studyAbroadArrayList) {
            this.certificationArrayList = studyAbroadArrayList;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tv_course_title, tv_description, tv_amount;

            public MyViewHolder(View view) {
                super(view);

                tv_course_title = (TextView) view.findViewById(R.id.tv_course_title);
                tv_description = (TextView) view.findViewById(R.id.tv_description);
                tv_amount = (TextView) view.findViewById(R.id.tv_amount);
            }
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final Certification certification = certificationArrayList.get(position);
            holder.tv_course_title.setText(certification.getCourseTitle().toString());
            holder.tv_description.setText(certification.getShortDescription().toString());
            holder.tv_amount.setText("Amount : " + certification.getAmount()+"");
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    App.certification = certificationArrayList.get(position);
                    changeFragment(new DetailsFragment(), true);
                }
            });
        }

    }
