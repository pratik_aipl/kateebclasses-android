package com.khateebclasses.Adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.khateebclasses.Model.CourseHistory;
import com.khateebclasses.R;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 4/20/2018.
 */

public class CourseHistoryAdapter extends RecyclerView.Adapter<CourseHistoryAdapter.MyViewHolder> {

    public ArrayList<CourseHistory> certificationArrayList;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_abroad_study_raw, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public int getItemCount() {
        return certificationArrayList.size();
    }

    public CourseHistoryAdapter(ArrayList<CourseHistory> studyAbroadArrayList) {
        this.certificationArrayList = studyAbroadArrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_course_title, tv_description, tv_amount;
        ImageView arrow_download_button;

        public MyViewHolder(View view) {
            super(view);

            tv_course_title = (TextView) view.findViewById(R.id.tv_course_title);
            tv_description = (TextView) view.findViewById(R.id.tv_description);
            tv_amount = (TextView) view.findViewById(R.id.tv_amount);
            arrow_download_button = (ImageView) view.findViewById(R.id.arrow_download_button);
        }
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final CourseHistory certification = certificationArrayList.get(position);
        holder.tv_course_title.setText(certification.getName().toString());
        holder.tv_description.setText("TransactionID : "+certification.getTransactionID().toString());
        holder.tv_amount.setText("Amount : " + certification.getAmount()+"");
        holder.arrow_download_button.setVisibility(View.GONE);
       /* holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  App.certification = certificationArrayList.get(position);
                changeFragment(new DetailsFragment(), true);
            }
        });*/
    }

}

