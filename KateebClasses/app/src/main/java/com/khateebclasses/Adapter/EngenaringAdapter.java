package com.khateebclasses.Adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.khateebclasses.App;
import com.khateebclasses.Fragment.EngDetailsFragment;
import com.khateebclasses.Model.Engenaring;
import com.khateebclasses.R;

import java.util.ArrayList;

import static com.khateebclasses.DashboardActivity.changeFragment;

public class EngenaringAdapter extends RecyclerView.Adapter<EngenaringAdapter.MyViewHolder> {
    public ArrayList<Engenaring> engClassesList;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_engg_class_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Engenaring engeneering = engClassesList.get(position);
        holder.tv_subject_name.setText(engeneering.getSubjectName());
        holder.tv_semester.setText(engeneering.getSemesterName());
        holder.tv_prof_name.setText("Professor : " + engeneering.getFacultyName());
        holder.tv_batch.setVisibility(View.GONE);
        holder.tv_batch.setText("Batch Code : " + engeneering.getBatchCode());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new EngDetailsFragment(), true);
                App.engeneering = engeneering;


            }
        });
    }

    @Override
    public int getItemCount() {
        return engClassesList.size();
    }

    public EngenaringAdapter(ArrayList<Engenaring> engClassesList) {
        this.engClassesList = engClassesList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_subject_name, tv_prof_name, tv_semester, tv_batch;

        public MyViewHolder(View view) {
            super(view);

            tv_subject_name = (TextView) view.findViewById(R.id.tv_subject_name);
            tv_prof_name = (TextView) view.findViewById(R.id.tv_prof_name);
            tv_semester = (TextView) view.findViewById(R.id.tv_semester);
            tv_batch = (TextView) view.findViewById(R.id.tv_batch);

        }

    }
}
