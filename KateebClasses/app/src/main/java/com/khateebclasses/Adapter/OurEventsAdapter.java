package com.khateebclasses.Adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.khateebclasses.App;
import com.khateebclasses.Fragment.EventDetailFragment;
import com.khateebclasses.Model.OurEvent;
import com.khateebclasses.R;
import com.khateebclasses.Utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.khateebclasses.DashboardActivity.changeFragment;

public class OurEventsAdapter extends RecyclerView.Adapter<OurEventsAdapter.MyViewHolder> {

    public ArrayList<OurEvent> ourEventArrayList;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_over_events_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final OurEvent ourEvent = ourEventArrayList.get(position);
        holder.tv_title.setText(ourEvent.getEventName().toString());
        holder.tv_time.setText(ourEvent.getEDate().toString() + " " + ourEvent.getEventTime());
        holder.tv_address.setText(ourEvent.getEventAddress().toString());
        Picasso.get()
                .load(Constant.DISPLAY_IMAGE_PREFIX + ourEvent.getDisplayImage())
                .error(R.drawable.java_demo)
                .into(holder.img_events);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.eventObj = ourEventArrayList.get(position);
                changeFragment(new EventDetailFragment(), true);
            }
        });


    }

    @Override
    public int getItemCount() {
        return ourEventArrayList.size();
    }

    public OurEventsAdapter(ArrayList<OurEvent> ourEventArrayList) {
        this.ourEventArrayList = ourEventArrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_title, tv_time, tv_address;
        public ImageView img_events;

        public MyViewHolder(View view) {
            super(view);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_time = (TextView) view.findViewById(R.id.tv_time);
            tv_address = (TextView) view.findViewById(R.id.tv_address);
            img_events = view.findViewById(R.id.img_events);

        }

    }
}

