package com.khateebclasses.Adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.khateebclasses.Fragment.QuestionFragment;

import java.util.ArrayList;

public class QuestionAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    public ArrayList<QuestionFragment> mFragments;

    public QuestionAdapter(FragmentManager fm, ArrayList<QuestionFragment> mFragments) {
        super(fm);
        this.mFragments = mFragments;
    }

    @Override
    public Fragment getItem(int position) {
        // EvalQuestionFragment fragment = new EvalQuestionFragment();
        return mFragments.get(position);
    }


    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(position + 1);
    }
}

