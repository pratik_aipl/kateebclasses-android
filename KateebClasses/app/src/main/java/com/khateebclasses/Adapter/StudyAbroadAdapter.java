package com.khateebclasses.Adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.khateebclasses.Model.Certification;
import com.khateebclasses.R;
import com.khateebclasses.App;
import com.khateebclasses.Fragment.DetailsFragment;

import java.util.ArrayList;

import static com.khateebclasses.DashboardActivity.changeFragment;

/**
 * Created by empiere-vaibhav on 4/7/2018.
 */

public class StudyAbroadAdapter extends RecyclerView.Adapter<StudyAbroadAdapter.MyViewHolder> {

    public ArrayList<Certification> studyAbroadArrayList;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_abroad_study_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Certification studyAbroad = studyAbroadArrayList.get(position);
        holder.tv_course_title.setText(studyAbroad.getCourseTitle().toString());
        holder.tv_description.setText(studyAbroad.getShortDescription().toString());
        holder.tv_amount.setText("Amount : " + studyAbroad.getAmount()+"");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new DetailsFragment(), true);
                App.certification = studyAbroad;
            }
        });


    }

    @Override
    public int getItemCount() {
        return studyAbroadArrayList.size();
    }

    public StudyAbroadAdapter(ArrayList<Certification> studyAbroadArrayList) {
        this.studyAbroadArrayList = studyAbroadArrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_course_title, tv_description, tv_amount;

        public MyViewHolder(View view) {
            super(view);

            tv_course_title = (TextView) view.findViewById(R.id.tv_course_title);
            tv_description = (TextView) view.findViewById(R.id.tv_description);
            tv_amount = (TextView) view.findViewById(R.id.tv_amount);
        }
    }

}