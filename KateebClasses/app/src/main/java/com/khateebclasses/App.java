package com.khateebclasses;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.security.NetworkSecurityPolicy;
import android.util.Log;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.khateebclasses.Fragment.HomeFragment;
import com.khateebclasses.Interface.NotificationTaskListner;
import com.khateebclasses.Model.Certification;
import com.khateebclasses.Model.Engenaring;
import com.khateebclasses.Model.MainUser;
import com.khateebclasses.Model.McqTestMain;
import com.khateebclasses.Model.Mycart;
import com.khateebclasses.Model.OurEvent;
import com.khateebclasses.Model.StudyAbroad;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;

import java.util.ArrayList;

import static com.khateebclasses.Fragment.HomeFragment.AnnouncementCount;
import static com.khateebclasses.Fragment.HomeFragment.EventCount;


/**
 * Created by admin on 10/12/2016.
 */
public class App extends Application {

    public static App instance;
    public static MainUser user;
    public static String IS_LOGGED_IN = "islogedin";
    public SharedPreferences sharedPref;
    public static StudyAbroad studyAbroad;
    public static Certification certification;
    public static boolean IS_LOGGED = false;
    public static String LOG_FILE_NAME = null, EXAM_DATA_STORAGE = null;
    public static ArrayList<McqTestMain> TestMainArray = new ArrayList<>();
    public static ArrayList<Mycart> MycartArray = new ArrayList<>();
    public static String StudentExamHdrID = "";
    public static Engenaring engeneering;
    public static OurEvent eventObj;
    public static String WebviewURL = "https://khateebclasses.com";
    public static String WebviewTitle = "";
    public static String WebviewType = "";
    public static int notiCount;
    public NotificationTaskListner notificationTaskListner;

    @Override
    public void onCreate() {
        super.onCreate();
        // Fabric.with(this, new Crashlytics());

        try {
            Fabric fabric = new Fabric.Builder(this).debuggable(true).kits(new Crashlytics()).build();
            Fabric.with(fabric);


        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            instance = this;
            // notificationTaskListner = (NotificationTaskListner) this;

            sharedPref = getSharedPreferences(getPackageName(), 0);
            if (sharedPref.contains("user")) {
                Gson gson = new Gson();
                user = gson.fromJson(sharedPref.getString("user", ""), MainUser.class);
            }
            Class.forName("android.os.AsyncTask");

        } catch (ClassNotFoundException e) {
        }

        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
                    @Override
                    public void notificationOpened(OSNotificationOpenResult result) {
                        try {
                            Intent intent = new Intent(instance, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("inNoti", "1");
                            instance.startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNotificationReceivedHandler(new OneSignal.NotificationReceivedHandler() {
                    @Override
                    public void notificationReceived(OSNotification notification) {

                        App.notiCount++;
                        DashboardActivity.redCircleNoti.setVisibility(View.VISIBLE);
                        DashboardActivity.countTextViewNoti.setText(String.valueOf(App.notiCount));
                        JSONObject additionalData = new JSONObject();
                        try {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject = notification.toJSONObject();
                            Log.i("TAG", "Objh :-> " + jsonObject.toString());
                            JSONObject jpayload = jsonObject.getJSONObject("payload");
                            additionalData = jpayload.getJSONObject("additionalData");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (additionalData.getString("type").equalsIgnoreCase("Event")) {
                                EventCount++;
                                if (EventCount != 0 && HomeFragment.tv_event_count != null) {
                                    HomeFragment.tv_event_count.setText(EventCount + "");
                                    HomeFragment.frame_event_count.setVisibility(View.VISIBLE);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (additionalData.getString("type").equalsIgnoreCase("Announcement")) {
                                AnnouncementCount++;
                                if (AnnouncementCount != 0 && HomeFragment.tv_announc_count != null) {
                                    HomeFragment.tv_announc_count.setText(AnnouncementCount + "");
                                    HomeFragment.frame_announc_count.setVisibility(View.VISIBLE);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                })

                .

                        inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .

                        init();

    }


    public App() {
        setInstance(this);
    }

    public static void setInstance(App instance) {
        App.instance = instance;
    }

    public static App getInstance() {
        return instance;
    }
}
