package com.khateebclasses;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.khateebclasses.Interface.AsynchTaskListner;
import com.khateebclasses.Model.Certification;
import com.khateebclasses.Model.Mycart;
import com.khateebclasses.Model.PaymentDetail;
import com.khateebclasses.Model.PaymentMain;
import com.khateebclasses.Utils.CallRequests;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.PermissionUtils;
import com.khateebclasses.Utils.Utils;
import com.khateebclasses.ccavenue.base.activities.BaseCCAvenueActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import io.fabric.sdk.android.Fabric;

public class ConfirmOrederActivity extends BaseCCAvenueActivity implements PermissionUtils.Callback, AsynchTaskListner {
    double Total = 0;
    public TextView tv_amount;
    public ArrayList<PaymentMain> paymentMainArrayList = new ArrayList<>();
    ArrayList<Certification> CaertArray;
    public TextView tv_empty;
    private LinearLayout emptyView;
    public SharedPreferences shared;
    public MycartAdapter adapter;
    public App app;
    public RecyclerView rcyclerView;
    public Button btn_paynow;
    public String tracking_id = "", OrderID = "";
    public ConfirmOrederActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_confirm_oreder);
        Utils.logUser();
        instance = this;
        shared = getSharedPreferences(getPackageName(), 0);
        app = App.getInstance();
        rcyclerView = findViewById(R.id.rcyclerView);
        btn_paynow = findViewById(R.id.btn_paynow);
        tv_amount = findViewById(R.id.tv_amount);
        emptyView = findViewById(R.id.empty_view);
        tv_empty = findViewById(R.id.tv_empty);


        try {
            for (int i = 0; i < App.MycartArray.size(); i++) {
                Total += App.MycartArray.get(i).getAmount();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        tv_amount.setText("Rs. " + Total + "");


        if (App.MycartArray != null) {
            adapter = new MycartAdapter(CaertArray);
            final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);

            rcyclerView.setLayoutAnimation(controller);

            rcyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
            rcyclerView.setItemAnimator(new DefaultItemAnimator());
            rcyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }

        btn_paynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OrderID = String.valueOf(new Random().nextInt(10000) + App.user.getStudentID());
                startPayment(OrderID, String.valueOf(Total));

                // SubmitData();
            }
        });
    }

    public void SubmitData() {

        PaymentMain dataObj = new PaymentMain();

        if (app.IS_LOGGED) {

            dataObj.setStudent_id(String.valueOf(app.user.getStudentID()));
            dataObj.setTransactionID(tracking_id);
            dataObj.setTotalAmount(Total + "");
            dataObj.setEmailID(App.user.getEmailID());
            dataObj.setOrderID(OrderID);
            dataObj.setPaymentStatus("success");
            for (int i = 0; i < app.MycartArray.size(); i++) {
                PaymentDetail hdrObj = new PaymentDetail();
                hdrObj.setAmount(String.valueOf(app.MycartArray.get(i).getAmount()));
                hdrObj.setBatchCourseID(String.valueOf(app.MycartArray.get(i).getCourseID()));
                if (app.MycartArray.get(i).getIsType() == 1) {
                    hdrObj.setBatchOrCourse("B");

                } else {
                    hdrObj.setBatchOrCourse("C");

                }
                dataObj.PaymentDtl.add(hdrObj);


            }
            paymentMainArrayList.add(dataObj);
            Gson gson = new GsonBuilder().create();
            JsonArray jCctArray = gson.toJsonTree(paymentMainArrayList).getAsJsonArray();
            String jsonString = String.valueOf(jCctArray);
            String jSonLog = jsonString.substring(1, jsonString.length() - 1);
            longInfo(jSonLog, "MCQ ::-->");
            if (app.user.getStudentID() != 0) {


                new CallRequests(instance).make_payment(jSonLog);


            }

        } else {


            new AlertDialog.Builder(instance)
                    .setTitle("Login?")
                    .setMessage("You want first login")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            Intent i = new Intent(instance, LoginActivity.class);
                            startActivity(i);
                            finishAffinity();
                            //  getActivity().moveTaskToBack(true);
                        }
                    }).create().show();

        }

    }

    public static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Log.i("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else {
            Log.i("TAG " + tag + " -->", str);
        }
    }

    @Override
    public void onPaymentDeclined() {

    }

    @Override
    public void onPaymentSuccess(String transactionID) {
        this.tracking_id = transactionID;
        Log.i("TAG", "tracking_id :-> " + tracking_id);
        SubmitData();
    }

    @Override
    public void onPaymentAborted() {

    }

    @Override
    public void onPaymentAbortedByUser() {

    }

    @Override
    public void onPaymentUnknown() {

    }

    @Override
    public void onAllPermissionGranted() {

    }

    @Override
    public void onPermissionDenial() {

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {

                case make_payment:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getString("Status").equals("Success")) {
                            Utils.showToast("Sucess Payment", this);
                            SharedPreferences.Editor et = shared.edit();

                            App.MycartArray.clear();
                            et.putString("CART", "");
                            et.clear();
                            et.apply();
                            et.commit();
                            startActivity(new Intent(instance, PaymentStatusActivity.class));
                            finish();
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    public class MycartAdapter extends RecyclerView.Adapter<MycartAdapter.MyViewHolder> {

        public ArrayList<Certification> featuredJobList;
        public Date date1, date2;
        Context context;

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_confirm_order_raw, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {

            final Mycart certification = App.MycartArray.get(position);
            holder.tv_title.setText(certification.getCourseTitle());
            holder.tv_price.setText("Rs. " + certification.getAmount());


        }

        @Override
        public int getItemCount() {
            return App.MycartArray.size();
        }

        public MycartAdapter(ArrayList<Certification> featuredJobList) {
            this.featuredJobList = featuredJobList;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tv_title, tv_price, tv_semester;

            public MyViewHolder(View view) {
                super(view);

                tv_title = (TextView) view.findViewById(R.id.tv_title);
                tv_price = (TextView) view.findViewById(R.id.tv_price);
                tv_semester = (TextView) view.findViewById(R.id.tv_semester);


            }


        }

    }
}
