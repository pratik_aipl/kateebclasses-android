package com.khateebclasses;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.khateebclasses.Fragment.AboutusFragment;
import com.khateebclasses.Fragment.AnnounceFragment;
import com.khateebclasses.Fragment.ContectUsFragment;
import com.khateebclasses.Fragment.CourseHistoryFragment;
import com.khateebclasses.Fragment.HomeFragment;
import com.khateebclasses.Fragment.MyCartFragment;
import com.khateebclasses.Fragment.NotificationFragment;
import com.khateebclasses.Fragment.OverEventsFragment;
import com.khateebclasses.Fragment.termsFragment;
import com.khateebclasses.Interface.AsynchTaskListner;
import com.khateebclasses.Model.MainUser;
import com.khateebclasses.Model.Notification;
import com.khateebclasses.Utils.BlurView;
import com.khateebclasses.Utils.CallRequests;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.JsonParserUniversal;
import com.khateebclasses.Utils.MyDBManagerOneSignal;
import com.khateebclasses.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class DashboardActivity extends AppCompatActivity implements AsynchTaskListner {

    public static DashboardActivity instance;
    public static DrawerLayout drawer;
    public static FragmentManager manager;
    public static AppCompatActivity activity;
    public static Toolbar toolbar;
    public static ActionBar actionBar;
    private NavigationView navigationView;
    public View navHeader;
    public EditText et_oldpass, et_newPass, et_confirm_pass, et_firstname, et_lastname, et_email, et_mobile;
    public ImageView img_old, img_newPass, img_confirm, passwor_dilog_close, profile_dilog_close;
    public static ActionBarDrawerToggle actionBarDrawerToggle;
    public RelativeLayout rel_contectus, rel_announs, rel_profile, rel_course, rel_event, rel_aboutus, rel_change_pass, rel_terms, rel_privacy, img_logout, img_close;
    public FrameLayout redCircle;
    public static FrameLayout redCircleNoti;
    public TextView countTextView;
    public static TextView countTextViewNoti;
    public TextView tv_changePass;
    public TextView tv_update;
    public TextView tv_email;
    public TextView tv_name;
    public static TextView tv_logo, tv_title;
    public SharedPreferences shared;
    public JsonParserUniversal jParser;
    public App app;
    public MainUser user;
    public static MyDBManagerOneSignal mDbOneSignal;
    public String inNoti = "";


    public ArrayList<Notification> notiArray = new ArrayList<>();
    public Notification notiObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_dashboard);
        Utils.logUser();

        mDbOneSignal = MyDBManagerOneSignal.getInstance(this);
        mDbOneSignal.open(this);
        getNotificationCount();
        FindEliments();

        instance = this;
        app = App.getInstance();
        jParser = new JsonParserUniversal();
        shared = getSharedPreferences(getPackageName(), 0);
        try {
            inNoti = getIntent().getExtras().getString("inNoti");
        } catch (Exception e) {
            e.printStackTrace();
        }

        activity = DashboardActivity.this;
        actionBar = getSupportActionBar();
        setSupportActionBar(toolbar);
        manager = getSupportFragmentManager();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        navHeader = navigationView.getHeaderView(0);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(drawerView.getWindowToken(), 0);
            }

        };
        drawer.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        changeFragment(new HomeFragment(), false);

        DrawerClickEvent();


    }

    public static void getNotificationCount() {
        Cursor c = mDbOneSignal.getAllRows("Select * from notification where opened=0 ifnull(collapse_id, '') = ''");
       if (c != null && c.moveToFirst()) {
            App.notiCount = c.getCount();
            Log.i("TAG", "In IF :-> " + App.notiCount);
            c.close();
        }


    }

    public void FindEliments() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nvView);
        rel_profile = findViewById(R.id.rel_profile);
        rel_course = findViewById(R.id.rel_course);
        rel_event = findViewById(R.id.rel_event);
        rel_aboutus = findViewById(R.id.rel_aboutus);
        rel_change_pass = findViewById(R.id.rel_change_pass);
        rel_terms = findViewById(R.id.rel_terms);
        rel_privacy = findViewById(R.id.rel_privacy);
        rel_contectus = findViewById(R.id.rel_contectus);
        rel_announs = findViewById(R.id.rel_announs);
        rel_privacy.setVisibility(View.GONE);

        img_logout = findViewById(R.id.img_logout);
        img_close = findViewById(R.id.img_close);
        tv_name = findViewById(R.id.tv_name);
        tv_email = findViewById(R.id.tv_email);
        tv_logo = findViewById(R.id.tv_logo);
        setDataforDrawer(App.user);
    }

    private void setDataforDrawer(MainUser user) {

        if (app.IS_LOGGED) {
            tv_name.setText(user.getFirstName() + " " + user.getLastName());
            tv_email.setText(user.getEmailID());
            String first_name = user.getFirstName();
            String fn = first_name.substring(0, 1);
            String last_name = user.getLastName();
            String ln = last_name.substring(0, 1);

            tv_logo.setText(fn + ln);
        } else {
            tv_logo.setText("N/A");
            tv_name.setText("Sign In");
            tv_name.setPadding(10, 10, 10, 10);
            tv_email.setVisibility(View.GONE);
            img_logout.setVisibility(View.GONE);
            rel_change_pass.setVisibility(View.GONE);
            rel_profile.setVisibility(View.GONE);
            rel_course.setVisibility(View.GONE);
            tv_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(instance, LoginActivity.class));
                    finishAffinity();
                }
            });

        }


    }

    public void DrawerClickEvent() {
        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogout();
            }
        });

        rel_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //AlertDioalog();
                UpdateProfile();

            }
        });
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
            }
        });
        rel_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new CourseHistoryFragment(), true);
                drawer.closeDrawers();
            }
        });
        rel_aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // startActivity(new Intent(instance, AboutActivity.class));

                changeFragment(new AboutusFragment(), true);
                drawer.closeDrawers();
            }
        });
        rel_change_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePassword();
                drawer.closeDrawers();
            }
        });
        rel_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment(new termsFragment(), true);
                drawer.closeDrawers();
            }
        });
        rel_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
            }
        });
        rel_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
                changeFragment(new OverEventsFragment(), true);

            }
        });
        rel_announs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
                changeFragment(new AnnounceFragment(), true);

            }
        });
        rel_contectus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
                changeFragment(new ContectUsFragment(), true);

            }
        });

    }

    public static void setDrawer() {
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        actionBarDrawerToggle.syncState();
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
        drawer.closeDrawers();
    }

    public static void setBackButton() {
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       /* final Drawable upArrow = activity.getResources().getDrawable(R.drawable.left_arrow);
        upArrow.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        activity.getSupportActionBar().setHomeAsUpIndicator(upArrow);
       */
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                activity.onBackPressed();
            }
        });

        drawer.closeDrawers();
    }

    public static void changeFragment(Fragment fragment, boolean doAddToBackStack) {

        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.frame, fragment);
        if (doAddToBackStack) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            transaction.addToBackStack(null);
            setBackButton();

        } else {
            setDrawer();
        }

        transaction.commit();

    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(Gravity.LEFT)) {
            drawer.closeDrawers();
            return;
        }

        FragmentManager manager = getSupportFragmentManager();

        if (manager.getBackStackEntryCount() == 1) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            manager.popBackStack();
        } else if (manager.getBackStackEntryCount() > 1) {
            manager.popBackStack();
            setBackButton();

        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Exit?")
                    .setMessage("Are you sure you want to exit?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            finish();
                            moveTaskToBack(true);
                        }
                    }).create().show();
        }
    }

    public void UpdateProfile() {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.popup_profile);

        profile_dilog_close = dialog.findViewById(R.id.profile_dilog_close);
        et_firstname = dialog.findViewById(R.id.et_firstName);
        et_lastname = dialog.findViewById(R.id.et_LastName);
        et_email = dialog.findViewById(R.id.et_email);
        et_mobile = dialog.findViewById(R.id.et_mobile);
        tv_update = dialog.findViewById(R.id.tv_update);

        if (App.user != null) {
            setDataForProfile(App.user);
        }


        tv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_firstname.getText().toString().equals("")) {
                    et_firstname.setError("Please Enter First Name");
                    et_firstname.setFocusable(true);
                } else if (et_lastname.getText().toString().equals("")) {
                    et_lastname.setError("Please Enter Last Name");
                    et_lastname.setFocusable(true);
                } else if (et_mobile.getText().toString().length() != 10) {
                    et_mobile.setError("Please Enter Valid Mobile No.");
                    et_mobile.setFocusable(true);
                } else if (et_email.getText().toString().equals("") || !Utils.isValidEmail(et_email.getText().toString())) {
                    et_email.setError(" Please Enter Valid Email ID");
                    et_email.setFocusable(true);
                } else {
                    dialog.dismiss();
                    drawer.closeDrawers();
                    new CallRequests(instance).update_profile(et_firstname.getText().toString(), et_lastname.getText().toString(),
                            et_email.getText().toString(), et_mobile.getText().toString());

                }
            }
        });

        profile_dilog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                drawer.closeDrawers();
            }
        });
        Bitmap map = Utils.takeScreenShot(DashboardActivity.this);
        Bitmap fast = new BlurView().fastBlur(map, 15);


        final Drawable draw = new BitmapDrawable(getResources(), fast);
        Window window = dialog.getWindow();
        window.setBackgroundDrawable(draw);
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);
        window.setGravity(Gravity.FILL);
        dialog.show();
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


    }

    private void setDataForProfile(MainUser user) {
        et_firstname.setText(user.getFirstName().toString());
        et_lastname.setText(user.getLastName().toString());
        et_email.setText(user.getEmailID().toString());
        et_mobile.setText(user.getMobileNo().toString());
    }

    public void ChangePassword() {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.popup_change_password);

        img_old = dialog.findViewById(R.id.img_old);
        et_oldpass = dialog.findViewById(R.id.et_oldpass);
        img_newPass = dialog.findViewById(R.id.img_newPass);
        et_newPass = dialog.findViewById(R.id.et_newPass);
        img_confirm = dialog.findViewById(R.id.img_confirm);
        et_confirm_pass = dialog.findViewById(R.id.et_confirm_pass);
        tv_changePass = dialog.findViewById(R.id.tv_changePass);
        passwor_dilog_close = dialog.findViewById(R.id.passwor_dilog_close);

        shoePass(img_old, et_oldpass);
        shoePass(img_newPass, et_newPass);
        shoePass(img_confirm, et_confirm_pass);

        tv_changePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_oldpass.getText().toString().equals("")) {
                    et_oldpass.setError("Please Enter Old Password");
                    et_oldpass.setFocusable(true);
                } else if (et_newPass.getText().toString().equals("")) {
                    et_newPass.setError("Please Enter New Password");
                    et_newPass.setFocusable(true);
                } else if (et_confirm_pass.getText().toString().equals("")) {
                    et_confirm_pass.setError("Please Enter Confirm Password");
                    et_confirm_pass.setFocusable(true);
                } else if (!et_confirm_pass.getText().toString().equals(et_newPass.getText().toString())) {
                    et_confirm_pass.setError("Password And Confirm Password Not Match");
                    et_confirm_pass.setFocusable(true);
                } else {
                    dialog.dismiss();
                    new CallRequests(instance).change_password(et_oldpass.getText().toString(), et_newPass.getText().toString());
                }
            }
        });
        passwor_dilog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Bitmap map = Utils.takeScreenShot(DashboardActivity.this);
        Bitmap fast = new BlurView().fastBlur(map, 15);


        final Drawable draw = new BitmapDrawable(getResources(), fast);
        Window window = dialog.getWindow();
        window.setBackgroundDrawable(draw);
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);
        window.setGravity(Gravity.FILL);

        dialog.show();
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    }

    public void shoePass(ImageView img, final EditText et_box) {
        img.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        et_box.setInputType(InputType.TYPE_CLASS_TEXT);
                        break;
                    case MotionEvent.ACTION_UP:
                        et_box.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.toolbar_icon, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cart_custom:
                changeFragment(new MyCartFragment(), true);
                // Toast.makeText(this, "count cleared", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.noti:
                // TODO update alert menu icon
                changeFragment(new NotificationFragment(), true);
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final MenuItem alertMenuItem = menu.findItem(R.id.cart_custom);
        FrameLayout rootView = (FrameLayout) alertMenuItem.getActionView();

        redCircle = (FrameLayout) rootView.findViewById(R.id.view_alert_red_circle);
        countTextView = (TextView) rootView.findViewById(R.id.view_alert_count_textview);
        if (App.MycartArray != null && App.MycartArray.size() > 0) {
            countTextView.setText(App.MycartArray.size() + "");
        } else {
            redCircle.setVisibility(View.GONE);
        }
        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(alertMenuItem);
            }
        });
        final MenuItem noti = menu.findItem(R.id.noti);

        FrameLayout notirootView = (FrameLayout) noti.getActionView();

        redCircleNoti = (FrameLayout) notirootView.findViewById(R.id.view_alert_red_circle);
        countTextViewNoti = (TextView) notirootView.findViewById(R.id.view_alert_count_textview);
        if (App.notiCount == 0) {
            redCircleNoti.setVisibility(View.GONE);
        } else {
            countTextViewNoti.setText(String.valueOf(App.notiCount));
        }


        notirootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(noti);
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getNotificationCount();

    }

    public void doLogout() {


        new AlertDialog.Builder(this)
                .setTitle("Logout")
                .setMessage("Are you sure you want to logout?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        SharedPreferences.Editor et = shared.edit();
                        et.putString("user", "");
                        et.putBoolean(App.IS_LOGGED_IN, false);
                        app.user = null;
                        et.clear();
                        et.apply();
                        et.commit();
                        app.IS_LOGGED = false;

                        Intent i = new Intent(instance, LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }
                }).create().show();

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case change_password:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getString("Status").equals("Success")) {
                            Utils.showToast(jObj.getString("message"), instance);
                            //doLogout();
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case update_profile:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getString("Status").equals("Success")) {
                            Utils.showToast(jObj.getString("message"), instance);
                            user = new MainUser();
                            user.setStudentID(jObj.getInt("StudentID"));
                            user.setFirstName(jObj.getString("FirstName"));
                            user.setLastName(jObj.getString("LastName"));
                            user.setEmailID(jObj.getString("EmailID"));
                            user.setMobileNo(jObj.getString("MobileNo"));
                            user.setPassword(jObj.getString("Password"));
                            app.user = user;
                            SharedPreferences.Editor et = shared.edit();
                            Gson gson = new Gson();
                            String json = gson.toJson(app.user);
                            et.putString("user", json);
                            et.putBoolean(app.IS_LOGGED_IN, true);
                            et.commit();

                            tv_name.setText(user.getFirstName().toString() + " " + user.getLastName().toString());
                            tv_email.setText(user.getEmailID().toString());
                            String first_name = user.getFirstName().toString();
                            String fn = first_name.substring(0, 1);
                            String last_name = user.getLastName().toString();
                            String ln = last_name.substring(0, 1);
                            tv_logo.setText(fn + ln);


                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;


            }
        }
    }


}
