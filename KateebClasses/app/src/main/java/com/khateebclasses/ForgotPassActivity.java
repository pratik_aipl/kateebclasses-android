package com.khateebclasses;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.khateebclasses.Interface.AsynchTaskListner;
import com.khateebclasses.Utils.CallRequests;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;

public class ForgotPassActivity extends AppCompatActivity implements AsynchTaskListner {
    public ImageView img_back;
    public TextView tv_send;
    public EditText et_email;
    public ForgotPassActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_forgot_pass);
        Utils.logUser();

        instance = this;
        img_back = findViewById(R.id.img_back);
        tv_send = findViewById(R.id.tv_send);
        et_email = findViewById(R.id.et_email);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (et_email.getText().toString().equals("") || !Utils.isValidEmail(et_email.getText().toString())) {
                    et_email.setError(" Please Enter Valid Email ID");
                    et_email.setFocusable(true);
                }else {
                    new CallRequests(instance).forgot_password(et_email.getText().toString());

                }
            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case forgot_password:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getString("Status").equals("Success")) {
                            Utils.showToast("Youe Password Reset Sucessfully", instance);
                            onBackPressed();

                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
