package com.khateebclasses.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.khateebclasses.Interface.ObservableScrollViewCallbacks;
import com.khateebclasses.R;
import com.khateebclasses.Utils.ObservableScrollView;
import com.khateebclasses.Utils.ScrollState;
import com.nineoldandroids.view.ViewHelper;

import static com.khateebclasses.DashboardActivity.tv_title;

public class AboutusFragment extends Fragment implements ObservableScrollViewCallbacks {
    public View view;
    public AboutusFragment instance;
    public WebView web_about;
    public ObservableScrollView mScrollView;
    private ImageView mImageView;
    public FrameLayout frame_content;
    public int mParallaxImageHeight;
    private TextView tv_FullDesc;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.aboutus_fragment, container, false);
        instance = this;
        tv_title.setText("About us");
        web_about = view.findViewById(R.id.web_about);
        frame_content = (FrameLayout) view.findViewById(R.id.frame_content);
        tv_FullDesc = (TextView) view.findViewById(R.id.tv_FullDesc);
        mImageView = view.findViewById(R.id.image);


        mScrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        mScrollView.setScrollViewCallbacks(this);

        mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen._180sdp);
        onScrollChanged(mScrollView.getCurrentScrollY(), false, false);
        tv_FullDesc.setVisibility(View.VISIBLE);
        tv_FullDesc.setText(Html.fromHtml("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "<body>\n" +
                "\n" +
                "<h2> About Khateeb Classes </h2>\n" +
                "<p>Founded by Junaid Khateeb in 2009 , Khateeb group of classes is an established name in Engineering coaching and training. Having trained over 10,000 students ,Khateeb group has build a strong relationship and trust with its student community by delivering high quality teaching and training programmes.</p>\n" +
                "<p>'STUDENT FIRST' has always been the motto of the institute and the faculties associated with the institute. We are proud to have a strong line up of highly qualified and experienced faculties, who have delivered consistent results over the years.</p>\n" +
                "<p>Khateeb Group of Institutes has the following institutions under its umbrella</p>\n" +
                "<p>1) Khateeb Engineering CLasses:  Teaching and training Engineering students from all branches of Engineering.</p>\n" +
                "<p>2) Khateeb Institute of Technical education : Provides training for global certification courses from ORACLE / CISCO/MICROSOFT/GOOGLE etc.</p>\n" +
                "<p>3) Khateeb Academy of higher studies :Provides assistance and training for studying abroad in the countries like USA /UK/CANADA/AUSTRALIA etc.</p>\n" +
                "<p>4) junkminds.com : Provides online lectures for all Engineering subjects.</p>\n" +
                "<p>5) Edizone academy : Provides classroom teaching for VIII / IX and X std. students from ICSE/CBSE and SSC boards.</p>\n" +
                "\n" +
                "</body>\n" +
                "</html>"));
        web_about.loadUrl("file:///android_asset/about.html");


        return view;
    }


    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        ViewHelper.setTranslationY(mImageView, scrollY / 2);
    }

    @Override
    public void onDownMotionEvent() {

    }


    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }
}
