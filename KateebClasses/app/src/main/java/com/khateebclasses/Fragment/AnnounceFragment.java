package com.khateebclasses.Fragment;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khateebclasses.App;
import com.khateebclasses.Interface.AsynchTaskListner;
import com.khateebclasses.Model.Announcement;
import com.khateebclasses.Model.Notification;
import com.khateebclasses.R;
import com.khateebclasses.Utils.CallRequests;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.CursorParserUniversal;
import com.khateebclasses.Utils.JsonParserUniversal;
import com.khateebclasses.Utils.MyDBManagerOneSignal;
import com.khateebclasses.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import static com.khateebclasses.DashboardActivity.countTextViewNoti;
import static com.khateebclasses.DashboardActivity.redCircleNoti;
import static com.khateebclasses.DashboardActivity.tv_title;

public class AnnounceFragment extends Fragment implements AsynchTaskListner {

    public View view;
    public AnnounceFragment instance;
    public RecyclerView rcyclerView;
    public ArrayList<Announcement> anousArray = new ArrayList<>();
    public Announcement announcement;
    public AnnounceAdapter adapter;
    public JsonParserUniversal jParser;
    private LinearLayout emptyView;
    public TextView tv_empty;
    public static MyDBManagerOneSignal mDbOneSignal;
    public ArrayList<Notification> notiArray = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        instance = this;
        tv_title.setText("ANNOUNCEMENT");
        jParser = new JsonParserUniversal();
        mDbOneSignal = MyDBManagerOneSignal.getInstance(getActivity());
        mDbOneSignal.open(getActivity());
        rcyclerView = view.findViewById(R.id.rcyclerView);
        setHasOptionsMenu(true);

        emptyView = view.findViewById(R.id.empty_view);
        tv_empty = view.findViewById(R.id.tv_empty);
        emptyView.setVisibility(View.GONE);
        new CallRequests(AnnounceFragment.this).get_announcement();
        new getRecorrd(getActivity()).execute();

      /*  try {

            //  System.out.println("Total Rows on try");

            anousArray = (ArrayList) new CustomDatabaseQuery(getActivity(), new Notification())
                    .execute(new String[]{"Select * from notification"}).get();


        } catch (InterruptedException e) {

            e.printStackTrace();
        } catch (ExecutionException e) {

            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mDbOneSignal.dbQuery("UPDATE notification SET opened = 1");
        if (anousArray.size() > 0) {


            adapter = new AnnounceAdapter(anousArray);
            final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);

            rcyclerView.setLayoutAnimation(controller);

            rcyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            rcyclerView.setItemAnimator(new DefaultItemAnimator());
            rcyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            Utils.hideProgressDialog();
            //  new CallRequests(AnnounceFragment.this).get_announcement();
        } else {
            rcyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }*/
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.getItem(1).setVisible(false);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            switch (request) {

                case get_announcement:

                    anousArray.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("Status").equals("Success")) {
                            if (jObj.getJSONArray("records") != null && jObj.getJSONArray("records").length() > 0) {
                                JSONArray jDataArray = jObj.getJSONArray("records");
                                if (jDataArray != null && jDataArray.length() > 0) {

                                    for (int i = 0; i < jDataArray.length(); i++) {
                                        JSONObject jOurEvent = jDataArray.getJSONObject(i);
                                        announcement = (Announcement) jParser.parseJson(jOurEvent, new Announcement());

                                        anousArray.add(announcement);
                                    }

                                    adapter = new AnnounceAdapter(anousArray);
                                    final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);

                                    rcyclerView.setLayoutAnimation(controller);

                                    rcyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                    rcyclerView.setItemAnimator(new DefaultItemAnimator());
                                    rcyclerView.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                    Utils.hideProgressDialog();

                                }
                            } else {
                                Utils.hideProgressDialog();
                                rcyclerView.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                            }
                        } else {
                            Utils.hideProgressDialog();
                            rcyclerView.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }


    public class AnnounceAdapter extends RecyclerView.Adapter<AnnounceAdapter.MyViewHolder> {

        public ArrayList<Announcement> announcementArrayList;

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_announce_raw, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            final Announcement announcement = announcementArrayList.get(position);
            holder.tv_title.setText(announcement.getAnnDescription().toString());
            String date = announcement.getAnnouncementDate();
            date = date.substring(0, 10);

            holder.tv_date.setText(printDifference(announcement.getAnnouncementDate()));

           /* try {
                JSONObject jObj = new JSONObject(announcement.get());
                Log.i("TAG", "MILI SECOND :-> " + jObj.getString("google.sent_time"));

                String date = convertDate(jObj.getString("google.sent_time"), "yyyy-MM-dd HH:mm:ss");
                holder.tv_date.setText(printDifference(date));

            } catch (JSONException e) {
                e.printStackTrace();
            }*/


        }

        @Override
        public int getItemCount() {
            return announcementArrayList.size();
        }

        public AnnounceAdapter(ArrayList<Announcement> featuredJobList) {
            this.announcementArrayList = featuredJobList;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tv_title, tv_date;


            public MyViewHolder(View view) {
                super(view);

                tv_title = (TextView) view.findViewById(R.id.tv_title);
                tv_date = (TextView) view.findViewById(R.id.tv_date);


            }


        }

        public CharSequence printDifference(String start) {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            long time = 0;

            try {
                time = sdf.parse(start).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long now = System.currentTimeMillis();

            CharSequence ago =
                    DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
            return ago;
        }

        public String convertDate(String dateInMilliseconds, String dateFormat) {
            return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
        }
    }

    public class CustomDatabaseQuery extends AsyncTask<String, ArrayList<Object>, ArrayList<Object>> {

        public MyDBManagerOneSignal mDb;
        public CursorParserUniversal cParse;

        public Context ct;
        public Object obj;

        public CustomDatabaseQuery(Context ct, Object obj) {
            this.ct = ct;
            this.obj = obj;

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDb = MyDBManagerOneSignal.getInstance(ct);
            mDb.open(ct);
            this.cParse = new CursorParserUniversal();


        }

        @Override
        protected ArrayList<Object> doInBackground(String... id) {
            // Log.i("Total Rows", " : " + id[0]);
            Cursor c = mDb.getAllRows(id[0]);


            // System.out.println("Total Rows : " + id[0]);

            ArrayList<Object> objArray = new ArrayList<>();

            if (c != null && c.moveToFirst()) {
                do {

                    // System.out.println("Total Rows on onPreExecute class");

                    Class cl;
                    try {
                        cl = Class.forName(obj.getClass().getCanonicalName(), false, obj.getClass().getClassLoader());
                        obj = cl.newInstance();
                        obj = (Object) cParse.parseCursor(c, obj);

                        objArray.add(obj);

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                } while (c.moveToNext());

                // Log.v("Cursor Object", DatabaseUtils.dumpCursorToString(c));
                c.close();
            } else {

                //  Log.i("Total Rows", " : Not any SubjectAccuracy Found from");
                //  Utils.showToast("Not any SubjectAccuracy Found from : ", ct);
            }

            return objArray;

        }

        @Override
        protected void onPostExecute(ArrayList<Object> objects) {

            super.onPostExecute(objects);
        }


        public ArrayList<Object> executeOnExecutor(String[] strings) {
            ArrayList<Object> objArray = new ArrayList<>();

            return objArray;
        }
    }

    public class getRecorrd extends AsyncTask<String, String, String> {


        Context context;
        public MyDBManagerOneSignal mDbOneSignal;
        public App app;
        public CursorParserUniversal cParse;
        public String SubjectName = "", paper_type = "", created_on = "", notification_type_id = "", type = "";


        private getRecorrd(Context context) {

            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();

            mDbOneSignal = MyDBManagerOneSignal.getInstance(context);
            mDbOneSignal.open(context);

        }

        @Override
        protected void onPreExecute() {


            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... id) {

            //    int N_ID = mDbOneSignal.getLastRecordIDForRequest("notification", "OneSignalID");

            Cursor noti = mDbOneSignal.getAllRows("Select * from notification where opened=0" /*where _id > " + N_ID*/);
            Log.d("TAG", "Cursor :->" + DatabaseUtils.dumpCursorToString(noti));

            if (noti != null && noti.moveToFirst()) {
                do {
                    Notification notiObj = (Notification) cParse.parseCursor(noti, new Notification());
                    notiArray.add(notiObj);
                } while (noti.moveToNext());
                noti.close();
            }

            if (notiArray != null && notiArray.size() > 0) {
                for (int i = 0; i < notiArray.size(); i++) {
                    try {
                        JSONObject jObj = new JSONObject(notiArray.get(i).getFull_data());
                        String custom = jObj.getString("custom");
                        JSONObject jcustom = new JSONObject(custom);
                        String a = jcustom.getString("a");
                        JSONObject jtype = new JSONObject(a);
                        type = jtype.getString("type");
                        if (type.equalsIgnoreCase("Announcement")) {
                            mDbOneSignal.dbQuery("UPDATE notification SET opened = 1, collapse_id=0 where _id=" + notiArray.get(i).get_id());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (App.notiCount == 0) {
                redCircleNoti.setVisibility(View.GONE);
            } else {
                countTextViewNoti.setText(String.valueOf(App.notiCount));
            }
            getNotificationCount();

        }
    }

    public static void getNotificationCount() {
        Cursor c = mDbOneSignal.getAllRows("Select * from notification where opened=0 ifnull(collapse_id, '') = ''");

        // Log.d("TAG", "Cursor :->" + DatabaseUtils.dumpCursorToString(c));
        // Log.i("TAG", "Query :-> Select from notification where opened=0");
        if (c != null && c.moveToFirst()) {
            App.notiCount = c.getCount();
            Log.i("TAG", "In IF :-> " + App.notiCount);
            c.close();
        } else {
            App.notiCount = 0;
        }



    }
}
