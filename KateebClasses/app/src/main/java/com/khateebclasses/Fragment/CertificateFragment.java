package com.khateebclasses.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khateebclasses.R;
import com.khateebclasses.Adapter.CertificateAdapter;
import com.khateebclasses.Interface.AsynchTaskListner;
import com.khateebclasses.Model.Certification;
import com.khateebclasses.Utils.CallRequests;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.JsonParserUniversal;
import com.khateebclasses.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.khateebclasses.DashboardActivity.tv_title;

/**
 * Created by empiere-vaibhav on 4/7/2018.
 */

public class CertificateFragment extends Fragment implements AsynchTaskListner {
    public View view;
    public CertificateFragment instance;
    public RecyclerView recyclerCertificate;
    public CardView header;
    public ArrayList<Certification> certificationArray = new ArrayList<>();
    public Certification certification;
    public CertificateAdapter adapter;
    public JsonParserUniversal jParser;
    private LinearLayout emptyView;
    public TextView tv_empty, tv_count;
    public ImageView img_filter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_engenaring, container, false);
        instance = this;
        jParser = new JsonParserUniversal();
        tv_title.setText("CERTIFICATION");

        recyclerCertificate = view.findViewById(R.id.rcyclerView);
        emptyView = view.findViewById(R.id.empty_view);
        tv_empty = view.findViewById(R.id.tv_empty);
        img_filter = view.findViewById(R.id.img_filter);
        tv_count = view.findViewById(R.id.tv_count);
        header = view.findViewById(R.id.header);

        img_filter.setVisibility(View.GONE);
        emptyView.setVisibility(View.GONE);

        new CallRequests(CertificateFragment.this).get_certification();

        return view;
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            switch (request) {

                case get_certification:

                    certificationArray.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("Status").equals("Success")) {
                            if (jObj.getJSONArray("records") != null && jObj.getJSONArray("records").length() > 0) {
                                JSONArray jDataArray = jObj.getJSONArray("records");
                                if (jDataArray != null && jDataArray.length() > 0) {

                                    for (int i = 0; i < jDataArray.length(); i++) {
                                        JSONObject jStudyAbord = jDataArray.getJSONObject(i);
                                        certification = (Certification) jParser.parseJson(jStudyAbord, new Certification());
                                        certification.setAmount(jStudyAbord.getDouble("Amount"));

                                        certificationArray.add(certification);
                                    }

                                    adapter = new CertificateAdapter(certificationArray);
                                    final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);

                                    recyclerCertificate.setLayoutAnimation(controller);

                                    recyclerCertificate.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                    recyclerCertificate.setItemAnimator(new DefaultItemAnimator());
                                    recyclerCertificate.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                    Utils.hideProgressDialog();
                                    tv_count.setText(certificationArray.size() + " Courses");
                                }
                            } else {
                                Utils.hideProgressDialog();
                                recyclerCertificate.setVisibility(View.GONE);
                                header.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                            }
                        } else {
                            Utils.hideProgressDialog();
                            recyclerCertificate.setVisibility(View.GONE);
                            header.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}




