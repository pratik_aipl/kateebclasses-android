package com.khateebclasses.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.khateebclasses.ObservScroll.ObservableScrollView;
import com.khateebclasses.ObservScroll.ObservableScrollViewCallbacks;
import com.khateebclasses.R;

import static com.khateebclasses.DashboardActivity.tv_title;

public class ContectUsFragment extends Fragment {
    public View view;
    public ContectUsFragment instance;
    public WebView web_map;
    public TextView tv_link;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.contectus_fragment, container, false);
        final ObservableScrollView scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        Activity parentActivity = getActivity();
        tv_title.setText("CONTACT US");
        scrollView.setTouchInterceptionViewGroup((ViewGroup) parentActivity.findViewById(R.id.container));
        if (parentActivity instanceof ObservableScrollViewCallbacks) {
            scrollView.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentActivity);
        }
        web_map = view.findViewById(R.id.web_map);
        tv_link = view.findViewById(R.id.tv_link);
        web_map.getSettings().setJavaScriptEnabled(true);

        web_map.getSettings().setAllowFileAccess(true);
        web_map.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        web_map.getSettings().setJavaScriptEnabled(true);
        web_map.getSettings().setDomStorageEnabled(true);
        web_map.getSettings().setUseWideViewPort(true);

        web_map.getSettings().setBuiltInZoomControls(false);
        web_map.getSettings().setDisplayZoomControls(false);
        web_map.getSettings().setLoadWithOverviewMode(true);
        web_map.setInitialScale(1);
        web_map.loadUrl("file:///android_asset/map.html");
        String udata="www.khateebclasses.com";
        SpannableString content = new SpannableString(udata);
        content.setSpan(new UnderlineSpan(), 0, udata.length(), 0);
        tv_link.setText(content);
        tv_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.khateebclasses.com"));
                startActivity(browserIntent);
            }
        });
        instance = this;
        return view;
    }
}