package com.khateebclasses.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khateebclasses.Adapter.CourseHistoryAdapter;
import com.khateebclasses.Interface.AsynchTaskListner;
import com.khateebclasses.Model.CourseHistory;
import com.khateebclasses.R;
import com.khateebclasses.Utils.CallRequests;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.JsonParserUniversal;
import com.khateebclasses.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.khateebclasses.DashboardActivity.tv_title;

/**
 * Created by empiere-vaibhav on 4/20/2018.
 */

public class CourseHistoryFragment extends Fragment implements AsynchTaskListner {
    public View view;
    public CourseHistoryFragment instance;
    public RecyclerView recyclerStudyAbroad;
    public CardView header;
    public ArrayList<CourseHistory> courseHistoryArray = new ArrayList<>();
    public CourseHistory courseHistory;
    public CourseHistoryAdapter adapter;
    public JsonParserUniversal jParser;
    private LinearLayout emptyView;
    public TextView tv_empty, tv_count;
    public ImageView img_filter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_engenaring, container, false);
        instance = this;
        jParser = new JsonParserUniversal();
        tv_title.setText("COURSE HISTORY");
        recyclerStudyAbroad = view.findViewById(R.id.rcyclerView);
        emptyView = view.findViewById(R.id.empty_view);
        tv_empty = view.findViewById(R.id.tv_empty);
        img_filter = view.findViewById(R.id.img_filter);
        tv_count = view.findViewById(R.id.tv_count);


        header = view.findViewById(R.id.header);
        img_filter.setVisibility(View.GONE);
        emptyView.setVisibility(View.GONE);

        new CallRequests(CourseHistoryFragment.this).get_EnrolledSubCourse();

        return view;
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            switch (request) {

                case get_EnrolledSubCourse:

                    courseHistoryArray.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("Status").equals("Success")) {
                            if (jObj.getJSONArray("Enrolled") != null && jObj.getJSONArray("Enrolled").length() > 0) {
                                JSONArray jDataArray = jObj.getJSONArray("Enrolled");
                                if (jDataArray != null && jDataArray.length() > 0) {

                                    for (int i = 0; i < jDataArray.length(); i++) {
                                        JSONObject jStudyAbord = jDataArray.getJSONObject(i);
                                        courseHistory = (CourseHistory) jParser.parseJson(jStudyAbord, new CourseHistory());
                                        courseHistory.setAmount(jStudyAbord.getDouble("Amount"));

                                        courseHistoryArray.add(courseHistory);
                                    }

                                    adapter = new CourseHistoryAdapter(courseHistoryArray);
                                    final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);

                                    recyclerStudyAbroad.setLayoutAnimation(controller);

                                    recyclerStudyAbroad.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                    recyclerStudyAbroad.setItemAnimator(new DefaultItemAnimator());
                                    recyclerStudyAbroad.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                    Utils.hideProgressDialog();
                                    tv_count.setText(courseHistoryArray.size() + " Courses");
                                }
                            } else {
                                Utils.hideProgressDialog();
                                recyclerStudyAbroad.setVisibility(View.GONE);
                                header.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                            }
                        } else {
                            Utils.hideProgressDialog();
                            recyclerStudyAbroad.setVisibility(View.GONE);
                            header.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}




