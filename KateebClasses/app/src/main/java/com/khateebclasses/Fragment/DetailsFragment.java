package com.khateebclasses.Fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.khateebclasses.App;
import com.khateebclasses.Model.Certification;
import com.khateebclasses.Model.Mycart;
import com.khateebclasses.R;
import com.khateebclasses.Utils.Constant;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.khateebclasses.DashboardActivity.changeFragment;

/**
 * Created by empiere-vaibhav on 4/7/2018.
 */

public class DetailsFragment extends Fragment {

    public View view;
    public DetailsFragment instance;
    public TextView tv_price, tv_CourseType, tv_corseName, tv_detail, tv_shortdis;
    public App app;
    public Button tv_paynow, tv_mordetails;
    public SharedPreferences shared;
    public ImageView img_course;
    public boolean isExists = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_details, container, false);
        instance = this;
        app = App.getInstance();
        shared = getActivity().getSharedPreferences(getActivity().getPackageName(), 0);
        tv_paynow = view.findViewById(R.id.tv_paynow);
        tv_mordetails = view.findViewById(R.id.tv_mordetails);
        img_course = view.findViewById(R.id.img_course);


        tv_price = view.findViewById(R.id.tv_price);
        tv_detail = view.findViewById(R.id.tv_detail);
        tv_CourseType = view.findViewById(R.id.tv_CourseType);
        tv_corseName = view.findViewById(R.id.tv_corseName);
        tv_shortdis = view.findViewById(R.id.tv_shortdis);
        try {
            Type listType = new TypeToken<ArrayList<Mycart>>() {
            }.getType();
            Gson gson = new Gson();
            App.MycartArray = (ArrayList<Mycart>) gson.fromJson(shared.getString("CART", ""), listType);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tv_mordetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.WebviewURL = App.certification.getCourseURL();
                App.WebviewTitle = App.certification.getCourseTitle();
                changeFragment(new WebviewFragment(), true);
            }
        });
        tv_paynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (App.MycartArray != null && App.MycartArray.size() > 0) {
                    for (int i = 0; i < App.MycartArray.size(); i++) {
                        if (App.MycartArray.get(i).getCourseID() == App.certification.getCourseID()) {
                            Log.i("TAG"," IF :-> "+App.MycartArray.get(i).getCourseID()+" = "+App.certification.getCourseID()) ;
                            isExists = true;
                            break;
                        }else{
                            isExists = false;
                        }
                    }
                    if (!isExists) {
                        Addtocart();
                    }
                    changeFragment(new MyCartFragment(), true);
                } else {
                    App.MycartArray = new ArrayList<>();
                    Addtocart();
                    changeFragment(new MyCartFragment(), true);

                }


            }
        });
        setData(App.certification);
        return view;
    }

    public void Addtocart() {
        Mycart obj = new Mycart();
        obj.setAmount(App.certification.getAmount());
        obj.setCourseID(App.certification.getCourseID());
        obj.setCourseTitle(App.certification.getCourseTitle());
        obj.setDescription(App.certification.getShortDescription());
        obj.setIsType(0);
        App.MycartArray.add(obj);
        SharedPreferences.Editor et = shared.edit();
        Gson gson = new Gson();
        String json = gson.toJson(App.MycartArray);
        et.putString("CART", json);
        et.commit();
    }

    private void setData(Certification cer) {
        tv_corseName.setText(cer.getCourseTitle());
        tv_price.setText("Rs. " + cer.getAmount() + "");
        tv_detail.setText(cer.getDetail());
        tv_CourseType.setText("CourseType : " + cer.getCourseType());
        tv_shortdis.setText("Short Dis : " + cer.getShortDescription());
        Picasso.get()
                .load(Constant.DISPLAY_IMAGE_PREFIX + cer.getDispImage())
                .error(R.drawable.java_demo)
                .into(img_course);
        Log.i("TAG ", "Amount :-> " + cer.getAmount());

    }
}
