package com.khateebclasses.Fragment;

import android.app.DownloadManager;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.app.NotificationCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.khateebclasses.App;
import com.khateebclasses.Model.DownLoad;
import com.khateebclasses.Model.DownloadModel;
import com.khateebclasses.Model.FolderList;
import com.khateebclasses.R;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.FileDownloader;
import com.khateebclasses.Utils.JsonParserUniversal;
import com.khateebclasses.Utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.Future;

import static android.content.Context.DOWNLOAD_SERVICE;
import static com.khateebclasses.DashboardActivity.changeFragment;
import static com.khateebclasses.DashboardActivity.tv_title;

public class DownloadsFragment extends Fragment {
    public View view;
    public DownloadsFragment instance;
    public RecyclerView rcyclerView;
    public ArrayList<DownLoad> downloadArray = new ArrayList<>();
    public DownloadAdapter adapter;
    public DownLoad downLoad;
    public JsonParserUniversal jParser;
    private LinearLayout emptyView;
    public TextView tv_empty;
    public ProgressBar mProgressBar;
    public long downloadId;
    public DownloadManager manager;
    public String QFile, PaperName;
    // --- Add these variables
    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;
    // ---
    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
    int PERMISSION_ALL = 1;
    Future<File> downloading;
    String rootPath;

    private static final String TAG = "DownloadsFragment";
    public FolderList testModelData;

    ArrayList<DownloadModel> downLoadArray;
    DownloadModel downloadModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        instance = this;
        rcyclerView = view.findViewById(R.id.rcyclerView);
        jParser = new JsonParserUniversal();

        assert getArguments() != null;
        downLoadArray = getArguments().getParcelableArrayList("DataArray");


        tv_title.setText("DOWNLOADS");
        mProgressBar = view.findViewById(R.id.progress_bar);
        emptyView = view.findViewById(R.id.empty_view);
        tv_empty = view.findViewById(R.id.tv_empty);
        emptyView.setVisibility(View.GONE);

        //new CallRequests(DownloadsFragment.this).get_download();

        adapter = new DownloadAdapter(downLoadArray);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);

        rcyclerView.setLayoutAnimation(controller);

        rcyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rcyclerView.setItemAnimator(new DefaultItemAnimator());
        rcyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        return view;
    }


    public class DownloadAdapter extends RecyclerView.Adapter<DownloadAdapter.MyViewHolder> {

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_download_raw, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            final DownloadModel downLoad = downLoadArray.get(position);
            Log.d(TAG, "title ==> " + downLoad.getDownloadFileTitle());


            holder.tv_file_title.setText(downLoad.getDownloadFileTitle());
            holder.tv_date.setText(downLoad.getDownloadFileDate());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String extantion = downLoad.getDownloadFileURl().substring(downLoad.getDownloadFileURl().lastIndexOf("."));
                    if (!extantion.equals(".pdf")) {
                        Utils.showToast("Preview not available in app, To open it please download.", getActivity());
                    } else {

                        String URL = downLoad.getDownloadFileURl();
                        String fileName = downLoad.getDownloadFileTitle();

                        App.WebviewURL = URL;
                        App.WebviewTitle = fileName;
                        App.WebviewType = "PDF";
                        changeFragment(new WebviewFragment(), true);
                    }

                }
            });
            holder.rel_download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!Utils.hasPermissions(getActivity(), PERMISSIONS)) {
                        ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);

                    } else {
                        Log.d(TAG, "url_link: " + downLoad.getDownloadFileURl());
                        Uri URL = Uri.parse(downLoad.getDownloadFileURl());
                        String convert = downLoad.getDownloadFileURl();
                        String extantion = convert.substring(convert.lastIndexOf("."));

                        String fileName = downLoad.getDownloadFileTitle();


                        File pdf = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + fileName + extantion);
                        Utils.showToast("Downloading", getActivity());


                        DownloadManager.Request request = new DownloadManager.Request(URL);
                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName + extantion);
                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); // to notify when download is complete
                        request.allowScanningByMediaScanner();// if you want to be available from media players
                        DownloadManager manager = (DownloadManager) getContext().getSystemService(DOWNLOAD_SERVICE);
                        manager.enqueue(request);
                    }
                    /*if (pdf.isFile()) {
                        Utils.showToast("Already Downloaded", getActivity());
                    } else {
                                         }
*/
                    // new DownloadFile().execute(URL, fileName, extantion);
                }
            });

        }

        @Override
        public int getItemCount() {
            return downLoadArray.size();
        }

        public DownloadAdapter(ArrayList<DownloadModel> featuredJobList) {
            downLoadArray = featuredJobList;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tv_file_title, tv_date, tv_semester;
            LinearLayout rel_download;

            public MyViewHolder(View view) {
                super(view);

                tv_file_title = (TextView) view.findViewById(R.id.tv_file_title);
                tv_date = (TextView) view.findViewById(R.id.tv_date);
                rel_download = (LinearLayout) view.findViewById(R.id.rel_download);


            }


        }

    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {

            Utils.showProgressDialog(getActivity(), "Downloading..");
        }

        @Override
        protected Void doInBackground(String... strings) {
            QFile = strings[0];
            PaperName = strings[1] + strings[2];// -> maven.pdf
            Log.i("TAG", "URL  :-> " + QFile);
            String SDCardPath = Constant.LOCAL_IMAGE_PATH;

            File qDirectory = new File(SDCardPath);
            if (!qDirectory.exists()) qDirectory.mkdirs();

            String qLocalUrl = SDCardPath + "/" + PaperName;
            File qLocalFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + PaperName);
            if (!qLocalFile.exists()) {
                FileDownloader.downloadFile(QFile, qLocalFile, getActivity().getApplicationContext());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Utils.hideProgressDialog();


            File pdf = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + PaperName);
            if (pdf.isFile()) {
                Log.i("TAG", " if POST EXECUTE ::-> " + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + PaperName);
                new AlertDialog.Builder(getActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Alert")
                        .setCancelable(false)
                        .setMessage("OPEN file ")
                        .setPositiveButton("go to dir", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                                Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                                        + File.separator + "KhateebClasses" + File.separator);
                                intent.setDataAndType(uri, "text/pdf");
                                startActivity(Intent.createChooser(intent, "Open folder"));
                                dialog.dismiss();


                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            } else {
                new AlertDialog.Builder(getActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Alert")
                        .setCancelable(false)
                        .setMessage("Paper not available ")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                Log.i("TAG", " else POST EXECUTE ::-> " + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + PaperName);

            }
            if (Utils.isNetworkAvailable(getActivity())) {

            } else {
                //Utils.hideProgressDialog();
            }


        }
    }

}


