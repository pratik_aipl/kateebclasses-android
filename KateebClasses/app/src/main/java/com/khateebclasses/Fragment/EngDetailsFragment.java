package com.khateebclasses.Fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.khateebclasses.App;
import com.khateebclasses.Model.Engenaring;
import com.khateebclasses.Model.Mycart;
import com.khateebclasses.R;
import com.khateebclasses.Utils.Constant;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.khateebclasses.DashboardActivity.changeFragment;
import static com.khateebclasses.DashboardActivity.tv_title;

public class EngDetailsFragment extends Fragment {

    public View view;
    public EngDetailsFragment instance;
    public TextView tv_price, tv_professor, tv_semester, tv_corseName, tv_vanue, tv_time, tv_start_date, tv_batchType, tv_batchCode, tv_end_date;
    public SharedPreferences shared;
    public Button tv_paynow, tv_mordetails;
    public ImageView image;
    public boolean isExists = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_eng_details, container, false);
        instance = this;
        shared = getActivity().getSharedPreferences(getActivity().getPackageName(), 0);
        tv_title.setText("ENGINEERING CLASSES");
        tv_paynow = view.findViewById(R.id.tv_paynow);
        tv_mordetails = view.findViewById(R.id.tv_mordetails);
        tv_price = view.findViewById(R.id.tv_price);
        image = view.findViewById(R.id.image);
        tv_professor = view.findViewById(R.id.tv_professor);
        tv_semester = view.findViewById(R.id.tv_semester);
        tv_corseName = view.findViewById(R.id.tv_corseName);
        tv_vanue = view.findViewById(R.id.tv_vanue);
        tv_time = view.findViewById(R.id.tv_time);
        tv_start_date = view.findViewById(R.id.tv_start_date);
        tv_end_date = view.findViewById(R.id.tv_end_date);
        tv_batchType = view.findViewById(R.id.tv_batchType);
        tv_batchCode = view.findViewById(R.id.tv_batchCode);
        try {
            Type listType = new TypeToken<ArrayList<Mycart>>() {
            }.getType();
            Gson gson = new Gson();
            App.MycartArray = (ArrayList<Mycart>) gson.fromJson(shared.getString("CART", ""), listType);

        } catch (Exception e) {
            e.printStackTrace();
        }
        tv_mordetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //App.WebviewURL = App.engeneering.get
                App.WebviewTitle = App.engeneering.getSubjectName();
                changeFragment(new WebviewFragment(), true);
            }
        });

        tv_paynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (App.MycartArray != null && App.MycartArray.size() > 0) {
                    for (int i = 0; i < App.MycartArray.size(); i++) {
                        if (App.MycartArray.get(i).getCourseTitle().equals(App.engeneering.getSubjectName())) {
                            isExists = true;
                            break;
                        } else {
                            isExists = false;
                        }
                    }
                    if (!isExists) {
                        Addtocart();
                    }
                    changeFragment(new MyCartFragment(), true);
                } else {
                    App.MycartArray = new ArrayList<>();

                    Addtocart();

                    changeFragment(new MyCartFragment(), true);

                }


            }
        });


        setDataForEngineering(App.engeneering);
        return view;
    }

    public void Addtocart() {

        Mycart obj = new Mycart();
        obj.setCourseTitle(App.engeneering.getSubjectName());
        obj.setAmount(App.engeneering.getAmount());
        obj.setDescription(App.engeneering.getSemesterName());
        obj.setCourseID(App.engeneering.getBatchId());
        obj.setIsType(1);
        App.MycartArray.add(obj);
        SharedPreferences.Editor et = shared.edit();
        Gson gson = new Gson();
        String json = gson.toJson(App.MycartArray);
        et.putString("CART", json);
        et.commit();
    }


    private void setDataForEngineering(Engenaring engeneering) {
        tv_corseName.setText(engeneering.getSubjectName());
        tv_semester.setText(engeneering.getSemesterName());
        tv_price.setText("Rs. " + engeneering.getAmount() + "");
        Log.i("TAG ", "Amount :-> " + engeneering.getAmount());
        tv_professor.setText("Professor : " + engeneering.getFacultyName());
        tv_batchCode.setText(" : " + engeneering.getBatchCode());
        tv_batchType.setText(" : " + engeneering.getBatchType());
        //  tv_days.setText(engeneering.getAmount());
        tv_time.setText(" : " + engeneering.getBatchSchedule());
        tv_vanue.setText(" : " + engeneering.getVenueName());
        tv_start_date.setText(" : " + engeneering.getStartDate());
        tv_end_date.setText(" : " + engeneering.getEndDate());

        Log.i("ImageUrl", "==>" + Constant.DISPLAY_IMAGE_PREFIX + engeneering.getDispImage());
        Picasso.get()
                .load(Constant.DISPLAY_IMAGE_PREFIX + engeneering.getDispImage())
                .error(R.drawable.java_demo)
                .into(image);

        //  tv__practicals.setText(engeneering.getAmount());

    }
}
