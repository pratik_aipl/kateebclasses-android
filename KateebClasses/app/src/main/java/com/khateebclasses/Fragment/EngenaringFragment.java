package com.khateebclasses.Fragment;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.khateebclasses.Adapter.EngenaringAdapter;
import com.khateebclasses.Interface.AsynchTaskListner;
import com.khateebclasses.Model.Branch;
import com.khateebclasses.Model.Engenaring;
import com.khateebclasses.Model.Faculty;
import com.khateebclasses.Model.Semester;
import com.khateebclasses.Model.Subject;
import com.khateebclasses.R;
import com.khateebclasses.Utils.BlurView;
import com.khateebclasses.Utils.CallRequests;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.JsonParserUniversal;
import com.khateebclasses.Utils.Utils;
import com.khateebclasses.fonts.MyCustomTypeface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.khateebclasses.DashboardActivity.tv_title;

public class EngenaringFragment extends Fragment implements AsynchTaskListner {
    public ImageView filter_dilog_close;
    public ArrayList<Engenaring> engArray = new ArrayList<>();
    public ArrayList<Subject> subArray = new ArrayList<>();
    public ArrayList<String> strsubArray = new ArrayList<>();
    public ArrayList<Branch> branchArray = new ArrayList<>();
    public ArrayList<String> strBranchArray = new ArrayList<>();
    public ArrayList<Semester> semesterArray = new ArrayList<>();
    public ArrayList<String> strSemesterArray = new ArrayList<>();
    public ArrayList<Faculty> facultyArray = new ArrayList<>();
    public ArrayList<String> strFacultyArray = new ArrayList<>();

    public Spinner spSubject, spSemester, spBranch, spFaculty, spBatchtype;
    public Subject subjectObj;
    public Faculty facultyObj;
    public Branch branchObj;
    public Semester semObj;
    public int subjectID = 0, branchID = 0, semesterID = 0, facultyID = 0;
    public String batchType = "";
    public View view;
    public EngenaringFragment instance;
    public RecyclerView recyclerEngi;
    public CardView header;
    public ArrayList<Engenaring> engeneeringArray = new ArrayList<>();
    public Engenaring engeneering;
    public EngenaringAdapter adapter;
    public JsonParserUniversal jParser;
    private LinearLayout emptyView;
    public TextView tv_empty, tv_count,tv_clear;
    public ImageView img_filter;
    public Bitmap fast, map;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_engenaring, container, false);
        instance = this;
        jParser = new JsonParserUniversal();
        tv_title.setText("ENGINEERING CLASSES");
        recyclerEngi = view.findViewById(R.id.rcyclerView);
        img_filter = view.findViewById(R.id.img_filter);
        tv_clear = view.findViewById(R.id.tv_clear);

        img_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Filter();
            }
        });
        tv_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_clear.setVisibility(View.GONE);
                new CallRequests(instance).get_engineering_Subjects("0", "0", "0", "0", "");

            }
        });
        //DashboardActivity.toolbar.setTitle("ENGINEERING CLASSES");
        emptyView = view.findViewById(R.id.empty_view);
        tv_empty = view.findViewById(R.id.tv_empty);
        tv_count = view.findViewById(R.id.tv_count);
        header = view.findViewById(R.id.header);

        emptyView.setVisibility(View.GONE);


        subjectObj = new Subject();
        subjectObj.setSubjectName("Select Subject");
        subjectObj.setSubjectID(0);
        strsubArray.add(subjectObj.getSubjectName());
        subArray.add(subjectObj);


        branchObj = new Branch();
        branchObj.setBranchID(0);
        branchObj.setBranchName("Select Branch");
        strBranchArray.add(branchObj.getBranchName());
        branchArray.add(branchObj);


        semObj = new Semester();
        semObj.setSemesterID(0);
        semObj.setSemesterName("Select Semester");
        strSemesterArray.add(semObj.getSemesterName());
        semesterArray.add(semObj);


        facultyObj = new Faculty();
        facultyObj.setFacultyID(0);
        facultyObj.setFacultyName("Select Faculty");
        strFacultyArray.add(facultyObj.getFacultyName());
        facultyArray.add(facultyObj);

        new CallRequests(instance).get_engineering_Subjects("0", "0", "0", "0", "");

        return view;
    }


    public void Filter() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.popup_filter);
        new CallRequests(instance).get_Branches();
        new CallRequests(instance).get_Semesters();
        new CallRequests(instance).get_Subjects();
        new CallRequests(instance).get_Faculty();
        TextView tv_filter = dialog.findViewById(R.id.tv_filter);

        spBranch = dialog.findViewById(R.id.spBranch);
        spSemester = dialog.findViewById(R.id.spSemester);
        spSubject = dialog.findViewById(R.id.spSubject);
        spFaculty = dialog.findViewById(R.id.spFaculty);
        spBatchtype = dialog.findViewById(R.id.spBatchtype);
        filter_dilog_close = dialog.findViewById(R.id.filter_dilog_close);

        filter_dilog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Utils.hideProgressDialog();
            }
        });


        spSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/OpenSans-Regular.ttf")));

                    if (position > 0) {
                        if (position == 0) {
                            subjectID = 0;

                        } else {
                            //  ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/OpenSans-Regular.ttf")));

                            subjectID = subArray.get(position).getSubjectID();
                        }
                    } else {
                        subjectID = 0;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spBranch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/OpenSans-Regular.ttf")));

                    if (position > 0) {
                        if (position == 0) {
                            branchID = 0;

                        } else {
                            // ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/OpenSans-Regular.ttf")));
                            branchID = subArray.get(position).getSubjectID();
                        }
                    } else {
                        branchID = 0;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spSemester.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/OpenSans-Regular.ttf")));

                    if (position > 0) {
                        if (position == 0) {
                            semesterID = 0;


                        } else {
                            // ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/OpenSans-Regular.ttf")));
                            semesterID = semesterArray.get(position).getSemesterID();
                        }
                    } else {
                        semesterID = 0;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spFaculty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/OpenSans-Regular.ttf")));

                    if (position > 0) {
                        if (position == 0) {
                            facultyID = 0;

                        } else {
                            //((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/OpenSans-Regular.ttf")));
                            facultyID = facultyArray.get(position).getFacultyID();
                        }
                    } else {
                        facultyID = 0;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spBatchtype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/OpenSans-Regular.ttf")));

                    if (position > 0) {
                        // ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/OpenSans-Regular.ttf")));
                        if (spBatchtype.getSelectedItemPosition() == 1) {
                            batchType = "R";
                        } else if (spBatchtype.getSelectedItemPosition() == 2) {
                            batchType = "V";
                        } else {
                            batchType = "";
                        }

                    } else {
                        batchType = "";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        tv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                tv_clear.setVisibility(View.VISIBLE);
                new CallRequests(instance).get_engineering_Subjects(String.valueOf(branchID), String.valueOf(semesterID), String.valueOf(subjectID), String.valueOf(facultyID), batchType);
            }
        });

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    map = Utils.takeScreenShot(getActivity());
                    fast = new BlurView().fastBlur(map, 15);


                    //  dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        thread.start();
        final Drawable draw = new BitmapDrawable(getResources(), fast);
        Window window = dialog.getWindow();
        window.setBackgroundDrawable(draw);
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);
        window.setGravity(Gravity.FILL);

        dialog.show();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            switch (request) {

                case get_engineering_Subjects:

                    engeneeringArray.clear();
                    try {

                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("Status").equals("Success")) {
                            if (jObj.getJSONArray("records") != null && jObj.getJSONArray("records").length() > 0) {
                                JSONArray jDataArray = jObj.getJSONArray("records");
                                if (jDataArray != null && jDataArray.length() > 0) {

                                    for (int i = 0; i < jDataArray.length(); i++) {
                                        JSONObject jStudyAbord = jDataArray.getJSONObject(i);
                                        engeneering = (Engenaring) jParser.parseJson(jStudyAbord, new Engenaring());
                                        engeneering.setBatchId(jStudyAbord.getInt("BatchID"));
                                        engeneering.setAmount(jStudyAbord.getDouble("Amount"));
                                        engeneeringArray.add(engeneering);
                                    }

                                    adapter = new EngenaringAdapter(engeneeringArray);
                                    final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);
                                    recyclerEngi.setVisibility(View.VISIBLE);
                                    emptyView.setVisibility(View.GONE);
                                    recyclerEngi.setLayoutAnimation(controller);

                                    recyclerEngi.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                    recyclerEngi.setItemAnimator(new DefaultItemAnimator());
                                    recyclerEngi.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                    Utils.hideProgressDialog();
                                    tv_count.setText(engeneeringArray.size() + " Courses");
                                }
                            } else {
                                Utils.hideProgressDialog();
                                recyclerEngi.setVisibility(View.GONE);
                                tv_count.setText("0 Courses");
                             //   tv_empty.setText(jObj.getString("message"));
                                emptyView.setVisibility(View.VISIBLE);
                            }
                        } else {
                            Utils.hideProgressDialog();
                            recyclerEngi.setVisibility(View.GONE);
                            tv_count.setText("0 Courses");
                           // tv_empty.setText(jObj.getString("message"));
                            emptyView.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;
                case get_Branches:

                    branchArray.clear();
                    try {

                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("Status").equals("Success")) {
                            strBranchArray.clear();
                            branchArray.clear();
                            if (jObj.getJSONArray("records") != null && jObj.getJSONArray("records").length() > 0) {
                                JSONArray jDataArray = jObj.getJSONArray("records");
                                if (jDataArray != null && jDataArray.length() > 0) {
                                    branchObj = new Branch();
                                    branchObj.setBranchID(0);
                                    branchObj.setBranchName("Select Branch");
                                    strBranchArray.add(branchObj.getBranchName());
                                    branchArray.add(branchObj);

                                    for (int i = 0; i < jDataArray.length(); i++) {
                                        JSONObject jStudyAbord = jDataArray.getJSONObject(i);
                                        branchObj = (Branch) jParser.parseJson(jStudyAbord, new Branch());
                                        branchObj.setBranchID(jStudyAbord.getInt("BranchID"));
                                        strBranchArray.add(branchObj.getBranchName());
                                        branchArray.add(branchObj);

                                    }
                                    spBranch.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_row, strBranchArray));

                                }
                            } else {
                                Utils.hideProgressDialog();
                            }
                        } else {
                            Utils.hideProgressDialog();
                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;
                case get_Semesters:

                    semesterArray.clear();
                    try {

                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("Status").equals("Success")) {
                            semesterArray.clear();
                            strSemesterArray.clear();
                            if (jObj.getJSONArray("records") != null && jObj.getJSONArray("records").length() > 0) {
                                JSONArray jDataArray = jObj.getJSONArray("records");
                                if (jDataArray != null && jDataArray.length() > 0) {
                                    semObj = new Semester();
                                    semObj.setSemesterID(0);
                                    semObj.setSemesterName("Select Semester");
                                    strSemesterArray.add(semObj.getSemesterName());
                                    semesterArray.add(semObj);


                                    for (int i = 0; i < jDataArray.length(); i++) {
                                        JSONObject jStudyAbord = jDataArray.getJSONObject(i);
                                        semObj = (Semester) jParser.parseJson(jStudyAbord, new Semester());
                                        semObj.setSemesterID(jStudyAbord.getInt("SemesterID"));
                                        strSemesterArray.add(semObj.getSemesterName());
                                        semesterArray.add(semObj);

                                    }
                                    spSemester.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_row, strSemesterArray));

                                }
                            } else {
                                Utils.hideProgressDialog();
                            }
                        } else {
                            Utils.hideProgressDialog();
                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;
                case get_Subjects:

                    subArray.clear();
                    try {

                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("Status").equals("Success")) {
                            strsubArray.clear();
                            subArray.clear();
                            if (jObj.getJSONArray("records") != null && jObj.getJSONArray("records").length() > 0) {
                                JSONArray jDataArray = jObj.getJSONArray("records");
                                if (jDataArray != null && jDataArray.length() > 0) {
                                    subjectObj = new Subject();
                                    subjectObj.setSubjectName("Select Subject");
                                    subjectObj.setSubjectID(0);
                                    strsubArray.add(subjectObj.getSubjectName());
                                    subArray.add(subjectObj);
                                    for (int i = 0; i < jDataArray.length(); i++) {
                                        JSONObject jStudyAbord = jDataArray.getJSONObject(i);
                                        subjectObj = (Subject) jParser.parseJson(jStudyAbord, new Subject());
                                        subjectObj.setSubjectID(jStudyAbord.getInt("SubjectID"));
                                        strsubArray.add(subjectObj.getSubjectName());
                                        subArray.add(subjectObj);

                                    }
                                    spSubject.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_row, strsubArray));

                                }
                            } else {
                                Utils.hideProgressDialog();
                            }
                        } else {
                            Utils.hideProgressDialog();
                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;
                case get_Faculty:

                    facultyArray.clear();
                    try {

                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("Status").equals("Success")) {
                            strFacultyArray.clear();
                            facultyArray.clear();
                            if (jObj.getJSONArray("records") != null && jObj.getJSONArray("records").length() > 0) {
                                JSONArray jDataArray = jObj.getJSONArray("records");
                                if (jDataArray != null && jDataArray.length() > 0) {
                                    facultyObj = new Faculty();
                                    facultyObj.setFacultyID(0);
                                    facultyObj.setFacultyName("Select Faculty");
                                    strFacultyArray.add(facultyObj.getFacultyName());
                                    facultyArray.add(facultyObj);

                                    for (int i = 0; i < jDataArray.length(); i++) {
                                        JSONObject jStudyAbord = jDataArray.getJSONObject(i);
                                        facultyObj = (Faculty) jParser.parseJson(jStudyAbord, new Faculty());
                                        facultyObj.setFacultyID(jStudyAbord.getInt("FacultyID"));
                                        strFacultyArray.add(facultyObj.getFacultyName());
                                        facultyArray.add(facultyObj);

                                    }
                                    spFaculty.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_row, strFacultyArray));

                                }
                            } else {
                                Utils.hideProgressDialog();
                            }
                        } else {
                            Utils.hideProgressDialog();
                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}



