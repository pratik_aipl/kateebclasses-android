package com.khateebclasses.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.khateebclasses.App;
import com.khateebclasses.Interface.ObservableScrollViewCallbacks;
import com.khateebclasses.Model.OurEvent;
import com.khateebclasses.R;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.ObservableScrollView;
import com.khateebclasses.Utils.ScrollState;
import com.nineoldandroids.view.ViewHelper;
import com.squareup.picasso.Picasso;

public class EventDetailFragment extends Fragment implements ObservableScrollViewCallbacks {

    public View view;
    public EventDetailFragment instance;
    private ObservableScrollView mScrollView;
    public int mParallaxImageHeight;
    private TextView tv_FullDesc, tv_title, tv_date, tv_location;
    private ImageView mImageView,img_event;
    public FrameLayout frame_content;
    public App app;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_event_detail, container, false);
        instance = this;

        app = App.getInstance();
        tv_title = (TextView) view.findViewById(R.id.tv_title);
        tv_FullDesc = (TextView) view.findViewById(R.id.tv_FullDesc);
        tv_date = (TextView) view.findViewById(R.id.tv_date);
        frame_content = (FrameLayout) view.findViewById(R.id.frame_content);
        tv_location= (TextView) view.findViewById(R.id.tv_location);
        mImageView = view.findViewById(R.id.image);

        mScrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        mScrollView.setScrollViewCallbacks(this);

        mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen._180sdp);
        onScrollChanged(mScrollView.getCurrentScrollY(), false, false);
        setData(app.eventObj);
        return view;

    }

    public void setData(OurEvent obj) {
        tv_date.setText(obj.getEDate().toString()+" "+obj.getEventTime());
        tv_title.setText(obj.getEventName().toString());
       /* tv_FullDesc.setText(Html.fromHtml("<html>\n" +
                "<head>\n" +
                "  <title></title>\n" +
                "</head>\n" +
                "<body>\n" +
                "  <p>Founded by Junaid Khateeb in 2009 , Khateeb group of classes is an established name in Engineering coaching and training. Having trained over 10,000 students ,Khateeb group has build a strong relationship and trust with its student community by delivering high quality teaching and training programmes.</p>\n" +
                "  <p>'STUDENT FIRST' has always been the motto of the institute and the faculties associated with the institute. We are proud to have a strong line up of highly qualified and experienced faculties, who have delivered consistent results over the years.</p>\n" +
                "  <p>Khateeb Group of Institutes has the following institutions under its umbrella</p>\n" +
                "  <ul>\n" +
                "    <li>1) Khateeb Engineering CLasses:  Teaching and training Engineering students from all branches of Engineering.</li>\n" +
                "    <li>2) Khateeb Institute of Technical education : Provides training for global certification courses from ORACLE / CISCO/MICROSOFT/GOOGLE etc.</li>\n" +
                "    <li>3) Khateeb Academy of higher studies :Provides assistance and training for studying abroad in the countries like USA /UK/CANADA/AUSTRALIA etc.</li>\n" +
                "    <li>4) junkminds.com : Provides online lectures for all Engineering subjects.</li>\n" +
                "    <li>5) Edizone academy : Provides classroom teaching for VIII / IX and X std. students from ICSE/CBSE and SSC boards. </li>\n" +
                "  </ul>\n" +
                "</body>\n" +
                "</html>"));*/
       tv_FullDesc.setText(obj.getEventDescription());
        Picasso.get()
                .load(Constant.DISPLAY_IMAGE_PREFIX + obj.getDisplayImage())
                .error(R.drawable.java_demo)
                .into(mImageView);
        tv_location.setText(obj.getEventAddress());

    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        ViewHelper.setTranslationY(mImageView, scrollY / 2);
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }
}
