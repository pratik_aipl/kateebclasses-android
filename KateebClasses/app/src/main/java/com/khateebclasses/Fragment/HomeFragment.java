package com.khateebclasses.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khateebclasses.App;
import com.khateebclasses.Interface.NotificationTaskListner;
import com.khateebclasses.LoginActivity;
import com.khateebclasses.McqTestActivity;
import com.khateebclasses.Model.Notification;
import com.khateebclasses.R;
import com.khateebclasses.Utils.BlurView;
import com.khateebclasses.Utils.CursorParserUniversal;
import com.khateebclasses.Utils.MyDBManagerOneSignal;
import com.khateebclasses.Utils.Utils;
import com.skyfishjy.library.RippleBackground;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import static com.khateebclasses.DashboardActivity.actionBarDrawerToggle;
import static com.khateebclasses.DashboardActivity.changeFragment;
import static com.khateebclasses.DashboardActivity.drawer;
import static com.khateebclasses.DashboardActivity.toolbar;
import static com.khateebclasses.DashboardActivity.tv_title;

public class HomeFragment extends Fragment implements NotificationTaskListner {
    public View view;
    public HomeFragment instance;
    public ImageView img_engClasses, img_online_lecture, img_downloads, img_abroad_study, img_certification, img_onlineTest, img_announcment, img_event;
    public SharedPreferences pref;
    public FrameLayout redCircle, redCircleNoti;
    public TextView countTextView;
    public TextView countTextViewNoti;
    public static TextView tv_event_count;
    public static TextView tv_announc_count;
    public int notiCount;
    public static MyDBManagerOneSignal mDbOneSignal;
    public LinearLayout linMain;
    public Bitmap map, fast;
    public static RippleBackground frame_event_count;
    public static RippleBackground frame_announc_count;
    public ArrayList<Notification> notiArray = new ArrayList<>();
    public static int EventCount = 0;
    public static int AnnouncementCount = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        instance = this;
        setHasOptionsMenu(true);

        tv_title.setText("DASHBOARD");
        toolbar.setTitle("");
        mDbOneSignal = MyDBManagerOneSignal.getInstance(getActivity());
        mDbOneSignal.open(getActivity());

        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
        drawer.closeDrawers();
        frame_event_count = view.findViewById(R.id.frame_event_count);
        frame_announc_count = view.findViewById(R.id.frame_announc_count);
        frame_event_count.startRippleAnimation();
        frame_announc_count.startRippleAnimation();

        tv_event_count = view.findViewById(R.id.tv_event_count);
        tv_announc_count = view.findViewById(R.id.tv_announc_count);
        img_event = view.findViewById(R.id.img_event);
        img_announcment = view.findViewById(R.id.img_announcment);
        linMain = view.findViewById(R.id.linMain);
        tv_announc_count = view.findViewById(R.id.tv_announc_count);
        frame_event_count = view.findViewById(R.id.frame_event_count);
        img_onlineTest = view.findViewById(R.id.img_onlineTest);
        img_engClasses = view.findViewById(R.id.img_engClasses);
        img_downloads = view.findViewById(R.id.img_downloads);
        img_abroad_study = view.findViewById(R.id.img_abroad_study);
        img_certification = view.findViewById(R.id.img_certification);
        img_online_lecture = view.findViewById(R.id.img_online_lecture);
        setHasOptionsMenu(true);
        img_onlineTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (App.IS_LOGGED) {
                    McqPopup();
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Login?")
                            .setMessage("Please login to continue ")
                            .setNegativeButton(android.R.string.no, null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface arg0, int arg1) {
                                    Intent i = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(i);
                                    getActivity().finishAffinity();
                                    //  getActivity().moveTaskToBack(true);
                                }
                            }).create().show();

                }


            }
        });
        img_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new OverEventsFragment(), true);
            }
        });
        img_announcment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new AnnounceFragment(), true);
            }
        });
        img_online_lecture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LecturePopup();

            }
        });
        img_engClasses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EngPopup();
            }
        });

        img_downloads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    changeFragment(new MainDownloadsFragment(), true);
                }
            }
        });
        img_abroad_study.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AbroadPopup();

            }
        });
        img_certification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CertiPopup();

            }
        });


        return view;
    }

    private void LecturePopup() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.defaault_popup);
        ImageView img_type = dialog.findViewById(R.id.img_type);
        img_type.setImageResource(R.drawable.alert_online);
        TextView tv_popup_title = dialog.findViewById(R.id.tv_popup_title);
        TextView tv_update = dialog.findViewById(R.id.tv_update);
        TextView tv_contanit = dialog.findViewById(R.id.tv_contanit);
        ImageView profile_dilog_close = dialog.findViewById(R.id.profile_dilog_close);
        tv_popup_title.setText("Online Lecture");
        tv_contanit.setText("if you wish to study your subjects from the comfort of your home. ");
        tv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Utils.isNetworkAvailable(getActivity())) {
                    App.WebviewURL = "https://junkminds.com/";
                    App.WebviewTitle = "ONLINE LECTURE";
                    changeFragment(new WebviewFragment(), true);
                }

            }
        });
        profile_dilog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    map = Utils.takeScreenShot(getActivity());
                    fast = new BlurView().fastBlur(map, 15);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        final Drawable draw = new BitmapDrawable(getResources(), fast);
        Window window = dialog.getWindow();
        window.setBackgroundDrawable(draw);
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);
        window.setGravity(Gravity.FILL);
        dialog.show();
    }

    private void McqPopup() {
      /*  Blurry.with(getActivity())
                .radius(25)
                .sampling(1)
                .color(Color.argb(66, 0, 255, 255))
                .async()
                .capture(linMain)
                .into((ImageView) view.findViewById(R.id.img_capture));
      */

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.defaault_popup);
        ImageView img_type = dialog.findViewById(R.id.img_type);
        img_type.setImageResource(R.drawable.alert_mcq);
        TextView tv_popup_title = dialog.findViewById(R.id.tv_popup_title);
        TextView tv_update = dialog.findViewById(R.id.tv_update);
        TextView tv_contanit = dialog.findViewById(R.id.tv_contanit);
        ImageView profile_dilog_close = dialog.findViewById(R.id.profile_dilog_close);
        tv_popup_title.setText("Suggeset My Branch Test");
        tv_contanit.setText("If you are confused about which branch of engineering to opt for, then take our simple test. Answer  60 carefully designed questions and based on your performance, we will be able to suggest the right branch for you. \n" +
                "you take the test and we will email you the result with 5 minutes of the completion of the test.");
        tv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Utils.isNetworkAvailable(getActivity())) {
                    if (App.TestMainArray != null && App.TestMainArray.size() > 0) {
                        Log.i("TAG", "IN IF :-> " + App.TestMainArray.size());

                        startActivity(new Intent(getActivity(), McqTestActivity.class)
                                .putExtra("MCQ", App.TestMainArray)
                                .putExtra("start", "resume"));
                    } else {

                        startActivity(new Intent(getActivity(), McqTestActivity.class)
                                .putExtra("start", "new"));
                    }


                }
            }
        });
        profile_dilog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Blurry.delete((ViewGroup) view.findViewById(R.id.img_capture));
                dialog.dismiss();

            }
        });

        dialog.show();

        long startMs = System.currentTimeMillis();

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    map = Utils.takeScreenShot(getActivity());
                    fast = new BlurView().fastBlur(map, 15);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
        final Drawable draw = new BitmapDrawable(getResources(), fast);
        Window window = dialog.getWindow();
        window.setBackgroundDrawable(draw);
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);
        window.setGravity(Gravity.FILL);
    }

    private void CertiPopup() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.defaault_popup);
        ImageView img_type = dialog.findViewById(R.id.img_type);
        img_type.setImageResource(R.drawable.alert_certificate);
        TextView tv_popup_title = dialog.findViewById(R.id.tv_popup_title);
        TextView tv_update = dialog.findViewById(R.id.tv_update);
        ImageView profile_dilog_close = dialog.findViewById(R.id.profile_dilog_close);
        TextView tv_contanit = dialog.findViewById(R.id.tv_contanit);
        tv_contanit.setText("For technical courses in JAVA /Android/.NET/Python or certification programmes from ORACLE /CISCO/GOOGLE etc.");
        tv_popup_title.setText("Certification");

        tv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Utils.isNetworkAvailable(getActivity())) {
                    changeFragment(new CertificateFragment(), true);
                }
            }
        });
        profile_dilog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    map = Utils.takeScreenShot(getActivity());
                    fast = new BlurView().fastBlur(map, 15);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        final Drawable draw = new BitmapDrawable(getResources(), fast);
        Window window = dialog.getWindow();
        window.setBackgroundDrawable(draw);
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);
        window.setGravity(Gravity.FILL);
        dialog.show();
    }

    private void AbroadPopup() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.defaault_popup);
        ImageView img_type = dialog.findViewById(R.id.img_type);
        img_type.setImageResource(R.drawable.alert_study);
        TextView tv_popup_title = dialog.findViewById(R.id.tv_popup_title);
        TextView tv_update = dialog.findViewById(R.id.tv_update);
        ImageView profile_dilog_close = dialog.findViewById(R.id.profile_dilog_close);
        TextView tv_contanit = dialog.findViewById(R.id.tv_contanit);
        tv_contanit.setText("Want to study abroad ? if you wish to study in USA /UK/CANADA/AUSTRALIA or just about anywhere in the world.");
        tv_popup_title.setText("Abroad Study");
        tv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Utils.isNetworkAvailable(getActivity())) {
                    changeFragment(new AbroadStudyFragment(), true);
                }
            }
        });
        profile_dilog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    map = Utils.takeScreenShot(getActivity());
                    fast = new BlurView().fastBlur(map, 15);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        final Drawable draw = new BitmapDrawable(getResources(), fast);
        Window window = dialog.getWindow();
        window.setBackgroundDrawable(draw);
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);
        window.setGravity(Gravity.FILL);
        dialog.show();
    }

    public void EngPopup() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.defaault_popup);
        ImageView img_type = dialog.findViewById(R.id.img_type);
        img_type.setImageResource(R.drawable.alert_eng);
        TextView tv_popup_title = dialog.findViewById(R.id.tv_popup_title);
        TextView tv_update = dialog.findViewById(R.id.tv_update);
        ImageView profile_dilog_close = dialog.findViewById(R.id.profile_dilog_close);
        TextView tv_contanit = dialog.findViewById(R.id.tv_contanit);
        tv_contanit.setText("Looking for classroom courses  for engineering subjects. we provide coaching from F.E. to B.E.");
        tv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Utils.isNetworkAvailable(getActivity())) {
                    changeFragment(new EngenaringFragment(), true);
                }
            }
        });
        profile_dilog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    map = Utils.takeScreenShot(getActivity());
                    fast = new BlurView().fastBlur(map, 15);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        final Drawable draw = new BitmapDrawable(getResources(), fast);
        Window window = dialog.getWindow();
        window.setBackgroundDrawable(draw);
        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.FILL_PARENT);
        window.setGravity(Gravity.FILL);
        dialog.show();
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public static String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] arr = baos.toByteArray();
        return Base64.encodeToString(arr, Base64.DEFAULT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cart_custom:
                changeFragment(new MyCartFragment(), true);
                return true;

            case R.id.noti:
                // TODO update alert menu icon
                changeFragment(new NotificationFragment(), true);
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void getNotificationCount() {
        Cursor c = mDbOneSignal.getAllRows("Select * from notification where opened=0 AND ifnull(collapse_id, '') = ''");

        // Log.d("TAG", "Cursor :->" + DatabaseUtils.dumpCursorToString(c));
        // Log.i("TAG", "Query :-> Select from notification where opened=0");
        if (c != null && c.moveToFirst()) {
            App.notiCount = c.getCount();
            Log.i("TAG", "In IF :-> " + App.notiCount);
            c.close();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        final MenuItem alertMenuItem = menu.findItem(R.id.cart_custom);
        FrameLayout rootView = (FrameLayout) alertMenuItem.getActionView();

        redCircle = (FrameLayout) rootView.findViewById(R.id.view_alert_red_circle);
        countTextView = (TextView) rootView.findViewById(R.id.view_alert_count_textview);

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(alertMenuItem);
            }
        });

        if (App.MycartArray != null && App.MycartArray.size() > 0) {
            countTextView.setText(App.MycartArray.size() + "");
        } else {
            redCircle.setVisibility(View.GONE);
        }


        final MenuItem noti = menu.findItem(R.id.noti);

        FrameLayout notirootView = (FrameLayout) noti.getActionView();

        redCircleNoti = (FrameLayout) notirootView.findViewById(R.id.view_alert_red_circle);
        countTextViewNoti = (TextView) notirootView.findViewById(R.id.view_alert_count_textview);
        if (App.notiCount == 0) {
            redCircleNoti.setVisibility(View.GONE);
        } else {
            countTextViewNoti.setText(String.valueOf(App.notiCount));
        }


        notirootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(noti);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        EventCount = 0;
        AnnouncementCount = 0;
        new getRecorrd(getActivity()).execute();
        try {
            if (App.MycartArray != null && App.MycartArray.size() > 0) {
                countTextView.setText(App.MycartArray.size() + "");
            } else {
                redCircle.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {

            /*  */


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onReceived(String result) {

        new getRecorrd(getActivity()).execute();
    }

    public class getRecorrd extends AsyncTask<String, String, String> {


        Context context;
        public MyDBManagerOneSignal mDbOneSignal;
        public App app;
        public CursorParserUniversal cParse;
        public String SubjectName = "", paper_type = "", created_on = "", notification_type_id = "", type = "";


        private getRecorrd(Context context) {

            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();

            mDbOneSignal = MyDBManagerOneSignal.getInstance(context);
            mDbOneSignal.open(context);

        }

        @Override
        protected void onPreExecute() {

            EventCount = 0;
            AnnouncementCount = 0;
            notiArray.clear();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... id) {
            getNotificationCount();
            //    int N_ID = mDbOneSignal.getLastRecordIDForRequest("notification", "OneSignalID");

            Cursor noti = mDbOneSignal.getAllRows("Select * from notification where opened=0  " /*where _id > " + N_ID*/);
            Log.d("TAG", "Cursor :->" + DatabaseUtils.dumpCursorToString(noti));

            if (noti != null && noti.moveToFirst()) {
                do {
                    Notification notiObj = (Notification) cParse.parseCursor(noti, new Notification());
                    notiArray.add(notiObj);
                } while (noti.moveToNext());
                noti.close();
            }

            if (notiArray != null && notiArray.size() > 0) {
                for (int i = 0; i < notiArray.size(); i++) {
                    try {
                        JSONObject jObj = new JSONObject(notiArray.get(i).getFull_data());
                        String custom = jObj.getString("custom");
                        JSONObject jcustom = new JSONObject(custom);
                        String a = jcustom.getString("a");
                        JSONObject jtype = new JSONObject(a);
                        type = jtype.getString("type");
                        if (type.equalsIgnoreCase("Event")) {
                            EventCount++;
                        } else if (type.equalsIgnoreCase("Announcement")) {
                            AnnouncementCount++;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (EventCount != 0) {
                tv_event_count.setText(EventCount + "");
                frame_event_count.setVisibility(View.VISIBLE);
            } else {
                frame_event_count.setVisibility(View.GONE);
            }
            if (AnnouncementCount != 0) {
                tv_announc_count.setText(AnnouncementCount + "");
                frame_announc_count.setVisibility(View.VISIBLE);
            } else {
                frame_announc_count.setVisibility(View.GONE);
            }

        }
    }
}
