package com.khateebclasses.Fragment;

import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.khateebclasses.Interface.AsynchTaskListner;
import com.khateebclasses.Model.DownloadModel;
import com.khateebclasses.Model.FolderList;
import com.khateebclasses.R;
import com.khateebclasses.Utils.CallRequests;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.JsonParserUniversal;
import com.khateebclasses.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.khateebclasses.DashboardActivity.changeFragment;
import static com.khateebclasses.DashboardActivity.tv_title;

public class MainDownloadsFragment extends Fragment implements AsynchTaskListner {

    private static final String TAG = "MainDownloadsFragment";

    public View view;
    public MainDownloadsFragment instance;
    public RecyclerView rcyclerView;

    public ArrayList<FolderList> folderArray = new ArrayList<>();

    public DownloadAdapter adapter;
    public FolderList folderList;
    public JsonParserUniversal jParser;
    private LinearLayout emptyView;
    public TextView tv_empty;
    public ProgressBar mProgressBar;
    int ids;
    Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        instance = this;
        rcyclerView = view.findViewById(R.id.rcyclerView);
        jParser = new JsonParserUniversal();

        tv_title.setText("DOWNLOADS");

        mProgressBar = view.findViewById(R.id.progress_bar);
        emptyView = view.findViewById(R.id.empty_view);
        tv_empty = view.findViewById(R.id.tv_empty);
        emptyView.setVisibility(View.GONE);
        new CallRequests(MainDownloadsFragment.this).get_Folderlist();

        return view;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            switch (request) {

                case get_folderlist:
                    folderArray.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        Log.d(TAG, "Folder List: ==>>" + jObj.toString());
                        if (jObj.getString("Status").equals("Success")) {
                            if (jObj.getJSONArray("records") != null && jObj.getJSONArray("records").length() > 0) {
                                JSONArray jDataArray = jObj.getJSONArray("records");
                                if (jDataArray != null && jDataArray.length() > 0) {

                                    for (int i = 0; i < jDataArray.length(); i++) {
                                        JSONObject jOurEvent = jDataArray.getJSONObject(i);
                                        folderList = (FolderList) jParser.parseJson(jOurEvent, new FolderList());
                                        folderList.setDownloadFolderID(jOurEvent.getInt("DownloadFolderID"));
                                        folderList.setPasswordProtected(jOurEvent.getBoolean("IsPasswordProtected"));
                                        folderList.setDFolderTitle(jOurEvent.getString("DFolderTitle"));
                                        folderArray.add(folderList);
                                    }

                                    adapter = new DownloadAdapter(folderArray);
                                    final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);

                                    rcyclerView.setLayoutAnimation(controller);

                                    rcyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                    rcyclerView.setItemAnimator(new DefaultItemAnimator());
                                    rcyclerView.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                    Utils.hideProgressDialog();
                                }
                            } else {
                                Utils.hideProgressDialog();
                                rcyclerView.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                            }
                        } else {
                            Utils.hideProgressDialog();
                            rcyclerView.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;
                case get_passwordProt:

                    //downLoads.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        Log.d(TAG, "passwd List: ==>>" + jObj.toString());
                        if (jObj.getString("Status").equals("Success")) {
                            if (jObj.getString("Result").equals("Success")) {
                                if (jObj.getJSONArray("records") != null && jObj.getJSONArray("records").length() > 0) {
                                    JSONArray jDataArray = jObj.getJSONArray("records");
                                    if (jDataArray != null && jDataArray.length() > 0) {
                                        ArrayList<DownloadModel> downLoads = new ArrayList<>();
                                        for (int i = 0; i < jDataArray.length(); i++) {
                                            JSONObject jOurEvent = jDataArray.getJSONObject(i);

                                            DownloadModel downLoad = (DownloadModel) jParser.parseJson(jOurEvent, new DownloadModel());
                                            downLoad.setDownloadFileTitle(jOurEvent.getString("DownloadFileTitle"));
                                            downLoad.setDownloadFileDate(jOurEvent.getString("DownloadFileDate"));
                                            downLoad.setDownloadFileURl(jOurEvent.getString("DownloadFileURl"));
                                            downLoad.setDownloadFolderFilesID(jOurEvent.getInt("DownloadFolderFilesID"));
                                            downLoad.setDownloadFolderID(jOurEvent.getInt("DownloadFolderID"));
                                            downLoads.add(downLoad);
                                        }
                                        dialog.dismiss();
                                        DownloadsFragment fragment = new DownloadsFragment();
                                        Bundle bundle = new Bundle();
                                        bundle.putParcelableArrayList("DataArray", downLoads);
                                        fragment.setArguments(bundle);
                                        changeFragment(fragment, true);

                                        //DownloadsFragment.newInstance(folderArray);
                                        Utils.hideProgressDialog();
                                    }
                                } else {
                                    Utils.hideProgressDialog();
                                }
                            } else {
                                Utils.hideProgressDialog();
                                Toast.makeText(getActivity(), jObj.getString("UMessage"), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Utils.hideProgressDialog();
                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;
                case get_passwordNoProt:

                    //folderArray.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        Log.d(TAG, "nopasswd List: ==>>" + jObj.toString());
                        if (jObj.getString("Status").equals("Success")) {
                            if (jObj.getString("Result").equals("Success")) {
                                if (jObj.getJSONArray("records") != null && jObj.getJSONArray("records").length() > 0) {
                                    JSONArray jDataArray = jObj.getJSONArray("records");
                                    if (jDataArray != null && jDataArray.length() > 0) {
                                        ArrayList<DownloadModel> downLoads = new ArrayList<>();

                                        for (int i = 0; i < jDataArray.length(); i++) {
                                            JSONObject jOurEvent = jDataArray.getJSONObject(i);

                                            DownloadModel downLoad = (DownloadModel) jParser.parseJson(jOurEvent, new DownloadModel());
                                            downLoad.setDownloadFileTitle(jOurEvent.getString("DownloadFileTitle"));
                                            downLoad.setDownloadFileDate(jOurEvent.getString("DownloadFileDate"));
                                            downLoad.setDownloadFileURl(jOurEvent.getString("DownloadFileURl"));
                                            downLoad.setDownloadFolderFilesID(jOurEvent.getInt("DownloadFolderFilesID"));
                                            downLoad.setDownloadFolderID(jOurEvent.getInt("DownloadFolderID"));
                                            downLoads.add(downLoad);
                                        }
                                        DownloadsFragment fragment = new DownloadsFragment();
                                        Bundle bundle = new Bundle();
                                        bundle.putParcelableArrayList("DataArray", downLoads);
                                        fragment.setArguments(bundle);
                                        changeFragment(fragment, true);
                                        Utils.hideProgressDialog();
                                    }
                                } else {
                                    Utils.hideProgressDialog();
                                }
                            } else {
                                Utils.hideProgressDialog();
                                Toast.makeText(getActivity(), jObj.getString("UMessage"), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Utils.hideProgressDialog();
                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;
            }
        } else {
            Utils.hideProgressDialog();
            Toast.makeText(getActivity(), "Data Not Found", Toast.LENGTH_SHORT).show();

        }
    }


    public class DownloadAdapter extends RecyclerView.Adapter<DownloadAdapter.MyViewHolder> {

        public ArrayList<FolderList> downLoadArrayList;

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.main_download_raw, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final FolderList folderList = folderArray.get(position);

            holder.tv_folder_title.setText(folderList.getDFolderTitle());
            if (folderList.getPasswordProtected() == true) {

                holder.img_prot.setVisibility(View.VISIBLE);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Here Check Condition And open Dialog or Activity

                    if (folderList.getPasswordProtected() == true) {

                        ConfirmationPopup(folderArray.get(position).getDownloadFolderID());
                    } else {

                        new CallRequests(instance).getPassword_NoProtected(folderList.getDownloadFolderID());
                    }

                }
            });
        }

        private void ConfirmationPopup(int id) {
            ids = id;
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialog.setContentView(R.layout.verify_down_popup);
            dialog.setCancelable(false);
            Button btn_cancle = dialog.findViewById(R.id.btn_cancle);
            Button btn_submit = dialog.findViewById(R.id.btn_submit);
            final EditText et_verify = dialog.findViewById(R.id.et_verify);
            //et_verify.setText("Khateeb@123");
            ImageView profile_dilog_close = dialog.findViewById(R.id.profile_dilog_close);

            btn_cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Utils.isNetworkAvailable(getActivity())) {

                        if (et_verify.getText().toString().equals("")) {
                            et_verify.setError("Please Enter Valid Password");
                            et_verify.setFocusable(true);
                        } else {
                            // dialog.dismiss();
                            // changeFragment(new DownloadsFragment(), true);
                            new CallRequests(instance).getPassword_Protected(ids, et_verify.getText().toString());
                        }
                    }
                }
            });

            dialog.show();
        }

        @Override
        public int getItemCount() {
            return folderArray.size();
        }

        public DownloadAdapter(ArrayList<FolderList> folderLists) {
            this.downLoadArrayList = folderLists;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tv_folder_title;
            ImageView img_prot;

            public MyViewHolder(View view) {
                super(view);

                tv_folder_title = (TextView) view.findViewById(R.id.tv_folder_title);
                img_prot = (ImageView) view.findViewById(R.id.img_prot);


            }
        }

    }


}


