package com.khateebclasses.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.khateebclasses.App;
import com.khateebclasses.ConfirmOrederActivity;
import com.khateebclasses.Interface.AsynchTaskListner;
import com.khateebclasses.LoginActivity;
import com.khateebclasses.Model.Certification;
import com.khateebclasses.Model.Mycart;
import com.khateebclasses.Model.PaymentDetail;
import com.khateebclasses.Model.PaymentMain;
import com.khateebclasses.R;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;

import static com.khateebclasses.DashboardActivity.tv_title;

public class MyCartFragment extends Fragment implements AsynchTaskListner {

    public View view;
    public MyCartFragment instance;
    public RecyclerView rcyclerView;
    public Button tv_checkout;
    public String title[] = {"Java Application Devlopment", "Computer Since"};
    public MycartAdapter adapter;
    public App app;
    public String amount;
    public SharedPreferences shared;
    public static ArrayList<Certification> CaertArray = new ArrayList<>();
    double Total = 0;
    public TextView tv_amount;
    public ArrayList<PaymentMain> paymentMainArrayList = new ArrayList<>();
    public TextView tv_empty;
    private LinearLayout emptyView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_mycart, container, false);

        shared = getActivity().getSharedPreferences(getActivity().getPackageName(), 0);
        app = App.getInstance();
        instance = this;
        tv_title.setText("MY CART");
        setHasOptionsMenu(true);
        rcyclerView = view.findViewById(R.id.rcyclerView);
        tv_checkout = view.findViewById(R.id.tv_checkout);
        tv_amount = view.findViewById(R.id.tv_amount);
        emptyView = view.findViewById(R.id.empty_view);
        tv_empty = view.findViewById(R.id.tv_empty);
        try {
            Type listType = new TypeToken<ArrayList<Mycart>>() {
            }.getType();
            Gson gson = new Gson();
            App.MycartArray = (ArrayList<Mycart>) gson.fromJson(shared.getString("CART", ""), listType);

        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            for (int i = 0; i < App.MycartArray.size(); i++) {
                Total += App.MycartArray.get(i).getAmount();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        tv_amount.setText("Rs. " + Total + "");

        if (App.MycartArray != null) {
            adapter = new MycartAdapter(CaertArray);
            final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);

            rcyclerView.setLayoutAnimation(controller);

            rcyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            rcyclerView.setItemAnimator(new DefaultItemAnimator());
            rcyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
        tv_checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (App.MycartArray != null && App.MycartArray.size()>0){
                    SubmitData();
                }else{
                    Utils.showToast("Atleast add 1 item in cart.",getActivity());
                }

            }
        });
        return view;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {

                case make_payment:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getString("Status").equals("Success")) {
                            Utils.showToast("Sucess Payment", getActivity());
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    public class MycartAdapter extends RecyclerView.Adapter<MycartAdapter.MyViewHolder> {

        public ArrayList<Certification> featuredJobList;
        public Date date1, date2;
        Context context;

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_my_cart_raw, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {

            final Mycart certification = App.MycartArray.get(position);
            holder.tv_title.setText(certification.getCourseTitle());
            holder.tv_price.setText("Rs. " + certification.getAmount());

            holder.rel_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Total = Total - App.MycartArray.get(position).getAmount();
                    App.MycartArray.remove(position);
                    SharedPreferences.Editor et = shared.edit();
                    Gson gson = new Gson();
                    String json = gson.toJson(App.MycartArray);
                    et.putString("CART", json);
                    et.commit();
                    notifyDataSetChanged();


                    tv_amount.setText("Rs. " + Total + "");
                }

            });


        }

        @Override
        public int getItemCount() {
            return App.MycartArray.size();
        }

        public MycartAdapter(ArrayList<Certification> featuredJobList) {
            this.featuredJobList = featuredJobList;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tv_title, tv_price, tv_semester;
            LinearLayout rel_cancel;

            public MyViewHolder(View view) {
                super(view);

                tv_title = (TextView) view.findViewById(R.id.tv_title);
                tv_price = (TextView) view.findViewById(R.id.tv_price);
                tv_semester = (TextView) view.findViewById(R.id.tv_semester);
                rel_cancel = (LinearLayout) view.findViewById(R.id.rel_cancel);


            }


        }

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.getItem(0).setVisible(false);


    }


    @Override
    public void onStart() {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.}
    }

    @Override
    public void onStop() {
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    public void SubmitData() {

        PaymentMain dataObj = new PaymentMain();

        if (app.IS_LOGGED) {

            dataObj.setStudent_id(String.valueOf(app.user.getStudentID()));
            dataObj.setTransactionID("abcd1234");
            dataObj.setTotalAmount(Total + "");
            dataObj.setPaymentStatus("success");
            for (int i = 0; i < app.MycartArray.size(); i++) {
                PaymentDetail hdrObj = new PaymentDetail();
                hdrObj.setAmount(String.valueOf(app.MycartArray.get(i).getAmount()));
                hdrObj.setBatchCourseID(String.valueOf(app.MycartArray.get(i).getCourseID()));
                if (app.MycartArray.get(i).getIsType() == 1) {
                    hdrObj.setBatchOrCourse("B");

                } else {
                    hdrObj.setBatchOrCourse("C");

                }
                dataObj.PaymentDtl.add(hdrObj);


            }
            paymentMainArrayList.add(dataObj);
            Gson gson = new GsonBuilder().create();
            JsonArray jCctArray = gson.toJsonTree(paymentMainArrayList).getAsJsonArray();
            String jsonString = String.valueOf(jCctArray);
            String jSonLog = jsonString.substring(1, jsonString.length() - 1);
            longInfo(jSonLog, "MCQ ::-->");
            if (app.user.getStudentID() != 0) {

                startActivity(new Intent(getActivity(), ConfirmOrederActivity.class)
                        .putExtra("jSonLog", jSonLog)
                        .putExtra("MycartArray", App.MycartArray));
                // new CallRequests(instance).make_payment(jSonLog);


            }

        } else {


            new AlertDialog.Builder(getActivity())
                    .setTitle("Login?")
                    .setMessage("Please login to continue ")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            Intent i = new Intent(getActivity(), LoginActivity.class);
                            startActivity(i);
                            getActivity().finishAffinity();
                            //  getActivity().moveTaskToBack(true);
                        }
                    }).create().show();

        }

    }

    public static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Log.i("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else {
            Log.i("TAG " + tag + " -->", str);
        }
    }
}

