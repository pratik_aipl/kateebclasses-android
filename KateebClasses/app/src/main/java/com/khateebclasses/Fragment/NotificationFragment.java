package com.khateebclasses.Fragment;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.khateebclasses.App;
import com.khateebclasses.Model.Notification;
import com.khateebclasses.R;
import com.khateebclasses.Utils.CursorParserUniversal;
import com.khateebclasses.Utils.JsonParserUniversal;
import com.khateebclasses.Utils.MyDBManagerOneSignal;
import com.khateebclasses.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.TimeZone;

import static com.khateebclasses.DashboardActivity.changeFragment;
import static com.khateebclasses.DashboardActivity.tv_title;

/**
 * Created by Karan - Empiere on 5/14/2018.
 */

public class NotificationFragment extends Fragment {

    public View view;
    public NotificationFragment instance;
    public RecyclerView rcyclerView;
    public Notification announcement;
    public AnnounceAdapter adapter;
    public JsonParserUniversal jParser;
    private LinearLayout emptyView;
    public TextView tv_empty;
    public static MyDBManagerOneSignal mDbOneSignal;
    public ArrayList<Notification> notiArray = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        instance = this;
        tv_title.setText("Notification");
        jParser = new JsonParserUniversal();
        mDbOneSignal = MyDBManagerOneSignal.getInstance(getActivity());
        mDbOneSignal.open(getActivity());
        rcyclerView = view.findViewById(R.id.rcyclerView);
        setHasOptionsMenu(true);

        emptyView = view.findViewById(R.id.empty_view);
        tv_empty = view.findViewById(R.id.tv_empty);
        emptyView.setVisibility(View.GONE);


        // mDbOneSignal.dbQuery("UPDATE notification SET opened = 1");
        App.notiCount = 0;
        new updateRecorrd(getActivity()).execute();


        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.getItem(1).setVisible(false);
    }


    public class AnnounceAdapter extends RecyclerView.Adapter<AnnounceAdapter.MyViewHolder> {

        public ArrayList<Notification> announcementArrayList;

        @Override
        public AnnounceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_announce_raw, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(AnnounceAdapter.MyViewHolder holder, final int position) {
            final Notification announcement = announcementArrayList.get(position);
            String type = "";
            try {
                JSONObject jObj = new JSONObject(announcement.getFull_data());
                String custom = jObj.getString("custom");
                JSONObject jcustom = new JSONObject(custom);
                String a = jcustom.getString("a");
                JSONObject jtype = new JSONObject(a);
                type = jtype.getString("type");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (type.equals("Event")) {
                holder.lin_type.setVisibility(View.VISIBLE);
                holder.img_type.setImageDrawable(getResources().getDrawable(R.drawable.event_notify));
                holder.tv_type.setText("Event");

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        changeFragment(new OverEventsFragment(), true);
                    }
                });
            } else if (type.equals("Announcement")) {
                holder.lin_type.setVisibility(View.VISIBLE);
                holder.img_type.setImageDrawable(getResources().getDrawable(R.drawable.announce_notify));
                holder.tv_type.setText("Announcement");
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        changeFragment(new AnnounceFragment(), true);

                    }
                });
            } else {
                holder.lin_type.setVisibility(View.GONE);

            }
            holder.tv_title.setText(announcement.getMessage().toString());

            try {
                JSONObject jObj = new JSONObject(announcement.getFull_data());
                Log.i("TAG", "MILI SECOND :-> " + jObj.getString("google.sent_time"));

                String date = getDate(Long.parseLong(jObj.getString("google.sent_time")), "yyyy-MM-dd HH:mm:ss");
                holder.tv_date.setText(printDifference(date));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                if (notiArray.get(position).getCollapse_id() != null) {
                    if (notiArray.get(position).getCollapse_id().equals("1")) {
                        holder.rel_new.setVisibility(View.VISIBLE);
                    } else {
                        holder.rel_new.setVisibility(View.GONE);
                    }
                } else {
                    holder.rel_new.setVisibility(View.GONE);
                }
            } catch (Exception e) {

                e.printStackTrace();
            }


        }

        @Override
        public int getItemCount() {
            return announcementArrayList.size();
        }

        public AnnounceAdapter(ArrayList<Notification> featuredJobList) {
            this.announcementArrayList = featuredJobList;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tv_title, tv_date;
            public RelativeLayout rel_new;
            public LinearLayout lin_type;
            public ImageView img_type;
            public TextView tv_type;

            public MyViewHolder(View view) {
                super(view);

                tv_title = (TextView) view.findViewById(R.id.tv_title);
                tv_date = (TextView) view.findViewById(R.id.tv_date);
                rel_new = view.findViewById(R.id.rel_new);
                tv_type = view.findViewById(R.id.tv_type);
                img_type = view.findViewById(R.id.img_type);
                lin_type = view.findViewById(R.id.lin_type);


            }


        }

        public CharSequence printDifference(String start) {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            long time = 0;

            try {
                time = sdf.parse(start).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long now = System.currentTimeMillis();

            CharSequence ago =
                    DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
            return ago;
        }

        public String convertDate(String dateInMilliseconds, String dateFormat) {
            return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
        }
    }

    public class CustomDatabaseQuery extends AsyncTask<String, ArrayList<Object>, ArrayList<Object>> {

        public MyDBManagerOneSignal mDb;
        public CursorParserUniversal cParse;

        public Context ct;
        public Object obj;

        public CustomDatabaseQuery(Context ct, Object obj) {
            this.ct = ct;
            this.obj = obj;

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDb = MyDBManagerOneSignal.getInstance(ct);
            mDb.open(ct);
            this.cParse = new CursorParserUniversal();


        }

        @Override
        protected ArrayList<Object> doInBackground(String... id) {
            // Log.i("Total Rows", " : " + id[0]);


            Cursor c = mDb.getAllRows(id[0]);


            // System.out.println("Total Rows : " + id[0]);

            ArrayList<Object> objArray = new ArrayList<>();

            if (c != null && c.moveToFirst()) {
                do {

                    // System.out.println("Total Rows on onPreExecute class");

                    Class cl;
                    try {
                        cl = Class.forName(obj.getClass().getCanonicalName(), false, obj.getClass().getClassLoader());
                        obj = cl.newInstance();
                        obj = (Object) cParse.parseCursor(c, obj);

                        objArray.add(obj);

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                } while (c.moveToNext());

                // Log.v("Cursor Object", DatabaseUtils.dumpCursorToString(c));
                c.close();
            } else {

                //  Log.i("Total Rows", " : Not any SubjectAccuracy Found from");
                //  Utils.showToast("Not any SubjectAccuracy Found from : ", ct);
            }

            return objArray;

        }

        @Override
        protected void onPostExecute(ArrayList<Object> objects) {

            super.onPostExecute(objects);
        }


        public ArrayList<Object> executeOnExecutor(String[] strings) {
            ArrayList<Object> objArray = new ArrayList<>();

            return objArray;
        }
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public class updateRecorrd extends AsyncTask<String, String, String> {


        Context context;
        public MyDBManagerOneSignal mDbOneSignal;
        public App app;
        public CursorParserUniversal cParse;
        public String SubjectName = "", paper_type = "", created_on = "", notification_type_id = "", type = "";


        private updateRecorrd(Context context) {

            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();

            mDbOneSignal = MyDBManagerOneSignal.getInstance(context);
            mDbOneSignal.open(context);
            notiArray.clear();
        }

        @Override
        protected void onPreExecute() {

            notiArray.clear();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... id) {

            //    int N_ID = mDbOneSignal.getLastRecordIDForRequest("notification", "OneSignalID");

            Cursor noti = mDbOneSignal.getAllRows("Select * from notification " /*where _id > " + N_ID*/);
            Log.d("TAG", "Cursor :->" + DatabaseUtils.dumpCursorToString(noti));

            if (noti != null && noti.moveToFirst()) {
                do {
                    Notification notiObj = (Notification) cParse.parseCursor(noti, new Notification());
                    notiArray.add(notiObj);
                } while (noti.moveToNext());
                noti.close();
            }

            if (notiArray != null && notiArray.size() > 0) {
                for (int i = 0; i < notiArray.size(); i++) {
                    try {
                        if (notiArray.get(i).getOpened().equals("0")) {
                            JSONObject jObj = new JSONObject(notiArray.get(i).getFull_data());
                            String custom = jObj.getString("custom");
                            JSONObject jcustom = new JSONObject(custom);
                            String a = jcustom.getString("a");
                            JSONObject jtype = new JSONObject(a);
                            type = jtype.getString("type");
                            notiArray.get(i).setType(type);
                            if (type.equalsIgnoreCase("Announcement") || type.equalsIgnoreCase("Event")) {

                                notiArray.get(i).setCollapse_id("1");
                                notiArray.get(i).setOpened("0");
                                mDbOneSignal.dbQuery("UPDATE notification SET opened = 0, collapse_id=1 where _id=" + notiArray.get(i).get_id());
                            } else {
                                notiArray.get(i).setCollapse_id("2");
                                notiArray.get(i).setOpened("1");
                                mDbOneSignal.dbQuery("UPDATE notification SET opened = 1 where _id=" + notiArray.get(i).get_id());
                            }
                        }

                    } catch (Exception e) {
                        notiArray.get(i).setCollapse_id("2");
                        notiArray.get(i).setOpened("1");
                        mDbOneSignal.dbQuery("UPDATE notification SET opened = 1 where _id=" + notiArray.get(i).get_id());

                        e.printStackTrace();
                    }
                }
            }


            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
           /* try {

                //  System.out.println("Total Rows on try");

                anousArray = (ArrayList) new CustomDatabaseQuery(getActivity(), new Notification())
                        .execute(new String[]{"Select * from notification"}).get();
                Collections.reverse(anousArray);


            } catch (InterruptedException e) {

                e.printStackTrace();
            } catch (ExecutionException e) {

                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            if (notiArray.size() > 0) {
                Collections.reverse(notiArray);

                adapter = new AnnounceAdapter(notiArray);
                final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);

                rcyclerView.setLayoutAnimation(controller);

                rcyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                rcyclerView.setItemAnimator(new DefaultItemAnimator());
                rcyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                Utils.hideProgressDialog();
                //  new CallRequests(AnnounceFragment.this).get_announcement();
            } else {
                rcyclerView.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
            }
        }
    }

}
