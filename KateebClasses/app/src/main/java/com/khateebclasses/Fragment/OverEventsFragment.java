package com.khateebclasses.Fragment;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khateebclasses.App;
import com.khateebclasses.Model.Notification;
import com.khateebclasses.R;
import com.khateebclasses.Adapter.OurEventsAdapter;
import com.khateebclasses.Interface.AsynchTaskListner;
import com.khateebclasses.Model.OurEvent;
import com.khateebclasses.Utils.CallRequests;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.CursorParserUniversal;
import com.khateebclasses.Utils.JsonParserUniversal;
import com.khateebclasses.Utils.MyDBManagerOneSignal;
import com.khateebclasses.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.khateebclasses.DashboardActivity.countTextViewNoti;
import static com.khateebclasses.DashboardActivity.redCircleNoti;
import static com.khateebclasses.DashboardActivity.tv_title;

public class OverEventsFragment extends Fragment implements AsynchTaskListner {

    public View view;
    public OverEventsFragment instance;
    public RecyclerView recyclerEvent;
    public ArrayList<OurEvent> ourEventArrayList = new ArrayList<>();
    public OurEvent ourEvent;
    public OurEventsAdapter adapter;
    public JsonParserUniversal jParser;
    private LinearLayout emptyView;
    public TextView tv_empty;
    public ArrayList<Notification> notiArray = new ArrayList<>();
    public static MyDBManagerOneSignal mDbOneSignal;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        instance = this;
        mDbOneSignal = MyDBManagerOneSignal.getInstance(getActivity());
        mDbOneSignal.open(getActivity());
        jParser = new JsonParserUniversal();
        tv_title.setText("OVER EVENTS");


        recyclerEvent = view.findViewById(R.id.rcyclerView);
        emptyView = view.findViewById(R.id.empty_view);
        tv_empty = view.findViewById(R.id.tv_empty);

        emptyView.setVisibility(View.GONE);
        new getRecorrd(getActivity()).execute();


        new CallRequests(OverEventsFragment.this).get_our_events();

        return view;
    }

    public static void getNotificationCount() {
        Cursor c = mDbOneSignal.getAllRows("Select * from notification where opened=0 ifnull(collapse_id, '') = ''");

        // Log.d("TAG", "Cursor :->" + DatabaseUtils.dumpCursorToString(c));
        // Log.i("TAG", "Query :-> Select from notification where opened=0");
        if (c != null && c.moveToFirst()) {
            App.notiCount = c.getCount();
            Log.i("TAG", "In IF :-> " + App.notiCount);
            c.close();
        } else {
            App.notiCount = 0;
        }


    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            switch (request) {

                case get_our_events:

                    ourEventArrayList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("Status").equals("Success")) {
                            if (jObj.getJSONArray("records") != null && jObj.getJSONArray("records").length() > 0) {
                                JSONArray jDataArray = jObj.getJSONArray("records");
                                if (jDataArray != null && jDataArray.length() > 0) {

                                    for (int i = 0; i < jDataArray.length(); i++) {
                                        JSONObject jOurEvent = jDataArray.getJSONObject(i);
                                        ourEvent = (OurEvent) jParser.parseJson(jOurEvent, new OurEvent());
                                        ourEvent.setEventID(jOurEvent.getInt("EventID"));
                                        ourEventArrayList.add(ourEvent);
                                    }

                                    adapter = new OurEventsAdapter(ourEventArrayList);
                                    final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);

                                    recyclerEvent.setLayoutAnimation(controller);

                                    recyclerEvent.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                    recyclerEvent.setItemAnimator(new DefaultItemAnimator());
                                    recyclerEvent.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                    Utils.hideProgressDialog();
                                }
                            } else {
                                Utils.hideProgressDialog();
                                recyclerEvent.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                            }
                        } else {
                            Utils.hideProgressDialog();
                            recyclerEvent.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);

                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    public class getRecorrd extends AsyncTask<String, String, String> {


        Context context;
        public MyDBManagerOneSignal mDbOneSignal;
        public App app;
        public CursorParserUniversal cParse;
        public String SubjectName = "", paper_type = "", created_on = "", notification_type_id = "", type = "";


        private getRecorrd(Context context) {

            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();

            mDbOneSignal = MyDBManagerOneSignal.getInstance(context);
            mDbOneSignal.open(context);

        }

        @Override
        protected void onPreExecute() {


            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... id) {

            //    int N_ID = mDbOneSignal.getLastRecordIDForRequest("notification", "OneSignalID");

            Cursor noti = mDbOneSignal.getAllRows("Select * from notification where opened=0" /*where _id > " + N_ID*/);
            Log.d("TAG", "Cursor :->" + DatabaseUtils.dumpCursorToString(noti));

            if (noti != null && noti.moveToFirst()) {
                do {
                    Notification notiObj = (Notification) cParse.parseCursor(noti, new Notification());
                    notiArray.add(notiObj);
                } while (noti.moveToNext());
                noti.close();
            }

            if (notiArray != null && notiArray.size() > 0) {
                for (int i = 0; i < notiArray.size(); i++) {
                    try {
                        JSONObject jObj = new JSONObject(notiArray.get(i).getFull_data());
                        String custom = jObj.getString("custom");
                        JSONObject jcustom = new JSONObject(custom);
                        String a = jcustom.getString("a");
                        JSONObject jtype = new JSONObject(a);
                        type = jtype.getString("type");
                        if (type.equalsIgnoreCase("Event")) {
                            App.notiCount--;
                            mDbOneSignal.dbQuery("UPDATE notification SET opened = 1, collapse_id=0 where _id=" + notiArray.get(i).get_id());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (App.notiCount == 0) {
                redCircleNoti.setVisibility(View.GONE);
            } else {
                countTextViewNoti.setText(String.valueOf(App.notiCount));
            }



        }
    }


}