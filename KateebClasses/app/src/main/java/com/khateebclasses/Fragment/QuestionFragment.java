package com.khateebclasses.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.khateebclasses.App;
import com.khateebclasses.McqTestActivity;
import com.khateebclasses.Model.McqTestMain;
import com.khateebclasses.R;
import com.khateebclasses.Utils.Constant;
import com.squareup.picasso.Picasso;

import static com.khateebclasses.McqTestActivity.TestMainArray;
import static com.khateebclasses.McqTestActivity.tabLayout;

public class QuestionFragment extends Fragment {
    public View v;
    public static QuestionFragment fragment;
    private static final String ARG_PAGE_NUMBER = "page_number";
    public int pageNumber = 0;
    public App app;
    public LinearLayout lin_a, lin_b, lin_c, lin_d;
    public TextView tv_q, tv_a, tv_b, tv_c, tv_d, ans_a, ans_b, ans_c, ans_d;
    public ImageView img_que, img_a, img_b, img_c, img_d;
    public AQuery aQuery;
    public McqTestMain myQuestion;
    private boolean isViewShown = false;
    ProgressBar pbar_a, pbar_b, pbar_c, pbar_d, pbar_q;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_question, container, false);
        app = App.getInstance();
        aQuery = new AQuery(getActivity());
        myQuestion = TestMainArray.get(pageNumber);
        FindElement();

        if (!isViewShown) {

            setListners();
            setData();
        }

        return v;
    }

    public void FindElement() {

        lin_a = (LinearLayout) v.findViewById(R.id.lin_a);
        lin_b = (LinearLayout) v.findViewById(R.id.lin_b);
        lin_c = (LinearLayout) v.findViewById(R.id.lin_c);
        lin_d = (LinearLayout) v.findViewById(R.id.lin_d);

        pbar_a = v.findViewById(R.id.pbar_a);
        pbar_b = v.findViewById(R.id.pbar_b);
        pbar_c = v.findViewById(R.id.pbar_c);
        pbar_d = v.findViewById(R.id.pbar_d);
        pbar_q = v.findViewById(R.id.pbar_q);

        tv_q = (TextView) v.findViewById(R.id.tv_q);
        tv_a = (TextView) v.findViewById(R.id.tv_a);
        tv_b = (TextView) v.findViewById(R.id.tv_b);
        tv_c = (TextView) v.findViewById(R.id.tv_c);
        tv_d = (TextView) v.findViewById(R.id.tv_d);
        img_que = (ImageView) v.findViewById(R.id.img_que);
        img_a = (ImageView) v.findViewById(R.id.img_a);
        img_b = (ImageView) v.findViewById(R.id.img_b);
        img_c = (ImageView) v.findViewById(R.id.img_c);
        img_d = (ImageView) v.findViewById(R.id.img_d);
        ans_a = (TextView) v.findViewById(R.id.a_ans);
        ans_b = (TextView) v.findViewById(R.id.b_ans);
        ans_c = (TextView) v.findViewById(R.id.c_ans);
        ans_d = (TextView) v.findViewById(R.id.d_ans);
    }


    public void setListners() {
        lin_a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_a.setBackgroundResource(R.drawable.circle_fill_blue);

                tv_b.setBackgroundColor(Color.TRANSPARENT);
                tv_a.setTextColor(Color.WHITE);
                tv_b.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.BLACK);
                tv_c.setBackgroundColor(Color.TRANSPARENT);
                tv_d.setBackgroundColor(Color.TRANSPARENT);
                myQuestion.selectedAns = 1;
                TestMainArray.get(pageNumber).isAttemptedd = 1;
                TestMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(0).getAnswerID());
              /*  if (myQuestion.optionArray.get(0).getIsCorrect().equalsIgnoreCase("1")) {
                    McqTestActivity.TestMainArray.get(pageNumber).isRightt = true;
                    McqTestActivity.TestMainArray.get(pageNumber).optionArray.get(0).isRight = true;
                }
         */
                selectAns();

            }
        });
        ans_a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_a.setBackgroundResource(R.drawable.circle_fill_blue);

                tv_b.setBackgroundColor(Color.TRANSPARENT);
                tv_a.setTextColor(Color.WHITE);
                tv_b.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.BLACK);
                tv_c.setBackgroundColor(Color.TRANSPARENT);
                tv_d.setBackgroundColor(Color.TRANSPARENT);
                myQuestion.selectedAns = 1;
                TestMainArray.get(pageNumber).isAttemptedd = 1;
                TestMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(0).getAnswerID());
             /*     if (myQuestion.optionArray.get(0).getIsCorrect().equalsIgnoreCase("1")) {
                    McqTestActivity.TestMainArray.get(pageNumber).isRightt = true;
                    McqTestActivity.TestMainArray.get(pageNumber).optionArray.get(0).isRight = true;
                }
              */
                selectAns();

            }
        });

        lin_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_b.setBackgroundResource(R.drawable.circle_fill_blue);
                tv_a.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.BLACK);
                tv_b.setTextColor(Color.WHITE);
                tv_a.setBackgroundColor(Color.TRANSPARENT);
                tv_c.setBackgroundColor(Color.TRANSPARENT);
                tv_d.setBackgroundColor(Color.TRANSPARENT);
                myQuestion.selectedAns = 2;
                TestMainArray.get(pageNumber).isAttemptedd = 2;
                TestMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(1).getAnswerID());
             /*   if (myQuestion.optionArray.get(1).getIsCorrect().equalsIgnoreCase("1")) {
                    McqTestActivity.TestMainArray.get(pageNumber).isRightt = true;
                    McqTestActivity.TestMainArray.get(pageNumber).optionArray.get(1).isRight = true;
                }
              */
                selectAns();
            }


        });
        ans_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_b.setBackgroundResource(R.drawable.circle_fill_blue);
                tv_a.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.BLACK);
                tv_b.setTextColor(Color.WHITE);
                tv_a.setBackgroundColor(Color.TRANSPARENT);
                tv_c.setBackgroundColor(Color.TRANSPARENT);
                tv_d.setBackgroundColor(Color.TRANSPARENT);
                myQuestion.selectedAns = 2;
                TestMainArray.get(pageNumber).isAttemptedd = 2;
                TestMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(1).getAnswerID());
            /*  if (myQuestion.optionArray.get(1).getIsCorrect().equalsIgnoreCase("1")) {
                    McqTestActivity.TestMainArray.get(pageNumber).isRightt = true;
                    McqTestActivity.TestMainArray.get(pageNumber).optionArray.get(1).isRight = true;
                }*/

                selectAns();
            }


        });


        lin_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_c.setBackgroundResource(R.drawable.circle_fill_blue);
                tv_a.setBackgroundColor(Color.TRANSPARENT);
                tv_b.setTextColor(Color.BLACK);
                tv_a.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.WHITE);
                tv_b.setBackgroundColor(Color.TRANSPARENT);
                tv_d.setBackgroundColor(Color.TRANSPARENT);
                myQuestion.selectedAns = 3;
                TestMainArray.get(pageNumber).isAttemptedd = 3;
                TestMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(2).getAnswerID());

              /*  if (myQuestion.optionArray.get(2).getIsCorrect().equalsIgnoreCase("1")) {
                    McqTestActivity.TestMainArray.get(pageNumber).isRightt = true;
                    McqTestActivity.TestMainArray.get(pageNumber).optionArray.get(2).isRight = true;
                }
               */
                selectAns();
            }
        });
        ans_c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_c.setBackgroundResource(R.drawable.circle_fill_blue);
                tv_a.setBackgroundColor(Color.TRANSPARENT);
                tv_b.setTextColor(Color.BLACK);
                tv_a.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.WHITE);
                tv_b.setBackgroundColor(Color.TRANSPARENT);
                tv_d.setBackgroundColor(Color.TRANSPARENT);
                myQuestion.selectedAns = 3;

                TestMainArray.get(pageNumber).isAttemptedd = 3;
                TestMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(2).getAnswerID());

             /*   if (myQuestion.optionArray.get(2).getIsCorrect().equalsIgnoreCase("1")) {
                    McqTestActivity.TestMainArray.get(pageNumber).isRightt = true;
                    McqTestActivity.TestMainArray.get(pageNumber).optionArray.get(2).isRight = true;
                }
              */
                selectAns();

            }
        });


        lin_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_d.setBackgroundResource(R.drawable.circle_fill_blue);
                tv_b.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.BLACK);
                tv_a.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.WHITE);
                tv_a.setBackgroundColor(Color.TRANSPARENT);
                tv_b.setBackgroundColor(Color.TRANSPARENT);
                tv_c.setBackgroundColor(Color.TRANSPARENT);
                myQuestion.selectedAns = 4;
                TestMainArray.get(pageNumber).isAttemptedd = 4;
                TestMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(3).getAnswerID());

          /*
                if (myQuestion.optionArray.get(3).getIsCorrect().equalsIgnoreCase("1")) {
                    McqTestActivity.TestMainArray.get(pageNumber).isRightt = true;
                    McqTestActivity.TestMainArray.get(pageNumber).optionArray.get(3).isRight = true;
                }
             */
                selectAns();

            }
        });

        ans_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_d.setBackgroundResource(R.drawable.circle_fill_blue);
                tv_b.setTextColor(Color.BLACK);
                tv_c.setTextColor(Color.BLACK);
                tv_a.setTextColor(Color.BLACK);
                tv_d.setTextColor(Color.WHITE);
                tv_a.setBackgroundColor(Color.TRANSPARENT);
                tv_b.setBackgroundColor(Color.TRANSPARENT);
                tv_c.setBackgroundColor(Color.TRANSPARENT);
                myQuestion.selectedAns = 4;
                TestMainArray.get(pageNumber).isAttemptedd = 4;
                TestMainArray.get(pageNumber).setAnswerID(myQuestion.optionArray.get(3).getAnswerID());
                selectAns();
            /*
                if (myQuestion.optionArray.get(3).getIsCorrect().equalsIgnoreCase("1")) {
                    McqTestActivity.TestMainArray.get(pageNumber).isRightt = true;
                    McqTestActivity.TestMainArray.get(pageNumber).optionArray.get(3).isRight = true;
                }*/


            }
        });

    }


    public void selectAns(String selectedAns, String isCorrect) {
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    if (McqTestActivity.viewPager.getCurrentItem() == j) {
                        ((TextView) tabViewChild).setBackground(getResources().getDrawable(R.drawable.circle_fill_blue));
                        ((TextView) tabViewChild).setTextColor(getResources().getColor(R.color.white));
                    }
                }
            }
        }
        myQuestion.isRightt = (isCorrect.equalsIgnoreCase("1")) ? true : false;
    }

    public void selectAns() {
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    if (McqTestActivity.viewPager.getCurrentItem() == j) {
                        ((TextView) tabViewChild).setBackground(getResources().getDrawable(R.drawable.circle_fill_blue));
                        ((TextView) tabViewChild).setTextColor(getResources().getColor(R.color.white));
                        //((TextView) tabViewChild).setPadding(0, 15, 0 , 15);
                        ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tabViewChild.getLayoutParams();
                        //  p.setMargins(20, 0, 20, 0);

                    }
                }
            }
        }

        ((McqTestActivity) this.getActivity()).nextPage();
        //myQuestion.isRightt = (isCorrect.equalsIgnoreCase("1")) ? true : false;
    }

    public void selectAnsResume() {
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    if (!TestMainArray.get(j).getAnswerID().equals("")) {
                        ((TextView) tabViewChild).setBackground(getResources().getDrawable(R.drawable.circle_fill_blue));
                        ((TextView) tabViewChild).setTextColor(getResources().getColor(R.color.white));
                        //((TextView) tabViewChild).setPadding(0, 15, 0 , 15);
                        ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tabViewChild.getLayoutParams();
                        //  p.setMargins(20, 0, 20, 0);

                    }
                }
            }
        }
        //myQuestion.isRightt = (isCorrect.equalsIgnoreCase("1")) ? true : false;
    }

    public QuestionFragment newInstance(int page) {
        fragment = new QuestionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE_NUMBER, page);
        fragment.setArguments(args);
        fragment.setHasOptionsMenu(true);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageNumber = getArguments().getInt(ARG_PAGE_NUMBER);

    }

    public void setData() {
        tv_q.setText((pageNumber + 1) + ".");
        if (!myQuestion.getQueImage().equals("")) {

            Picasso.get()
                    .load(Constant.IMAGE_PREFIX + myQuestion.getQueImage())
                    .error(R.drawable.ic_manu)
                    .into(img_que);
            //  Log.i("TAG", "IMG URL :-> " + Constant.IMAGE_PREFIX + myQuestion.getQueImage());
        } else {
            pbar_q.setVisibility(View.GONE);
            img_que.setVisibility(View.GONE);
        }

        tv_q.setText(Html.fromHtml(myQuestion.getQuestionDesc()));

        ans_a.setText(myQuestion.optionArray.get(0).getAnswerDesc());
        if (!myQuestion.optionArray.get(0).getAnswerImg().equals("")) {
            Picasso.get()
                    .load(Constant.IMAGE_PREFIX + myQuestion.optionArray.get(0).getAnswerImg())
                    .error(R.drawable.ic_manu)
                    .into(img_a);
            Log.i("TAG", "IMG URL A:-> " + Constant.IMAGE_PREFIX + myQuestion.optionArray.get(0).getAnswerImg());
        } else {
            pbar_a.setVisibility(View.GONE);
            img_a.setVisibility(View.GONE);
        }

        ans_b.setText(Html.fromHtml(myQuestion.optionArray.get(1).getAnswerDesc()).toString().trim());
        if (!myQuestion.optionArray.get(0).getAnswerImg().equals("")) {
            Picasso.get()
                    .load(Constant.IMAGE_PREFIX + myQuestion.optionArray.get(1).getAnswerImg())
                    .error(R.drawable.ic_manu)
                    .into(img_b);

        } else {
            pbar_b.setVisibility(View.GONE);
            img_b.setVisibility(View.GONE);
        }

        ans_c.setText(Html.fromHtml(myQuestion.optionArray.get(2).getAnswerDesc()).toString().trim());
        if (!myQuestion.optionArray.get(2).getAnswerImg().equals("")) {
            Picasso.get()
                    .load(Constant.IMAGE_PREFIX + myQuestion.optionArray.get(2).getAnswerImg())
                    .error(R.drawable.ic_manu)
                    .into(img_c);
            Log.i("TAG", "text C :-> " + myQuestion.optionArray.get(2).getAnswerImg());
            Log.i("TAG", "IMG URL C:-> " + Constant.IMAGE_PREFIX + myQuestion.optionArray.get(2).getAnswerImg());

        } else {
            pbar_c.setVisibility(View.GONE);
            img_c.setVisibility(View.GONE);
        }


        ans_d.setText(Html.fromHtml(myQuestion.optionArray.get(3).getAnswerDesc()).toString().trim());
        if (!myQuestion.optionArray.get(3).getAnswerImg().equals("")) {
            Picasso.get()
                    .load(Constant.IMAGE_PREFIX + myQuestion.optionArray.get(3).getAnswerImg())
                    .error(R.drawable.ic_manu)
                    .into(img_d);
            Log.i("TAG", "IMG URL D:-> " + Constant.IMAGE_PREFIX + myQuestion.optionArray.get(3).getAnswerImg());

        } else {
            pbar_d.setVisibility(View.GONE);
            img_d.setVisibility(View.GONE);
        }


        if (myQuestion.selectedAns != -1) {
            switch (myQuestion.selectedAns) {
                case 1:
                    tv_a.setBackgroundResource(R.drawable.circle_fill_blue);

                    tv_b.setBackgroundColor(Color.TRANSPARENT);
                    tv_a.setTextColor(Color.WHITE);
                    tv_b.setTextColor(Color.BLACK);
                    tv_c.setTextColor(Color.BLACK);
                    tv_d.setTextColor(Color.BLACK);
                    tv_c.setBackgroundColor(Color.TRANSPARENT);
                    tv_d.setBackgroundColor(Color.TRANSPARENT);
                    break;

                case 2:
                    tv_b.setBackgroundResource(R.drawable.circle_fill_blue);
                    tv_a.setTextColor(Color.BLACK);
                    tv_c.setTextColor(Color.BLACK);
                    tv_d.setTextColor(Color.BLACK);
                    tv_b.setTextColor(Color.WHITE);
                    tv_a.setBackgroundColor(Color.TRANSPARENT);
                    tv_c.setBackgroundColor(Color.TRANSPARENT);
                    tv_d.setBackgroundColor(Color.TRANSPARENT);
                    break;
                case 3:
                    tv_c.setBackgroundResource(R.drawable.circle_fill_blue);
                    tv_a.setBackgroundColor(Color.TRANSPARENT);
                    tv_b.setTextColor(Color.BLACK);
                    tv_a.setTextColor(Color.BLACK);
                    tv_d.setTextColor(Color.BLACK);
                    tv_c.setTextColor(Color.WHITE);
                    tv_b.setBackgroundColor(Color.TRANSPARENT);
                    tv_d.setBackgroundColor(Color.TRANSPARENT);
                    break;
                case 4:
                    tv_d.setBackgroundResource(R.drawable.circle_fill_blue);
                    tv_b.setTextColor(Color.BLACK);
                    tv_c.setTextColor(Color.BLACK);
                    tv_a.setTextColor(Color.BLACK);
                    tv_d.setTextColor(Color.WHITE);
                    tv_a.setBackgroundColor(Color.TRANSPARENT);
                    tv_b.setBackgroundColor(Color.TRANSPARENT);
                    tv_c.setBackgroundColor(Color.TRANSPARENT);
                    break;
            }
        }
        selectAnsResume();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (getView() != null) {
            isViewShown = true;
            setListners();
            setData();
        } else {
            isViewShown = false;
        }


    }

}