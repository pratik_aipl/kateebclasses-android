package com.khateebclasses.Fragment;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.khateebclasses.App;
import com.khateebclasses.Interface.AsynchTaskListner;
import com.khateebclasses.R;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.JsonParserUniversal;
import com.khateebclasses.Utils.Utils;

import static com.khateebclasses.DashboardActivity.tv_title;

/**
 * Created by Karan - Empiere on 4/29/2018.
 */

public class WebviewFragment extends Fragment implements AsynchTaskListner {
    public View view;
    public WebviewFragment instance;
    public JsonParserUniversal jParser;
    public WebView web_view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_webview, container, false);
        instance = this;
        jParser = new JsonParserUniversal();
        tv_title.setText(App.WebviewTitle);
        web_view = view.findViewById(R.id.web_view);


        WebSettings settings = web_view.getSettings();
        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccessFromFileURLs(false);
        settings.setAllowUniversalAccessFromFileURLs(false);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setBuiltInZoomControls(true);
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        settings.setDisplayZoomControls(false);
        settings.setDomStorageEnabled(true);
        web_view.setWebViewClient(new myWebClient());
        if (App.WebviewType.equals("PDF")) {
            web_view.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=" + App.WebviewURL);
            //   Log.i("TAG","URL:-> "+"http://drive.google.com/viewerng/viewer?embedded=true&url=" + App.WebviewURL);
        } else {
            Utils.showProgressDialog(getActivity());
            web_view.loadUrl(App.WebviewURL);
        }
        // pdfview.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");


        return view;
    }

    private class myWebClient extends WebViewClient {
        ProgressDialog progressDialog;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            view.loadUrl(url);
            return true;

        }

        public void onLoadResource(WebView view, String url) {

        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            try {
                Utils.hideProgressDialog();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

    }
}
