package com.khateebclasses.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.khateebclasses.R;

import static com.khateebclasses.DashboardActivity.tv_title;

/**
 * Created by Karan - Empiere on 4/14/2018.
 */

public class termsFragment extends Fragment {
    public View view;
    public termsFragment instance;
    public WebView web_about;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_webview, container, false);
        instance = this;
        tv_title.setText(R.string.terms);
        web_about = view.findViewById(R.id.web_view);
        web_about.setVisibility(View.VISIBLE);
        web_about.loadUrl("file:///android_asset/terms&condition.html");
        return view;
    }
}
