package com.khateebclasses.Interface;


import com.khateebclasses.Utils.Constant;

/**
 * Created by admin on 10/12/2016.
 */
public interface AsynchTaskListner {
    public void onTaskCompleted(String result, Constant.REQUESTS request);
}
