package com.khateebclasses;

import android.content.Intent;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.khateebclasses.Adapter.MyViewPagerAdapter;
import com.khateebclasses.Model.Intro;
import com.khateebclasses.Utils.Utils;

import io.fabric.sdk.android.Fabric;

public class IntroActivity extends AppCompatActivity {
    ViewPager viewPager;
    public LinearLayout pager_indicator, lin_next;
    private ImageView[] dots;
    public TextView tv_login;
    public Button btn_joinNow;
    private int dotsCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_intro);
        Utils.logUser();


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tv_login = (TextView) findViewById(R.id.tv_login);
        String loginText="Have an account? <font color=\"#e79902\">Login</font>";
        tv_login.setText(Html.fromHtml(loginText), TextView.BufferType.SPANNABLE);
        btn_joinNow = (Button) findViewById(R.id.btn_joinNow);
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);
        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(IntroActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
        btn_joinNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(IntroActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
        viewPager.setAdapter(new MyViewPagerAdapter(this));

        setUiPageViewController();

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));
                }
                dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void setUiPageViewController() {

        dotsCount = Intro.values().length;
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(8, 0, 8, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
    }

}
