package com.khateebclasses;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.khateebclasses.Interface.AsynchTaskListner;
import com.khateebclasses.Model.MainUser;
import com.khateebclasses.Utils.CallRequests;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.JsonParserUniversal;
import com.khateebclasses.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity implements AsynchTaskListner {
    public LoginActivity instance;
    public TextView tv_signup, tv_forgotPass, tv_skip;
    public EditText et_pass, et_email;
    public SharedPreferences shared;
    public JsonParserUniversal jParser;
    public App app;
    public MainUser user;
    public Button tv_login;
    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
    int PERMISSION_ALL = 1;
    public String PleyaerID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_login);
        Utils.logUser();
        instance = this;
        app = App.getInstance();
        jParser = new JsonParserUniversal();
        PleyaerID = Utils.OneSignalPlearID();
        shared = getSharedPreferences(getPackageName(), 0);
        if (!Utils.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
        FindElimants();
        //  et_email.setText("karanpitroda1012@gmail.com");
        // et_pass.setText("67890");
        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //new CallRequests(instance).getRSA();
                Validation();
            }
        });

        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, DashboardActivity.class));
            }
        });
        tv_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, RegisterActivity.class));
            }
        });
        tv_forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(instance, ForgotPassActivity.class));

            }
        });
    }

    public void FindElimants() {
        tv_signup = findViewById(R.id.tv_signup);
        tv_forgotPass = findViewById(R.id.tv_forgotPass);
        tv_login = findViewById(R.id.tv_login);
        et_pass = findViewById(R.id.et_pass);
        et_email = findViewById(R.id.et_email);
        tv_skip = findViewById(R.id.tv_skip);
    }

    public void Validation() {
        if (et_email.getText().toString().equals("") || !Utils.isValidEmail(et_email.getText().toString())) {
            et_email.setError(" Please Enter Valid Email ID");
            et_email.setFocusable(true);
        } else if (et_pass.getText().toString().equals("") && !Utils.Passwordvalidate(et_pass.getText().toString())) {
            et_pass.setError("Please Enter Valid Password");
            et_pass.setFocusable(true);
        } else {
            //Log.i("TAG", "PleyaerID:-> " + PleyaerID);
            new CallRequests(instance).getLogin(et_email.getText().toString(), et_pass.getText().toString(), PleyaerID);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT ==> ", result);
            switch (request) {
                case getLogin:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getString("Status").equals("Success")) {
                            if (jObj.getJSONArray("records") != null && jObj.getJSONArray("records").length() > 0) {
                                JSONArray jData = jObj.getJSONArray("records");
                                for (int i = 0; i < jData.length(); i++) {

                                    JSONObject jUser = jData.getJSONObject(i);
                                    user = (MainUser) jParser.parseJson(jUser, new MainUser());
                                    app.user = user;
                                    app.IS_LOGGED = true;

                                    SharedPreferences.Editor et = shared.edit();
                                    Gson gson = new Gson();
                                    String json = gson.toJson(app.user);
                                    et.putString("user", json);
                                    et.putBoolean(app.IS_LOGGED_IN, true);
                                    et.commit();
                                    startActivity(new Intent(instance, DashboardActivity.class));
                                    finish();
                                }
                            } else {
                                Utils.showToast("Something getting wrong! Please try again later.", instance);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }
                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;
                case forgot_password:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getString("Status").equals("Success")) {
                            Utils.showToast(jObj.getString("message"), instance);

                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }
                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;

                case getRSA:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("Status").equals("Success")) {
                            String vResponse = jObj.getString("message");
                            // vResponse = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0GEtvHjdez44e5/OpDEjJW+zHHvRKbJmtn4PKok+7nDVaodOqtZt6u0brOwDhGKfd76ytRbRsaasveTi8RUFAAtX33yECvDXgI2cR+AnzQYWDJxiVWID+iD/YecLkPcmBkyfso5uBjhEUBdHz4vsZXdm+yRwiFCJAcwfbhws9LtIN49i3PEGQ5MdAaIbhuVuDOEsGy1yKwpsj5YLbRrfJXEgGckZd8OMTGg4UGxSmPBjMU3vHimflP9Ks18On3kendHPaRoT/pVtm5JI9kKXBeTERWvaAPM+bP/1fgFjB+qLqaugaVdE8pGR14lGGSGjwzsSN5CfuDY0YBMSfZtAawIDAQAB";

                            System.out.println(vResponse);


                        }
                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
