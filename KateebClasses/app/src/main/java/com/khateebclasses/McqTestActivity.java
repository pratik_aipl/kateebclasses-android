package com.khateebclasses;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.khateebclasses.Adapter.QuestionAdapter;
import com.khateebclasses.Fragment.QuestionFragment;
import com.khateebclasses.Interface.AsynchTaskListner;
import com.khateebclasses.Model.Header;
import com.khateebclasses.Model.McqTestMain;
import com.khateebclasses.Model.Options;
import com.khateebclasses.Model.Result;
import com.khateebclasses.Model.SubmitAnswers;
import com.khateebclasses.Model.SubmitData;
import com.khateebclasses.Utils.CallRequests;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.CustPagerTransformer;
import com.khateebclasses.Utils.JsonParserUniversal;
import com.khateebclasses.Utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class McqTestActivity extends AppCompatActivity implements AsynchTaskListner, Animator.AnimatorListener {
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public QuestionAdapter adapter;
    public ArrayList<QuestionFragment> fragments = new ArrayList<>();
    public static TextView tv;

    public static ArrayList<McqTestMain> TestMainArray = new ArrayList<>();
    public static ArrayList<SubmitData> SubmitArray = new ArrayList<>();
    public McqTestMain mcqTestMain = new McqTestMain();
    public static Toolbar toolbar;
    public static ActionBar actionBar;
    public SharedPreferences shared;
    public JsonParserUniversal jParser;
    public McqTestMain mcqObj;
    public Options optionsObj;
    public App app;
    public TextView btn_submit;
    private boolean mIsInAnimation;
    private long mMotionBeginTime;
    private float mLastMotionX;
    ObjectAnimator anim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_mcq_test);
        Utils.logUser();


        app = App.getInstance();
        jParser = new JsonParserUniversal();
        shared = getSharedPreferences(getPackageName(), 0);

        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);
        btn_submit = findViewById(R.id.btn_submit);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        actionBar = getSupportActionBar();
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().getExtras().getString("start").equals("resume")) {
            TestMainArray = (ArrayList<McqTestMain>) getIntent().getExtras().getSerializable("MCQ");
            setupViewPager();
        } else {
            new CallRequests(this).StartExam();
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubmitData();
            }
        });

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SharedPreferences.Editor et = shared.edit();
        Gson gson = new Gson();
        String json = gson.toJson(TestMainArray);
        et.putString("MCQ", json);
        et.putString("StudentExamHdrID", app.StudentExamHdrID);
        et.commit();
    }

    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences.Editor et = shared.edit();
        Gson gson = new Gson();
        String json = gson.toJson(TestMainArray);
        et.putString("MCQ", json);
        et.putString("StudentExamHdrID", app.StudentExamHdrID);
        et.commit();
    }

    public void nextPage() {
        int Pos = viewPager.getCurrentItem();
        if (Pos < TestMainArray.size()) {
            if (mIsInAnimation) return;
            if (!hasNextPage()) return;
            anim = ObjectAnimator.ofFloat(this, "motionX", 0, -viewPager.getWidth());

            anim.setInterpolator(new LinearInterpolator());
            anim.addListener(this);
            anim.setDuration(300);
            anim.start();

        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case StartExam:
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("Status").equals("Success")) {
                            TestMainArray.clear();
                            if (jObj.getJSONObject("records") != null && jObj.getJSONObject("records").length() > 0) {
                                JSONObject jrecord = jObj.getJSONObject("records");
                                JSONObject jQList = jrecord.getJSONObject("Question_List");
                                app.StudentExamHdrID = jQList.getString("StudentExamHdrID");
                                JSONArray jQuestion = jQList.getJSONArray("Question");
                                for (int i = 0; i < jQuestion.length(); i++) {
                                    JSONObject jpaper = jQuestion.getJSONObject(i);
                                    mcqObj = (McqTestMain) jParser.parseJson(jpaper, new McqTestMain());

                                    JSONArray jQuestion_optionArray = jpaper.getJSONArray("Question_option");
                                    if (jQuestion_optionArray != null && jQuestion_optionArray.length() > 0) {

                                        for (int j = 0; j < jQuestion_optionArray.length(); j++) {
                                            JSONObject joption = jQuestion_optionArray.getJSONObject(j);
                                            optionsObj = (Options) jParser.parseJson(joption, new Options());
                                            mcqObj.optionArray.add(optionsObj);
                                        }
                                    }
                                    TestMainArray.add(mcqObj);
                                }
                                Log.i("TAG", "Arrat Size::--> " + TestMainArray.size());
                                setupViewPager();
                                Utils.hideProgressDialog();
                            }
                        } else {
                            if (jObj.getString("message").contains("Test Already Taken")) {
                                showAlert(jObj.getString("message"));
                            } else {
                                Utils.showToast(jObj.getString("message"), this);
                            }

                            Utils.hideProgressDialog();
                        }
                        break;
                    case SubmitTest:
                        jObj = new JSONObject(result);
                        if (jObj.getString("Status").equals("Success")) {
                            SharedPreferences.Editor et = shared.edit();
                            TestMainArray.clear();
                            App.TestMainArray.clear();
                            et.putString("MCQ", "");
                            et.clear();
                            et.apply();
                            et.commit();
                            Result obj = (Result) jParser.parseJson(jObj, new Result());
                            Intent i = new Intent(McqTestActivity.this, TestResultActivity.class);
                            i.putExtra("obj", obj);
                            startActivity(i);
                            finish();
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                            Utils.hideProgressDialog();
                        }

                        break;

                    case GetTestResult:
                        jObj = new JSONObject(result);
                        if (jObj.getString("Status").equals("Success")) {
                            Result obj = (Result) jParser.parseJson(jObj, new Result());
                            Intent i = new Intent(McqTestActivity.this, TestResultActivity.class);
                            i.putExtra("obj", obj);
                            startActivity(i);
                            finish();
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                            Utils.hideProgressDialog();
                        }

                }


            } catch (JSONException e) {
                Utils.hideProgressDialog();
                e.printStackTrace();
            }
        }
    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        dialog.dismiss();
                        new CallRequests(McqTestActivity.this).GetTestResult();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void setupViewPager() {
        viewPager.setPageTransformer(false, new CustPagerTransformer(this));

        for (int i = 0; i < TestMainArray.size(); i++) {
            fragments.add(new QuestionFragment().newInstance(i));
            tabLayout.addTab(tabLayout.newTab().setText((String.format("%02d", i + 1) + "")));
            TextView tv = (TextView) LayoutInflater.from(this).inflate(R.layout.textview, null);
            //tv.setBackground(getResources().getDrawable(R.drawable.circle_border_blue));
            tabLayout.getTabAt(i).setCustomView(tv);
        }

        adapter = new QuestionAdapter(getSupportFragmentManager(), fragments);
        viewPager.setAdapter(adapter);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        viewPager.setPageMargin(pageMargin);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

    }

    public void SubmitData() {

        SubmitData dataObj = new SubmitData();

        Header hdrObj = new Header();

        hdrObj.setStudent_id(String.valueOf(app.user.getStudentID()));
        hdrObj.setStudentExamHdrID(app.StudentExamHdrID);
        dataObj.header.add(hdrObj);
        for (int i = 0; i < TestMainArray.size(); i++) {
            //for (EvaluterMcqTestMain que : EvalMainArray) {
            SubmitAnswers ansObj = new SubmitAnswers();
            ansObj.setStudentExamDTLID(TestMainArray.get(i).getStudentExamDTLID());
            ansObj.setStudent_id(String.valueOf(app.user.getStudentID()));
            ansObj.setStudentExamHdrID(app.StudentExamHdrID);
            ansObj.setQuestionID(TestMainArray.get(i).getQuestionID());
            if (TestMainArray.get(i).isAttemptedd != -1) {
                ansObj.setIsAttempt("1");
                ansObj.setAnswerID(TestMainArray.get(i).getAnswerID());
            } else {
                ansObj.setIsAttempt("0");
                ansObj.setAnswerID("0");
            }
            dataObj.Answers.add(ansObj);
        }
        SubmitArray.add(dataObj);
        Gson gson = new GsonBuilder().create();
        JsonArray jCctArray = gson.toJsonTree(SubmitArray).getAsJsonArray();
        String jsonString = jCctArray.toString();
        String jSonLog = jsonString.substring(1, jsonString.length() - 1);

        new CallRequests(this).SubmitTest(jSonLog);
    }


    public static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Log.i("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else {
            Log.i("TAG " + tag + " -->", str);
        }
    }

    public void setMotionX(float motionX) {
        if (!mIsInAnimation) return;
        mLastMotionX = motionX;
        final long time = SystemClock.uptimeMillis();
        simulate(MotionEvent.ACTION_MOVE, mMotionBeginTime, time);
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        mIsInAnimation = false;
        final long time = SystemClock.uptimeMillis();
        simulate(MotionEvent.ACTION_UP, mMotionBeginTime, time);
    }

    @Override
    public void onAnimationStart(Animator animation) {
        mLastMotionX = 0;
        mIsInAnimation = true;
        final long time = SystemClock.uptimeMillis();
        simulate(MotionEvent.ACTION_DOWN, time, time);
        mMotionBeginTime = time;
    }

    // method from http://stackoverflow.com/a/11599282/1294681
    private void simulate(int action, long startTime, long endTime) {
        // specify the property for the two touch points
        MotionEvent.PointerProperties[] properties = new MotionEvent.PointerProperties[1];
        MotionEvent.PointerProperties pp = new MotionEvent.PointerProperties();
        pp.id = 0;
        pp.toolType = MotionEvent.TOOL_TYPE_FINGER;

        properties[0] = pp;

        // specify the coordinations of the two touch points
        // NOTE: you MUST set the pressure and size value, or it doesn't work
        MotionEvent.PointerCoords[] pointerCoords = new MotionEvent.PointerCoords[1];
        MotionEvent.PointerCoords pc = new MotionEvent.PointerCoords();
        pc.x = mLastMotionX;
        pc.pressure = 1;
        pc.size = 1;
        pointerCoords[0] = pc;

        final MotionEvent ev = MotionEvent.obtain(
                startTime, endTime, action, 1, properties,
                pointerCoords, 0, 0, 1, 1, 0, 0, 0, 0);

        viewPager.dispatchTouchEvent(ev);
    }

    private boolean hasPrevPage() {
        return viewPager.getCurrentItem() > 0;
    }

    private boolean hasNextPage() {
        return viewPager.getCurrentItem() + 1 < viewPager.getAdapter().getCount();
    }

    @Override
    public void onAnimationCancel(Animator animation) {
    }

    @Override
    public void onAnimationRepeat(Animator animation) {
    }
}
