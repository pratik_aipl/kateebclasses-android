package com.khateebclasses.Model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 4/30/2018.
 */

public class Announcement implements Serializable {
    public String AnnouncementDate = "", AnnDescription = "", AnnouncementType = "", CreatedBy = "", CreatedOn = "", ModifiedBy = "", ModifiedOn = "", EndDate = "", ATypename = "";

   public int  AnnouncementID ;

    public int getAnnouncementID() {
        return AnnouncementID;
    }

    public void setAnnouncementID(int announcementID) {
        AnnouncementID = announcementID;
    }

    public String getAnnouncementDate() {
        return AnnouncementDate;
    }

    public void setAnnouncementDate(String announcementDate) {
        AnnouncementDate = announcementDate;
    }

    public String getAnnDescription() {
        return AnnDescription;
    }

    public void setAnnDescription(String annDescription) {
        AnnDescription = annDescription;
    }

    public String getAnnouncementType() {
        return AnnouncementType;
    }

    public void setAnnouncementType(String announcementType) {
        AnnouncementType = announcementType;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getATypename() {
        return ATypename;
    }

    public void setATypename(String ATypename) {
        this.ATypename = ATypename;
    }
}
