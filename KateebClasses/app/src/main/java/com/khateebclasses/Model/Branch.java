package com.khateebclasses.Model;

import java.io.Serializable;

public class Branch implements Serializable {
    public String BranchName = "";
    public int BranchID ;

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public int getBranchID() {
        return BranchID;
    }

    public void setBranchID(int branchID) {
        BranchID = branchID;
    }
}
