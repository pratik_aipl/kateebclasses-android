package com.khateebclasses.Model;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 4/20/2018.
 */

public class CourseHistory implements Serializable {
    public int StudentID ;
    public int PaymentHdrID ;
    public String TransactionID = "";
    public int PaymentDtlID ;
    public String BatchOrCourse = "";
    public int BatchCourseID;
    public String BatchCode = "";

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        StudentID = studentID;
    }

    public int getPaymentHdrID() {
        return PaymentHdrID;
    }

    public void setPaymentHdrID(int paymentHdrID) {
        PaymentHdrID = paymentHdrID;
    }

    public String getTransactionID() {
        return TransactionID;
    }

    public void setTransactionID(String transactionID) {
        TransactionID = transactionID;
    }

    public int getPaymentDtlID() {
        return PaymentDtlID;
    }

    public void setPaymentDtlID(int paymentDtlID) {
        PaymentDtlID = paymentDtlID;
    }

    public String getBatchOrCourse() {
        return BatchOrCourse;
    }

    public void setBatchOrCourse(String batchOrCourse) {
        BatchOrCourse = batchOrCourse;
    }

    public int getBatchCourseID() {
        return BatchCourseID;
    }

    public void setBatchCourseID(int batchCourseID) {
        BatchCourseID = batchCourseID;
    }

    public String getBatchCode() {
        return BatchCode;
    }

    public void setBatchCode(String batchCode) {
        BatchCode = batchCode;
    }

    public String getBatchType() {
        return BatchType;
    }

    public void setBatchType(String batchType) {
        BatchType = batchType;
    }

    public String getVenueName() {
        return VenueName;
    }

    public void setVenueName(String venueName) {
        VenueName = venueName;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSemesterName() {
        return SemesterName;
    }

    public void setSemesterName(String semesterName) {
        SemesterName = semesterName;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public String getCourseType() {
        return CourseType;
    }

    public void setCourseType(String courseType) {
        CourseType = courseType;
    }

    public String BatchType = "";
    public String VenueName = "";
    public double Amount;
    public String Name = "";
    public String SemesterName = "";
    public String BranchName = "";
    public String CourseType = "";
}
