package com.khateebclasses.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 4/7/2018.
 */

public class DownLoad implements Parcelable {
    public int DownloadID;
    public String DownloadDate = "";
    public String DownloadTitle = "";
    public String DownloadFileURl = "";
    public String CreatedBy = "";
    public String CreatedOn = "";
    public String ModifiedBy = "";
    public String ModifiedOn = "";

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public DownLoad createFromParcel(Parcel in) {
            return new DownLoad(in);
        }

        public DownLoad[] newArray(int size) {
            return new DownLoad[size];
        }
    };

    public DownLoad() {

    }

    // Parcelling part
    public DownLoad(Parcel in) {
        this.DownloadID = in.readInt();
        this.DownloadDate = in.readString();
        this.DownloadTitle = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.DownloadID);
        dest.writeString(this.DownloadDate);
        dest.writeString(this.DownloadTitle);
    }

    public int getDownloadID() {
        return DownloadID;
    }

    public void setDownloadID(int downloadID) {
        DownloadID = downloadID;
    }

    public String getDownloadDate() {
        return DownloadDate;
    }

    public void setDownloadDate(String downloadDate) {
        DownloadDate = downloadDate;
    }

    public String getDownloadTitle() {
        return DownloadTitle;
    }

    public void setDownloadTitle(String downloadTitle) {
        DownloadTitle = downloadTitle;
    }

    public String getDownloadFileURl() {
        return DownloadFileURl;
    }

    public void setDownloadFileURl(String downloadFileURl) {
        DownloadFileURl = downloadFileURl;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }


}
