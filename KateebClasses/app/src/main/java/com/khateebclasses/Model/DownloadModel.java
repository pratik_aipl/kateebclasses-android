package com.khateebclasses.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class DownloadModel implements Parcelable {


    int DownloadFolderFilesID;
    int DownloadFolderID;
    String DownloadFileDate;
    String DownloadFileTitle;
    String DownloadFileURl;

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public DownLoad createFromParcel(Parcel in) {
            return new DownLoad(in);
        }

        public DownLoad[] newArray(int size) {
            return new DownLoad[size];
        }
    };

    public DownloadModel(int downloadFolderFilesID, int downloadFolderID, String downloadFileDate, String downloadFileTitle, String downloadFileURl) {
        DownloadFolderFilesID = downloadFolderFilesID;
        DownloadFolderID = downloadFolderID;
        DownloadFileDate = downloadFileDate;
        DownloadFileTitle = downloadFileTitle;
        DownloadFileURl = downloadFileURl;
    }

    public int getDownloadFolderFilesID() {
        return DownloadFolderFilesID;
    }

    public void setDownloadFolderFilesID(int downloadFolderFilesID) {
        DownloadFolderFilesID = downloadFolderFilesID;
    }

    public int getDownloadFolderID() {
        return DownloadFolderID;
    }

    public void setDownloadFolderID(int downloadFolderID) {
        DownloadFolderID = downloadFolderID;
    }

    public String getDownloadFileDate() {
        return DownloadFileDate;
    }

    public void setDownloadFileDate(String downloadFileDate) {
        DownloadFileDate = downloadFileDate;
    }

    public String getDownloadFileTitle() {
        return DownloadFileTitle;
    }

    public void setDownloadFileTitle(String downloadFileTitle) {
        DownloadFileTitle = downloadFileTitle;
    }

    public String getDownloadFileURl() {
        return DownloadFileURl;
    }

    public void setDownloadFileURl(String downloadFileURl) {
        DownloadFileURl = downloadFileURl;
    }

    public DownloadModel() {

    }

    // Parcelling part
    public DownloadModel(Parcel in) {
        this.DownloadFolderFilesID = in.readInt();
        this.DownloadFolderID = in.readInt();
        this.DownloadFileDate = in.readString();
        this.DownloadFileTitle = in.readString();
        this.DownloadFileURl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.DownloadFolderFilesID);
        dest.writeInt(this.DownloadFolderID);
        dest.writeString(this.DownloadFileDate);
        dest.writeString(this.DownloadFileTitle);
        dest.writeString(this.DownloadFileURl);
    }
}
