package com.khateebclasses.Model;

import java.io.Serializable;

public class Engenaring implements Serializable {
    public String BatchCode = "";
    public String BatchType = "";
    public String VenueName="";
    public String StartDate="";
    public String EndDate="";
    public String BatchSchedule="";

    public String getDispImage() {
        return DispImage;
    }

    public void setDispImage(String dispImage) {
        DispImage = dispImage;
    }

    public String DispImage="";
    public double Amount;

    public String getBatchCode() {
        return BatchCode;
    }

    public void setBatchCode(String batchCode) {
        BatchCode = batchCode;
    }

    public String getBatchType() {
        return BatchType;
    }

    public void setBatchType(String batchType) {
        BatchType = batchType;
    }

    public String getVenueName() {
        return VenueName;
    }

    public void setVenueName(String venueName) {
        VenueName = venueName;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getBatchSchedule() {
        return BatchSchedule;
    }

    public void setBatchSchedule(String batchSchedule) {
        BatchSchedule = batchSchedule;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getSemesterName() {
        return SemesterName;
    }

    public void setSemesterName(String semesterName) {
        SemesterName = semesterName;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public String getFacultyName() {
        return FacultyName;
    }

    public void setFacultyName(String facultyName) {
        FacultyName = facultyName;
    }

    public String SubjectName="";

    public int getBatchId() {
        return BatchId;
    }

    public void setBatchId(int batchId) {
        BatchId = batchId;
    }

    public String SemesterName="";
    public String BranchName="";
    public String FacultyName="";
    public int BatchId;
    }