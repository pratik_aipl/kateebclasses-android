package com.khateebclasses.Model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 4/30/2018.
 */

public class Faculty implements Serializable {
    public int FacultyID ;
    public String FacultyName = "";

    public int getFacultyID() {
        return FacultyID;
    }

    public void setFacultyID(int facultyID) {
        FacultyID = facultyID;
    }

    public String getFacultyName() {
        return FacultyName;
    }

    public void setFacultyName(String facultyName) {
        FacultyName = facultyName;
    }
}
