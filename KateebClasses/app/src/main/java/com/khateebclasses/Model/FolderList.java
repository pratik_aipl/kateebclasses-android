package com.khateebclasses.Model;

import java.io.Serializable;

public class FolderList implements Serializable {

    int DownloadFolderID;
    String DownloadFolderDate;
    String DFolderTitle;
    Boolean IsPasswordProtected;

    public int getDownloadFolderID() {
        return DownloadFolderID;
    }

    public void setDownloadFolderID(int downloadFolderID) {
        DownloadFolderID = downloadFolderID;
    }

    public String getDownloadFolderDate() {
        return DownloadFolderDate;
    }

    public void setDownloadFolderDate(String downloadFolderDate) {
        DownloadFolderDate = downloadFolderDate;
    }

    public String getDFolderTitle() {
        return DFolderTitle;
    }

    public void setDFolderTitle(String DFolderTitle) {
        this.DFolderTitle = DFolderTitle;
    }

    public Boolean getPasswordProtected() {
        return IsPasswordProtected;
    }

    public void setPasswordProtected(Boolean passwordProtected) {
        IsPasswordProtected = passwordProtected;
    }
}
