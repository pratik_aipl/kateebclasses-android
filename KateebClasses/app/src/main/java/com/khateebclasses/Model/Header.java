package com.khateebclasses.Model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 4/13/2018.
 */

public class Header implements Serializable{
    public String StudentExamHdrID = "";

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String student_id = "";

    public String getStudentExamHdrID() {
        return StudentExamHdrID;
    }

    public void setStudentExamHdrID(String studentExamHdrID) {
        StudentExamHdrID = studentExamHdrID;
    }
}
