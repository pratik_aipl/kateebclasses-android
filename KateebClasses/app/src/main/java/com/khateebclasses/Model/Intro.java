package com.khateebclasses.Model;


import com.khateebclasses.R;

public enum Intro {

    MCQ(R.string.tour_one, R.layout.activity_tour_one),
    GENRATE(R.string.tour_two, R.layout.activity_tour_two),
    BOARD(R.string.tour_three, R.layout.activity_tour_three);

    private int mTitleResId;
    private int mLayoutResId;

    Intro(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}