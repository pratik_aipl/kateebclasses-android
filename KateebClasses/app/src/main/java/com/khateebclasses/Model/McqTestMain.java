package com.khateebclasses.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class McqTestMain implements Serializable, Cloneable {
    public Object clone() throws CloneNotSupportedException {

        return super.clone();
    }

    public boolean isRightt = false;
    public int selectedAns = -1;

    public String StudentExamDTLID = "", AnswerID = "", QuestionID = "", SrNo = "",
            QuestionDesc = "", QueType = "", QueImage = "", IsHintclicked = "";
    public int isAttemptedd = -1;

    public String getStudentExamDTLID() {
        return StudentExamDTLID;
    }

    public void setStudentExamDTLID(String studentExamDTLID) {
        StudentExamDTLID = studentExamDTLID;
    }

    public String getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(String questionID) {
        QuestionID = questionID;
    }

    public String getSrNo() {
        return SrNo;
    }

    public void setSrNo(String srNo) {
        SrNo = srNo;
    }

    public String getQuestionDesc() {
        return QuestionDesc;
    }

    public void setQuestionDesc(String questionDesc) {
        QuestionDesc = questionDesc;
    }

    public String getQueType() {
        return QueType;
    }

    public void setQueType(String queType) {
        QueType = queType;
    }

    public String getQueImage() {
        return QueImage;
    }

    public void setQueImage(String queImage) {
        QueImage = queImage;
    }

    public String getIsHintclicked() {
        return IsHintclicked;
    }

    public void setIsHintclicked(String isHintclicked) {
        IsHintclicked = isHintclicked;
    }

    public String getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(String answerID) {
        AnswerID = answerID;
    }

    public ArrayList<Options> optionArray = new ArrayList<>();

}