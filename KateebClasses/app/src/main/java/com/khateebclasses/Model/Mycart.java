package com.khateebclasses.Model;

import java.io.Serializable;

public class Mycart implements Serializable {
    public int CourseID;
    public String CourseTitle = "", Description = "";
    public double Amount;

    public int getIsType() {
        return isType;
    }

    public void setIsType(int isType) {
        this.isType = isType;
    }

    public int isType;
    public int getCourseID() {
        return CourseID;
    }

    public void setCourseID(int courseID) {
        CourseID = courseID;
    }

    public String getCourseTitle() {
        return CourseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        CourseTitle = courseTitle;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String shortDescription) {
        Description = shortDescription;
    }


    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }
}