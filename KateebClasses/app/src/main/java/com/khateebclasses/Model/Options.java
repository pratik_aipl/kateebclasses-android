package com.khateebclasses.Model;

import java.io.Serializable;

public class Options implements Serializable {

    public boolean isRight = false;

    public String AnswerID = "", AnswerDesc = "", AnswerImg = "", IsCorrect = "";

    public String getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(String answerID) {
        AnswerID = answerID;
    }

    public String getAnswerDesc() {
        return AnswerDesc;
    }

    public void setAnswerDesc(String answerDesc) {
        AnswerDesc = answerDesc;
    }

    public String getAnswerImg() {
        return AnswerImg;
    }

    public void setAnswerImg(String answerImg) {
        AnswerImg = answerImg;
    }

    public String getIsCorrect() {
        return IsCorrect;
    }

    public void setIsCorrect(String isCorrect) {
        IsCorrect = isCorrect;
    }
}