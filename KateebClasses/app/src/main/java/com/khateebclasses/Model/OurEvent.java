package com.khateebclasses.Model;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 4/7/2018.
 */

public class OurEvent implements Serializable {

    public int EventID ;
    public String EventName = "";
    public String EventDates = "";
    public String EventImageURL = "";
    public String EventAddress = "";
    public String EventDescription = "";
    public String EventURL = "";
    public String EventTime="";
    public String EDate="";
    public String DisplayImage="";

    public String getDisplayImage() {
        return DisplayImage;
    }

    public void setDisplayImage(String displayImage) {
        DisplayImage = displayImage;
    }

    public String getEDate() {
        return EDate;
    }

    public void setEDate(String EDate) {
        this.EDate = EDate;
    }

    public String getEventTime() {
        return EventTime;
    }

    public void setEventTime(String eventTime) {
        EventTime = eventTime;
    }

    public int getEventID() {
        return EventID;
    }

    public void setEventID(int eventID) {
        EventID = eventID;
    }

    public String getEventName() {
        return EventName;
    }

    public void setEventName(String eventName) {
        EventName = eventName;
    }

    public String getEventDates() {
        return EventDates;
    }

    public void setEventDates(String eventDates) {
        EventDates = eventDates;
    }

    public String getEventImageURL() {
        return EventImageURL;
    }

    public void setEventImageURL(String eventImageURL) {
        EventImageURL = eventImageURL;
    }

    public String getEventAddress() {
        return EventAddress;
    }

    public void setEventAddress(String eventAddress) {
        EventAddress = eventAddress;
    }

    public String getEventDescription() {
        return EventDescription;
    }

    public void setEventDescription(String eventDescription) {
        EventDescription = eventDescription;
    }

    public String getEventURL() {
        return EventURL;
    }

    public void setEventURL(String eventURL) {
        EventURL = eventURL;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String CreatedBy = "";
    public String CreatedOn = "";
    public String ModifiedBy = "";
    public String ModifiedOn = "";
}
