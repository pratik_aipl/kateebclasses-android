package com.khateebclasses.Model;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 4/14/2018.
 */

public class PaymentDetail implements Serializable{
    public String BatchOrCourse="";
    public String BatchCourseID="";

    public String getBatchOrCourse() {
        return BatchOrCourse;
    }

    public void setBatchOrCourse(String batchOrCourse) {
        BatchOrCourse = batchOrCourse;
    }

    public String getBatchCourseID() {
        return BatchCourseID;
    }

    public void setBatchCourseID(String batchCourseID) {
        BatchCourseID = batchCourseID;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String Amount="";
}
