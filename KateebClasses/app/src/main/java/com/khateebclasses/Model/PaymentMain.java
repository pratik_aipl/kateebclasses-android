package com.khateebclasses.Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 4/14/2018.
 */

public class PaymentMain implements Serializable {

    public String student_id = "";

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getTransactionID() {
        return TransactionID;
    }

    public void setTransactionID(String transactionID) {
        TransactionID = transactionID;
    }

    public String getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        TotalAmount = totalAmount;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String TransactionID = "";
    public String TotalAmount = "";
    public String PaymentStatus = "";
    public String OrderID="";
    public String emailID="";

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public ArrayList<PaymentDetail> PaymentDtl = new ArrayList<>();
}
