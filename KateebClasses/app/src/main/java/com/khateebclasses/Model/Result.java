package com.khateebclasses.Model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 4/13/2018.
 */

public class Result implements Serializable {
    public String Name = "", EmailID = "", MobileNo = "", CorrectAnswer = "", GradePoints = "", Date = "", ResultString = "";

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getCorrectAnswer() {
        return CorrectAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        CorrectAnswer = correctAnswer;
    }

    public String getGradePoints() {
        return GradePoints;
    }

    public void setGradePoints(String gradePoints) {
        GradePoints = gradePoints;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getResultString() {
        return ResultString;
    }

    public void setResultString(String resultString) {
        ResultString = resultString;
    }
}
