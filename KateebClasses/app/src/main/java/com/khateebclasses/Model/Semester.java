package com.khateebclasses.Model;

import java.io.Serializable;

public class Semester implements Serializable {
    public String SemesterName = "";
    public int SemesterID ;

    public String getSemesterName() {
        return SemesterName;
    }

    public void setSemesterName(String semesterName) {
        SemesterName = semesterName;
    }

    public int getSemesterID() {
        return SemesterID;
    }

    public void setSemesterID(int semesterID) {
        SemesterID = semesterID;
    }
}
