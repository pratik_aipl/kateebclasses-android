package com.khateebclasses.Model;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 4/7/2018.
 */

public class StudyAbroad implements Serializable {
    public int CourseID ;
    public String CourseType = "";
    public String CourseTitle = "";
    public String ShortDescription = "";

    public int getCourseID() {
        return CourseID;
    }

    public void setCourseID(int courseID) {
        CourseID = courseID;
    }

    public String getCourseType() {
        return CourseType;
    }

    public void setCourseType(String courseType) {
        CourseType = courseType;
    }

    public String getCourseTitle() {
        return CourseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        CourseTitle = courseTitle;
    }

    public String getShortDescription() {
        return ShortDescription;
    }

    public void setShortDescription(String shortDescription) {
        ShortDescription = shortDescription;
    }

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }

    public String getCourseImageURL() {
        return CourseImageURL;
    }

    public void setCourseImageURL(String courseImageURL) {
        CourseImageURL = courseImageURL;
    }

    public String getCourseURL() {
        return CourseURL;
    }

    public void setCourseURL(String courseURL) {
        CourseURL = courseURL;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String Detail = "";
    public String CourseImageURL = "";
    public String CourseURL = "";
    public String Amount = "";
    public String CreatedBy = "";
    public String CreatedOn = "";
    public String ModifiedBy = "";
    public String ModifiedOn = "";
}
