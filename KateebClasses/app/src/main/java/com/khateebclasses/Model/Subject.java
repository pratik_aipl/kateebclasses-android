package com.khateebclasses.Model;

import java.io.Serializable;

public class Subject implements Serializable {
    public String SubjectName = "";
    public int SubjectID;

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public int getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(int subjectID) {
        SubjectID = subjectID;
    }
}
