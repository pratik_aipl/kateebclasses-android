package com.khateebclasses.Model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 4/12/2018.
 */

public class SubmitAnswers implements Serializable {
    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getStudentExamHdrID() {
        return StudentExamHdrID;
    }

    public void setStudentExamHdrID(String studentExamHdrID) {
        StudentExamHdrID = studentExamHdrID;
    }

    public String student_id="",StudentExamHdrID="",StudentExamDTLID="",QuestionID="",AnswerID="",IsAttempt="";

    public String getStudentExamDTLID() {
        return StudentExamDTLID;
    }

    public void setStudentExamDTLID(String studentExamDTLID) {
        StudentExamDTLID = studentExamDTLID;
    }

    public String getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(String questionID) {
        QuestionID = questionID;
    }

    public String getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(String answerID) {
        AnswerID = answerID;
    }

    public String getIsAttempt() {
        return IsAttempt;
    }

    public void setIsAttempt(String isAttempt) {
        IsAttempt = isAttempt;
    }
}
