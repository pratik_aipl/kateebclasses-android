package com.khateebclasses.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class SubmitData implements Serializable {
    public ArrayList<SubmitAnswers> Answers = new ArrayList<>();
    public ArrayList<Header> header = new ArrayList<>();
}
