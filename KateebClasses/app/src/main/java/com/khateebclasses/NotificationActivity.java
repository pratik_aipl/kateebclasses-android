package com.khateebclasses;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.khateebclasses.Model.Notification;
import com.khateebclasses.Utils.CursorParserUniversal;
import com.khateebclasses.Utils.JsonParserUniversal;
import com.khateebclasses.Utils.MyDBManagerOneSignal;
import com.khateebclasses.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import io.fabric.sdk.android.Fabric;

import static com.khateebclasses.DashboardActivity.tv_title;

public class NotificationActivity extends AppCompatActivity {

    public NotificationActivity instance;
    public RecyclerView rcyclerView;
    public ArrayList<Notification> noticeArray = new ArrayList<>();

    public NotificatioinAdapter adapter;
    public JsonParserUniversal jParser;
    private LinearLayout emptyView;
    public TextView tv_empty;
    public static MyDBManagerOneSignal mDbOneSignal;
    public static Toolbar toolbar;
    public static ActionBar actionBar;
    public ArrayList<Notification> notiArray = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_notification);
        Utils.logUser();


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);

        actionBar = getSupportActionBar();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(instance, DashboardActivity.class);
                startActivity(i);
                finish();
            }
        });

        instance = this;
        tv_title.setText("NOTIFICATION");
        jParser = new JsonParserUniversal();
        mDbOneSignal = MyDBManagerOneSignal.getInstance(this);
        mDbOneSignal.open(this);
        rcyclerView = findViewById(R.id.rcyclerView);


        emptyView = findViewById(R.id.empty_view);
        tv_empty = findViewById(R.id.tv_empty);
        emptyView.setVisibility(View.GONE);

        mDbOneSignal.dbQuery("UPDATE `notification` SET `opened`= 1 WHERE json_extract(json_extract(json_extract(full_data, '$.custom'), '$.a') , '$.type') NOT IN ('Event','Announcement')");
        mDbOneSignal.dbQuery("UPDATE `notification` SET `collapse_id`= 1 WHERE json_extract(json_extract(json_extract(full_data, '$.custom'), '$.a') , '$.type') IN ('Event','Announcement')");

        try {

            //  System.out.println("Total Rows on try");

            noticeArray = (ArrayList) new CustomDatabaseQuery(instance, new Notification())
                    .execute(new String[]{"Select * from notification"}).get();
            Collections.reverse(noticeArray);
        } catch (InterruptedException e) {

            e.printStackTrace();
        } catch (ExecutionException e) {

            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mDbOneSignal.dbQuery("UPDATE notification SET opened = 1");
        if (noticeArray.size() > 0) {


            adapter = new NotificatioinAdapter(noticeArray);
            final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);

            rcyclerView.setLayoutAnimation(controller);

            rcyclerView.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
            rcyclerView.setItemAnimator(new DefaultItemAnimator());
            rcyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            Utils.hideProgressDialog();
            //  new CallRequests(AnnounceFragment.this).get_announcement();
        } else {
            rcyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }


    }

    public class NotificatioinAdapter extends RecyclerView.Adapter<NotificatioinAdapter.MyViewHolder> {

        public ArrayList<Notification> announcementArrayList;

        @Override
        public NotificatioinAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_announce_raw, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(NotificatioinAdapter.MyViewHolder holder, int position) {
            final Notification obj = announcementArrayList.get(position);
            holder.tv_title.setText(obj.getMessage().toString());


            try {
                JSONObject jObj = new JSONObject(obj.getFull_data());
                Log.i("TAG", "MILI SECOND :-> " + jObj.getString("google.sent_time"));

                String date = getDate(Long.parseLong(jObj.getString("google.sent_time")), "yyyy-MM-dd HH:mm:ss");
                Log.i("TAG", "date :-> " + date);


                holder.tv_date.setText(printDifference(date));

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        @Override
        public int getItemCount() {
            return announcementArrayList.size();
        }

        public NotificatioinAdapter(ArrayList<Notification> featuredJobList) {
            this.announcementArrayList = featuredJobList;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tv_title, tv_date;

            public MyViewHolder(View view) {
                super(view);

                tv_title = (TextView) view.findViewById(R.id.tv_title);
                tv_date = (TextView) view.findViewById(R.id.tv_date);


            }


        }

        public CharSequence printDifference(String start) {

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            long time = 0;

            try {
                time = sdf.parse(start).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long now = System.currentTimeMillis();

            CharSequence ago =
                    DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
            return ago;
        }

        public String convertDate(String dateInMilliseconds, String dateFormat) {
            return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
        }
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public class CustomDatabaseQuery extends AsyncTask<String, ArrayList<Object>, ArrayList<Object>> {

        public MyDBManagerOneSignal mDb;
        public CursorParserUniversal cParse;

        public Context ct;
        public Object obj;

        public CustomDatabaseQuery(Context ct, Object obj) {
            this.ct = ct;
            this.obj = obj;

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDb = MyDBManagerOneSignal.getInstance(ct);
            mDb.open(ct);
            this.cParse = new CursorParserUniversal();


        }

        @Override
        protected ArrayList<Object> doInBackground(String... id) {
            // Log.i("Total Rows", " : " + id[0]);
            Cursor c = mDb.getAllRows(id[0]);


            // System.out.println("Total Rows : " + id[0]);

            ArrayList<Object> objArray = new ArrayList<>();

            if (c != null && c.moveToFirst()) {
                do {

                    // System.out.println("Total Rows on onPreExecute class");

                    Class cl;
                    try {
                        cl = Class.forName(obj.getClass().getCanonicalName(), false, obj.getClass().getClassLoader());
                        obj = cl.newInstance();
                        obj = (Object) cParse.parseCursor(c, obj);

                        objArray.add(obj);

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                } while (c.moveToNext());

                // Log.v("Cursor Object", DatabaseUtils.dumpCursorToString(c));
                c.close();
            } else {

                //  Log.i("Total Rows", " : Not any SubjectAccuracy Found from");
                //  Utils.showToast("Not any SubjectAccuracy Found from : ", ct);
            }

            return objArray;

        }

        @Override
        protected void onPostExecute(ArrayList<Object> objects) {

            super.onPostExecute(objects);
        }


        public ArrayList<Object> executeOnExecutor(String[] strings) {
            ArrayList<Object> objArray = new ArrayList<>();

            return objArray;
        }
    }

    public class updateRecorrd extends AsyncTask<String, String, String> {


        Context context;
        public MyDBManagerOneSignal mDbOneSignal;
        public App app;
        public CursorParserUniversal cParse;
        public String SubjectName = "", paper_type = "", created_on = "", notification_type_id = "", type = "";


        private updateRecorrd(Context context) {

            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();

            mDbOneSignal = MyDBManagerOneSignal.getInstance(context);
            mDbOneSignal.open(context);

        }

        @Override
        protected void onPreExecute() {


            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... id) {

            //    int N_ID = mDbOneSignal.getLastRecordIDForRequest("notification", "OneSignalID");

            Cursor noti = mDbOneSignal.getAllRows("Select * from notification where opened=0" /*where _id > " + N_ID*/);
            Log.d("TAG", "Cursor :->" + DatabaseUtils.dumpCursorToString(noti));

            if (noti != null && noti.moveToFirst()) {
                do {
                    Notification notiObj = (Notification) cParse.parseCursor(noti, new Notification());
                    notiArray.add(notiObj);
                } while (noti.moveToNext());
                noti.close();
            }

            if (notiArray != null && notiArray.size() > 0) {
                for (int i = 0; i < notiArray.size(); i++) {
                    try {
                        JSONObject jObj = new JSONObject(notiArray.get(i).getFull_data());
                        String custom = jObj.getString("custom");
                        JSONObject jcustom = new JSONObject(custom);
                        String a = jcustom.getString("a");
                        JSONObject jtype = new JSONObject(a);
                        type = jtype.getString("type");
                        if (type.equalsIgnoreCase("Announcement")) {
                            mDbOneSignal.dbQuery("UPDATE notification SET opened = 1 where _id=" + notiArray.get(i).get_id());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


        }
    }

}
