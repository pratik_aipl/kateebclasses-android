package com.khateebclasses;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.crashlytics.android.Crashlytics;
import com.khateebclasses.Utils.Utils;

import io.fabric.sdk.android.Fabric;

public class PaymentStatusActivity extends AppCompatActivity {
public Button btn_ok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_payment_status);
        Utils.logUser();

        btn_ok = findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PaymentStatusActivity.this, DashboardActivity.class));
                finish();
            }
        });

    }
}
