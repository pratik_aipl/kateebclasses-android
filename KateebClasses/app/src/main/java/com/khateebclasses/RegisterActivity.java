package com.khateebclasses;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.khateebclasses.Interface.AsynchTaskListner;
import com.khateebclasses.Model.MainUser;
import com.khateebclasses.Utils.CallRequests;
import com.khateebclasses.Utils.Constant;
import com.khateebclasses.Utils.JsonParserUniversal;
import com.khateebclasses.Utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;


public class RegisterActivity extends AppCompatActivity implements AsynchTaskListner {
    public ImageView img_back;
    public TextView tv_register;
    public EditText et_firstname, et_lastname, et_email, et_mobile, et_password, et_confirm_pass;
    public RegisterActivity instance;
    public SharedPreferences shared;
    public JsonParserUniversal jParser;
    public App app;
    public MainUser user;
    public String PleyaerID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_register);
        Utils.logUser();

        instance = this;
        app = App.getInstance();
        jParser = new JsonParserUniversal();
        shared = getSharedPreferences(getPackageName(), 0);
        PleyaerID = Utils.OneSignalPlearID();
        FindElimants();

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Validation();
            }
        });
    }

    public void FindElimants() {
        img_back = findViewById(R.id.img_back);
        tv_register = findViewById(R.id.tv_register);
        et_firstname = findViewById(R.id.et_firstName);
        et_lastname = findViewById(R.id.et_LastName);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_pass);
        et_mobile = findViewById(R.id.et_mobile);
        et_confirm_pass = findViewById(R.id.et_confirm_pass);


    }

    public void Validation() {
        if (et_firstname.getText().toString().equals("")) {
            et_firstname.setError("Please Enter First Name");
            et_firstname.setFocusable(true);
        } else if (et_lastname.getText().toString().equals("")) {
            et_lastname.setError("Please Enter Last Name");
            et_lastname.setFocusable(true);
        } else if (et_mobile.getText().toString().length() != 10) {
            et_mobile.setError("Please Enter Valid Mobile No.");
            et_mobile.setFocusable(true);
        } else if (et_email.getText().toString().equals("") || !Utils.isValidEmail(et_email.getText().toString())) {
            et_email.setError(" Please Enter Valid Email ID");
            et_email.setFocusable(true);
        } else if (et_password.getText().toString().equals("")) {
            et_password.setError("Please Enter Valid Password");
            et_password.setFocusable(true);
        } else if (et_confirm_pass.getText().toString().equals("")) {
            et_confirm_pass.setError("Please Enter Confirm Password");
            et_confirm_pass.setFocusable(true);
        } else if (!et_confirm_pass.getText().toString().equals(et_password.getText().toString())) {
            et_confirm_pass.setError("Password And Confirm Password Not Match");
            et_confirm_pass.setFocusable(true);
        } else {
            new CallRequests(instance).StudentRegister(et_firstname.getText().toString(),
                    et_lastname.getText().toString(), et_email.getText().toString(),
                    et_mobile.getText().toString(), et_password.getText().toString(), PleyaerID);
        }
    }

    public void alert(String message) {
        new AlertDialog.Builder(this)
                .setMessage((message))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        RegisterActivity.this.startActivity(new Intent(instance, LoginActivity.class));
                        RegisterActivity.this.finish();
                    }
                })
                .show();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case StudentRegister:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getString("Status").equals("Success")) {
                           // Utils.showToast("Welcome to the Khateeb Classes", instance);

                           /* user = new MainUser();
                            user.setStudentID(jObj.getInt("StudentID"));
                            user.setFirstName(jObj.getString("FirstName"));
                            user.setLastName(jObj.getString("LastName"));
                            user.setEmailID(jObj.getString("EmailID"));
                            user.setMobileNo(jObj.getString("MobileNo"));
                            user.setPassword(jObj.getString("Password"));
                            app.user = user;
                            app.IS_LOGGED = true;

                            SharedPreferences.Editor et = shared.edit();
                            Gson gson = new Gson();
                            String json = gson.toJson(app.user);
                            et.putString("user", json);
                            et.putBoolean(app.IS_LOGGED_IN, true);
                            et.commit();*/
                            alert(jObj.getString("message"));

                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
