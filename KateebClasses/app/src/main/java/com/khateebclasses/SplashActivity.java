package com.khateebclasses;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.khateebclasses.Model.MainUser;
import com.khateebclasses.Model.McqTestMain;
import com.khateebclasses.Model.Mycart;
import com.khateebclasses.Utils.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;


public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    public SharedPreferences pref;
    public SharedPreferences shared;
    public App app;
    public static ArrayList<McqTestMain> testMainArray = new ArrayList<>();
    public String inNoti = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        Utils.logUser();

        app = App.getInstance();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                shared = getSharedPreferences(getPackageName(), 0);
                pref = getSharedPreferences(getApplication().getPackageName(), 0);
                if (pref.getBoolean(App.IS_LOGGED_IN, false)) {
                    Gson gson = new Gson();
                    String json = shared.getString("user", "");
                    app.StudentExamHdrID = shared.getString("StudentExamHdrID", "");
                    MainUser user = gson.fromJson(json, MainUser.class);
                    app.user = user;
                    app.IS_LOGGED = true;
                    String MCQ = shared.getString("MCQ", "");
                    Log.i("TAg", "JSon::-->" + MCQ);
                    try {
                        Type listType = new TypeToken<ArrayList<McqTestMain>>() {
                        }.getType();
                        app.TestMainArray = (ArrayList<McqTestMain>) gson.fromJson(shared.getString("MCQ", ""), listType);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        Type listType = new TypeToken<ArrayList<Mycart>>() {
                        }.getType();
                        app.MycartArray = (ArrayList<Mycart>) gson.fromJson(shared.getString("CART", ""), listType);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        if (getIntent().getExtras().getString("inNoti").equals("1")) {
                            Intent i = new Intent(SplashActivity.this, NotificationActivity.class);
                            startActivity(i);
                            finish();
                        } else {
                            Intent i = new Intent(SplashActivity.this, DashboardActivity.class);
                            startActivity(i);
                            finish();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Intent i = new Intent(SplashActivity.this, DashboardActivity.class);
                        startActivity(i);
                        finish();
                    }

                } else {
                    App.IS_LOGGED = false;
                    Intent i = new Intent(SplashActivity.this, IntroActivity.class);
                    startActivity(i);
                    finish();

                }
            }


        }, SPLASH_TIME_OUT);
    }
}
