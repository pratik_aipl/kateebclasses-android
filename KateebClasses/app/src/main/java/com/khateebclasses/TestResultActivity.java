package com.khateebclasses;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.khateebclasses.Model.Result;
import com.khateebclasses.Utils.MyDBManagerOneSignal;
import com.khateebclasses.Utils.Utils;

import io.fabric.sdk.android.Fabric;

public class TestResultActivity extends AppCompatActivity {
    public View view;
    public TestResultActivity instance;
    public SharedPreferences shared;
    public static MyDBManagerOneSignal mDbOneSignal;
    public TextView tv_gread, tv_correct, tv_date;
    public WebView web_rsult;
    public Button btn_ok;
    public Result obj;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_test_result);
        Utils.logUser();


        shared = getSharedPreferences(getPackageName(), 0);

        SharedPreferences.Editor et = shared.edit();
        et.putString("MCQ", "");
        et.apply();
        et.commit();

        obj = (Result) getIntent().getExtras().getSerializable("obj");
        tv_date = findViewById(R.id.tv_date);
        tv_gread = findViewById(R.id.tv_gread);
        tv_correct = findViewById(R.id.tv_correct);
        web_rsult = findViewById(R.id.web_rsult);
        btn_ok = findViewById(R.id.btn_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tv_date.setText(obj.getDate());
        tv_gread.setText(obj.getGradePoints());
        tv_correct.setText(Html.fromHtml("<font color=\"#e79902\">" + obj.getCorrectAnswer() + "</font> / 60"));
        web_rsult.loadDataWithBaseURL("", obj.getResultString(), "text/html", "UTF-8", "");

    }

}
