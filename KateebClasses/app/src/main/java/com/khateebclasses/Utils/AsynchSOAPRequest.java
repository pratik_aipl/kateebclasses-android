package com.khateebclasses.Utils;

import android.content.Context;
import android.os.AsyncTask;
import androidx.fragment.app.Fragment;

import android.util.Log;

import com.khateebclasses.Interface.AsynchTaskListner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Karan - Empiere on 7/7/2017.
 */

public class AsynchSOAPRequest {
    public Fragment ft;
    public Constant.REQUESTS request;
    public Map<String, String> map;
    public AsynchTaskListner aListner;
    public Constant.POST_TYPE post_type;
    public StringBuilder sb = new StringBuilder();


    public AsynchSOAPRequest(Fragment ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ft = ft;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ft;
        this.post_type = post_type;

        if (Utils.isNetworkAvailable(ft.getActivity())) {
            if (!map.containsKey("show")) {
                Utils.showProgressDialog(ft.getActivity());
            } else {
                this.map.remove("show");
            }

            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast("No Internet, Please try again later", ft.getActivity());
        }
    }

    public Context ct;

    public AsynchSOAPRequest(Context ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ct = ft;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ft;
        this.post_type = post_type;
        if (Utils.isNetworkAvailable(ct)) {
            if (!map.containsKey("show")) {
                Utils.showProgressDialog(ct);
            } else {
                this.map.remove("show");
            }
            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast("No Internet, Please try again later", ct);
        }
    }

    class MyRequest extends AsyncTask<Map<String, String>, Void, String> {
        MyCustomMultiPartEntity reqEntity;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Map<String, String>... map) {
            System.out.println("::urls ::" + map[0].get("url"));
            String responseBody = "";
            try {
                switch (post_type) {
                    case GET:
                        DefaultHttpClient httpClient = new DefaultHttpClient();
                        String query = map[0].get("url");
                        System.out.println();
                        map[0].remove("url");
                        List<String> values = new ArrayList<String>(map[0].values());
                        List<String> keys = new ArrayList<String>(map[0].keySet());
                        for (int i = 0; i < values.size(); i++) {
                            System.out.println();
                            System.out.println(keys.get(i) + "====>" + values.get(i));
                            query = query + keys.get(i) + "=" + values.get(i).replace(" ", "%20");

                            if (i < values.size() - 1) {
                                query += "&";
                            }
                        }
                        System.out.println("URL" + "====>" + query);
                        HttpGet httpGet = new HttpGet(query);
                        HttpResponse httpResponse = httpClient.execute(httpGet);
                        HttpEntity httpEntity = httpResponse.getEntity();
                        responseBody = EntityUtils.toString(httpEntity);
                        System.out.println("Responce" + "====>" + responseBody);
                        return responseBody;

                    case POST:

                        String URL = map[0].get("url");
                        String METHOD_NAME = map[0].get(Constant.METHOD_NAME);
                        map[0].remove(Constant.SOAP_ACTION);
                        map[0].remove(Constant.METHOD_NAME);
                        String result = "";
                        try {
                            String strURL = URL + "/" + METHOD_NAME;
                            Log.i("REQUEST URL :::", strURL);
                            URL url = new URL(strURL);
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setRequestMethod("POST");
                            conn.setRequestProperty("Content-Type", "application/json");
                            conn.setRequestProperty("Content-Encoding", "gzip");
                            conn.setRequestProperty("Accept-Encoding", "identity");
                            //conn.setRequestProperty("ACCESS-KEY","admin@123");
                            conn.setDoInput(true);
                            conn.setDoOutput(true);
                            conn.setChunkedStreamingMode(0);

                            JSONObject json = generateSOAPRequest(map[0]);
                            Log.i("REQUEST URL JSON :::", json.toString());
//                            longInfo(json.toString(), "REQUEST URL JSON :::");
                            conn.getOutputStream().write(json.toString().getBytes("UTF-8"));
                            conn.getOutputStream().close();

                            int status = conn.getResponseCode();
                            InputStream inStream = null;
                            inStream = conn.getInputStream();
                            BufferedInputStream in = new BufferedInputStream(inStream);

                            int b;
                            while ((b = in.read()) != -1) {
                                result += (char) b;
                            }
                            conn.disconnect();
                            Log.i("RESULT :::", result);
                            return result;
                        } catch (Exception e) {
                            Utils.hideProgressDialog();
                            e.printStackTrace();
                            return "";
                        }

                    case POST_NEW:
                        System.out.println("new Requested URL >> " + map[0].get("url"));
                        HttpPost postRequest = new HttpPost(map[0].get("url"));

                        java.net.URL url = new URL(map[0].get("url"));
                        map[0].remove("url");

                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("POST");
                            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                        conn.setRequestProperty("Accept", "application/json");
                        conn.setDoOutput(true);
                        conn.setDoInput(true);
                        String jsonValue = new JSONObject(map[0]).toString();//String.valueOf(new JSONObject(map[0]));
                        Log.i("Json Value : ", jsonValue);
//                        if(jsonValue.equalsIgnoreCase("{}")){
//                            Utils.hideProgressDialog();
//                        }
                        jsonValue = jsonValue.replaceAll("\\\\", "");
                        longInfo(jsonValue, "MCQ ::-->");
                        DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                        //os.writeBytes(URLEncoder.encode(jsonValue.toString(), "UTF-8"));
                        os.writeBytes(String.valueOf(jsonValue));

                        os.flush();
                        os.close();

                        Log.i("STATUS", String.valueOf(conn.getResponseCode()));


                        int HttpResult = conn.getResponseCode();

                        if (HttpResult == HttpURLConnection.HTTP_OK) {
                            BufferedReader br = new BufferedReader(new InputStreamReader(
                                    conn.getInputStream(), "utf-8"));
                            String line = null;
                            while ((line = br.readLine()) != null) {
                                sb.append(line + "\n");
                            }
                            br.close();


                            Log.i("MSG", sb.toString());
                            return sb.toString();

                        } else {
                            System.out.println(conn.getResponseMessage());
                            return "";
                        }


                    case POST_WITH_JSON:

                        System.out.println("new Requested URL >> " + map[0].get("url"));
                         postRequest = new HttpPost(map[0].get("url"));

                         url = new URL(map[0].get("url"));
                        map[0].remove("url");

                         conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                        conn.setRequestProperty("Accept", "application/json");
                        conn.setDoOutput(true);
                        conn.setDoInput(true);
                         jsonValue = new JSONObject(map[0].get("json")).toString();//String.valueOf(new JSONObject(map[0]));
                        Log.i("Json Value : ", jsonValue);
                        jsonValue = jsonValue.replaceAll("\\\\", "");
                        longInfo(jsonValue, "MCQ ::-->");
                         os = new DataOutputStream(conn.getOutputStream());
                        //os.writeBytes(URLEncoder.encode(jsonValue.toString(), "UTF-8"));
                        os.writeBytes(String.valueOf(jsonValue));

                        os.flush();
                        os.close();

                        Log.i("STATUS", String.valueOf(conn.getResponseCode()));


                         HttpResult = conn.getResponseCode();

                        if (HttpResult == HttpURLConnection.HTTP_OK) {
                            BufferedReader br = new BufferedReader(new InputStreamReader(
                                    conn.getInputStream(), "utf-8"));
                            String line = null;
                            while ((line = br.readLine()) != null) {
                                sb.append(line + "\n");
                            }
                            br.close();


                            Log.i("MSG", sb.toString());
                            return sb.toString();

                        } else {
                            System.out.println(conn.getResponseMessage());
                            return "";
                        }

                    case POST_WITH_IMAGE:
                        httpClient = new DefaultHttpClient();

                        postRequest = new HttpPost(map[0].get("url"));

                        map[0].remove("url");
                        reqEntity = new MyCustomMultiPartEntity(new MyCustomMultiPartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                            }
                        });
                        BasicHttpContext localContext = new BasicHttpContext();
                        for (String key : map[0].keySet()) {
                            System.out.println();
                            System.out.println(key + "====>" + map[0].get(key));
                            if (key.equals("image")) {
                                if (map[0].get(key) != null) {
                                    if (map[0].get(key).length() > 1) {
                                        String type = "imageSelected/png";
                                        File f = new File(map[0].get(key));
                                        FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                        reqEntity.addPart("image", fbody);
                                    }
                                }
                            } else if (key.equals("cover")) {
                                if (map[0].get(key) != null) {
                                    if (map[0].get(key).length() > 1) {
                                        String type = "imageSelected/png";
                                        File f = new File(map[0].get(key));
                                        FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                        reqEntity.addPart("cover", fbody);
                                    }
                                }
                            } else if (key.equals("file")) {
                                if (map[0].get(key) != null) {
                                    if (map[0].get(key).length() > 1) {
                                        String type = "imageSelected/png";
                                        File f = new File(map[0].get(key));

                                        String filetype = f.getAbsolutePath().substring(f.getAbsolutePath().lastIndexOf(".") + 1, f.getAbsolutePath().length());
                                        Log.i("TAG ", "File Type :: " + filetype);
                                        if (filetype.equals("png") || filetype.equals("jpg") || filetype.equals("jpeg")) {
                                            type = "imageSelected/" + filetype;
                                        } else {
                                            type = "video/" + filetype;
                                        }

                                        FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                        reqEntity.addPart("file", fbody);
                                    }
                                }
                            } else {
                                try {
                                    Log.i("view N review", " :  Key ::" + key);
                                    Log.i("view N review", " :  value ::" + map[0].get(key));
                                    reqEntity.addPart(key, new StringBody(map[0].get(key).replace(" ", "%20")));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    reqEntity.addPart(key, new StringBody(map[0].get(key)));
                                }
                            }
                        }
                        postRequest.setEntity(reqEntity);
                        HttpResponse responses = httpClient.execute(postRequest, localContext);
                        BufferedReader reader = new BufferedReader(new InputStreamReader(responses.getEntity().getContent(),
                                "UTF-8"));
                        String sResponse;
                        while ((sResponse = reader.readLine()) != null) {
                            responseBody = responseBody + sResponse;
                        }
                        System.out.println("Responce ::" + responseBody);
                        return responseBody;
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                Utils.hideProgressDialog();
            } catch (Exception e) {
                e.printStackTrace();
                Utils.hideProgressDialog();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            aListner.onTaskCompleted(result, request);
            super.onPostExecute(result);
        }
    }

    public static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Log.i("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else {
            Log.i("TAG " + tag + " -->", str);
        }
    }

    public JSONObject generateSOAPRequest(Map<String, String> map) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        for (String key : map.keySet()) {
            if (map.get(key) != null) {
                jsonObject.put(key, map.get(key));
            }

        }
        return jsonObject;
    }


}

