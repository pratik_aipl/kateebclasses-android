package com.khateebclasses.Utils;

import android.content.Context;
import android.net.Uri;
import androidx.fragment.app.Fragment;
import android.util.Base64;

import com.khateebclasses.App;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

import static com.khateebclasses.BuildConfig.URL;
import static com.khateebclasses.BuildConfig.URLN;

public class CallRequests {

    private static final String TAG = "CallRequests";
    App app;
    Context ct;
    Fragment ft;
    // public String URL = "http://clientworksarea.com/Khateebclassess/WebService1.asmx/";
    // public String URLN = "http://clientworksarea.com/Khateebclassess/WebService2.asmx/";
    // public String URL = "https://khateebclasses.com/WebService1.asmx/";
    // public String URLN = "https://khateebclasses.com/WebService2.asmx/";
    public String METHOD_NAME;
    public String Student_id = "", AppUserID = "";
    public String BranchID = "";

    public CallRequests(Context ct) {
        this.ct = ct;
        app = App.getInstance();
        try {
            Student_id = String.valueOf(App.user.getStudentID());

        } catch (Exception e) {
            Student_id = "";
            e.printStackTrace();
        }


    }

    public CallRequests(Fragment ft) {
        this.ft = ft;
        app = App.getInstance();

        if (app != null && app.user != null && app.user.getStudentID() != 0)
            Student_id = String.valueOf(app.user.getStudentID());
        else
            Student_id = "";

    }

    public void getLogin(String EmailID, String Password, String playerID) {
        METHOD_NAME = "get_login";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);

        map.put("user_name", EmailID);
        map.put("password", Password);
        map.put("playerID", playerID);

        new AsynchSOAPRequest(ct, Constant.REQUESTS.getLogin, Constant.POST_TYPE.POST_NEW, map);
    }


    public void StudentRegister(String FirstName, String LastName, String EmailID, String Mobileno, String Password, String playerID) {
        METHOD_NAME = "student_register";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);

        map.put("first_name", FirstName);
        map.put("last_name", LastName);
        map.put("email_id", EmailID);
        map.put("mobile_no", Mobileno);
        map.put("password", Password);
        map.put("playerID", playerID);
        new AsynchSOAPRequest(ct, Constant.REQUESTS.StudentRegister, Constant.POST_TYPE.POST_NEW, map);
    }

    public void change_password(String OldPassword, String NewPassword) {
        METHOD_NAME = "change_password";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);

        map.put("student_id", Student_id);
        map.put("old_password", OldPassword);
        map.put("new_password", NewPassword);
        new AsynchSOAPRequest(ct, Constant.REQUESTS.change_password, Constant.POST_TYPE.POST_NEW, map);


    }

    public void forgot_password(String email_id) {
        METHOD_NAME = "forgot_password";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);

        map.put("email_id", email_id);
        new AsynchSOAPRequest(ct, Constant.REQUESTS.forgot_password, Constant.POST_TYPE.POST_NEW, map);


    }

    public void update_profile(String first_name, String last_name, String email_id, String mobile_no) {

        METHOD_NAME = "update_profile";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);

        map.put("student_id", Student_id);
        map.put("first_name", first_name);
        map.put("last_name", last_name);
        map.put("email_id", email_id);
        map.put("mobile_no", mobile_no);

        new AsynchSOAPRequest(ct, Constant.REQUESTS.update_profile, Constant.POST_TYPE.POST_NEW, map);
    }

    public void get_study_abroad() {
        METHOD_NAME = "get_study_abroad";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);

        map.put("student_id", "");
        new AsynchSOAPRequest(ft, Constant.REQUESTS.get_study_abroad, Constant.POST_TYPE.POST_NEW, map);
    }

    public void get_certification() {
        METHOD_NAME = "get_certification";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);

        map.put("student_id", "");
        new AsynchSOAPRequest(ft, Constant.REQUESTS.get_certification, Constant.POST_TYPE.POST_NEW, map);
    }

    public void get_our_events() {
        METHOD_NAME = "get_our_events";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);

        map.put("student_id", Student_id);
        new AsynchSOAPRequest(ft, Constant.REQUESTS.get_our_events, Constant.POST_TYPE.POST_NEW, map);
    }

    public void get_announcement() {
        METHOD_NAME = "get_announcement";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);

        map.put("student_id", Student_id);
        new AsynchSOAPRequest(ft, Constant.REQUESTS.get_announcement, Constant.POST_TYPE.POST_NEW, map);
    }

    public void get_download() {
        METHOD_NAME = "get_download";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);

        map.put("student_id", Student_id);
        new AsynchSOAPRequest(ft, Constant.REQUESTS.get_download, Constant.POST_TYPE.POST_NEW, map);
    }

    //New Implement API

    public void get_Folderlist() {
        METHOD_NAME = "get_download_FolderList";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URLN + METHOD_NAME);
        new AsynchSOAPRequest(ft, Constant.REQUESTS.get_folderlist, Constant.POST_TYPE.POST_NEW, map);
    }
    //New Implement API

    public void getPassword_Protected(int FolderID, String Password) {
        METHOD_NAME = "get_download_PasswordProtected_FileList";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URLN + METHOD_NAME);

        map.put("DownloadFolderID", String.valueOf(FolderID));
        map.put("DFolderPassword", Password);

        new AsynchSOAPRequest(ft, Constant.REQUESTS.get_passwordProt, Constant.POST_TYPE.POST_NEW, map);
    }
    //New Implement API

    public void getPassword_NoProtected(int FolderID) {
        METHOD_NAME = "get_download_FileList";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URLN + METHOD_NAME);

        map.put("DownloadFolderID", String.valueOf(FolderID));

        new AsynchSOAPRequest(ft, Constant.REQUESTS.get_passwordNoProt, Constant.POST_TYPE.POST_NEW, map);
    }

    public void get_EnrolledSubCourse() {
        METHOD_NAME = "get_EnrolledSubCourse";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);

        map.put("student_id", Student_id);
        new AsynchSOAPRequest(ft, Constant.REQUESTS.get_EnrolledSubCourse, Constant.POST_TYPE.POST_NEW, map);
    }

    public String ImageConvert(String ImageUrl) {
        String imageAsString = "";
        try {
            if (ImageUrl != null && !ImageUrl.equalsIgnoreCase("")) {
                Uri uriString = Uri.parse(ImageUrl);
                File file = new File(uriString.getPath());
                FileInputStream objFileIS;
                objFileIS = new FileInputStream(file);
                ByteArrayOutputStream objByteArrayOS = new ByteArrayOutputStream();
                byte[] byteBufferString = new byte[1024];
                for (int readNum; (readNum = objFileIS.read(byteBufferString)) != -1; ) {
                    objByteArrayOS.write(byteBufferString, 0, readNum);
                }

                imageAsString = Base64.encodeToString(objByteArrayOS.toByteArray(), Base64.DEFAULT);
            } else {
                imageAsString = "";
            }
            return imageAsString;

        } catch (Exception e) {
            e.printStackTrace();
            return imageAsString;
        }
    }

    public void StartExam() {
        METHOD_NAME = "StartExam";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);

        map.put("student_id", Student_id);
        new AsynchSOAPRequest(ct, Constant.REQUESTS.StartExam, Constant.POST_TYPE.POST_NEW, map);


    }

    public void get_engineering_Subjects(String BranchID, String SemesterID, String SubjectID, String FacultyID, String Batchtype) {
        METHOD_NAME = "get_engineering_SubjectsFilter";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);

        map.put("BranchID", BranchID);
        map.put("SemesterID", SemesterID);
        map.put("SubjectID", SubjectID);
        map.put("FacultyID", FacultyID);
        map.put("Batchtype", Batchtype);

        new AsynchSOAPRequest(ft, Constant.REQUESTS.get_engineering_Subjects, Constant.POST_TYPE.POST_NEW, map);
    }

    public void make_payment(String json) {
        METHOD_NAME = "TransactionDetails";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);

        map.put("json", json);

        new AsynchSOAPRequest(ct, Constant.REQUESTS.make_payment, Constant.POST_TYPE.POST_WITH_JSON, map);
    }

    public void SubmitTest(String json) {
        METHOD_NAME = "SubmitTest";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);
        //map.put(Constant.METHOD_NAME,METHOD_NAME);
        map.put("json", json);

        new AsynchSOAPRequest(ct, Constant.REQUESTS.SubmitTest, Constant.POST_TYPE.POST_WITH_JSON, map);
    }

    public void GetTestResult() {
        METHOD_NAME = "GetTestResult";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);

        map.put("student_id", Student_id);

        new AsynchSOAPRequest(ct, Constant.REQUESTS.GetTestResult, Constant.POST_TYPE.POST_NEW, map);
    }

    public void get_Semesters() {
        METHOD_NAME = "get_Semesters";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);


        new AsynchSOAPRequest(ft, Constant.REQUESTS.get_Semesters, Constant.POST_TYPE.POST_NEW, map);
    }

    public void get_Branches() {
        METHOD_NAME = "get_Branches";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);


        new AsynchSOAPRequest(ft, Constant.REQUESTS.get_Branches, Constant.POST_TYPE.POST_NEW, map);
    }

    public void get_Subjects() {
        METHOD_NAME = "get_Subjects";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);


        new AsynchSOAPRequest(ft, Constant.REQUESTS.get_Subjects, Constant.POST_TYPE.POST_NEW, map);
    }

    public void get_Faculty() {
        METHOD_NAME = "get_Faculty";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + METHOD_NAME);


        new AsynchSOAPRequest(ft, Constant.REQUESTS.get_Faculty, Constant.POST_TYPE.POST_NEW, map);
    }

    public void getRSA() {
        METHOD_NAME = "getRSA";
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", "http://junkminds.com/WebService2.asmx");

        map.put("access_code", "AVJD66DI78AB96DJBA");
        map.put("order_id", "445566");
        new AsynchSOAPRequest(ct, Constant.REQUESTS.getRSA, Constant.POST_TYPE.POST_NEW, map);
    }
}

