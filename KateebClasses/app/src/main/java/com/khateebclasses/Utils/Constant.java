package com.khateebclasses.Utils;


import android.os.Environment;

public class Constant {

    public static int TIME_OUT = 60000;
    public static final int READ_WRITE_EXTERNAL_STORAGE_CAMERA = 2355;
    public static final String IMAGE_PREFIX = "http://junkminds.com/";
    public static final String DISPLAY_IMAGE_PREFIX = "http://www.khateebclasses.com/";
    public static final String DOWNLOAD_PDF_PREFIX = "http://www.khateebclasses.com/admin/";

    public static final String SOAP_ACTION = "SOAP_ACTION", METHOD_NAME = "METHOD_NAME";
//	public static final int pink = Color.rgb(179, 62, 93);

    public static final String PLAY_STORE_LINK = "Click Here to download and install " + /*add the application name here*/ " android Application "
            + "https://play.google.com/store/apps/details?id="; //// add the package name at the end
    public static final String[] NO_RECORDS = new String[]{"No Recent Chat Found"};
    public static final String EXAM_DATA_STORAGE = "cache_%1s.txt";


    public enum POST_TYPE {
        GET, POST, POST_WITH_IMAGE, Soap, POST_WITH_JSON, POST_NEW;
    }


    public enum REQUESTS {
        StudentRegister, change_password, forgot_password, update_profile, get_study_abroad, get_certification, get_our_events, get_announcement, get_download, mcq_view_paper, StartExam, get_engineering_Subjects, make_payment, SubmitTest, get_Semesters, get_Branches, get_Subjects, get_EnrolledSubCourse, getRSA, GetTestResult, get_Faculty, getLogin, get_folderlist, get_passwordProt, get_passwordNoProt
    }

    /**
     * This is for PUSH Notification
     **/

    public static final String SENDER_ID = "";
    public static final String DISPLAY_ACTION = "com.markteq.wms";
    public static final String LOCAL_IMAGE_PATH = Environment.DIRECTORY_DOWNLOADS;

    public static final int TWITTER_LOGIN_REQUEST_CODE = 1;


}
