package com.khateebclasses.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.StateListDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings.Secure;
import androidx.core.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.format.Formatter;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.khateebclasses.R;
import com.onesignal.OneSignal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Utils {

    /* for GCM */
    static final String DISPLAY_MESSAGE_ACTION = "com.maxinternational.max.DISPLAY_MESSAGE";
    static final String EXTRA_MESSAGE = "message";
    private static final String TAG = "Typefaces";
    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();
    public static SimpleDateFormat Input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat InputDate = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat output = new SimpleDateFormat("MMMM dd, yyyy");
    public static SimpleDateFormat outputDateFormate = new SimpleDateFormat("MMM dd, yyyy");
    public static SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyyMMdd");
    public static Dialog dialog;
    static float density = 1;
    static String Pleyaer = "";
    private static ProgressDialog mProgressDialog;
    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";

    public static int getScreenWidth(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        return display.getWidth();
    }

    public static boolean checkValidation(EditText et) {
        if (et.getText().toString().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    public static String OneSignalPlearID() {

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null)
                    Pleyaer = userId;
                Log.d("debug", "User:" + userId);

                Log.d("debug", "registrationId:" + registrationId);

            }
        });
        return Pleyaer;
    }
    public static String changeDateToMMDDYYYY(String input) {
        String outputDateStr ="";
        try {
            Date date = InputDate.parse(input);
            outputDateStr = output.format(date);
            //Log.i("output", outputDateStr);
        } catch (ParseException p) {
            p.printStackTrace();
        }
        return outputDateStr;
    }
    public static Bitmap takeScreenShot(Activity activity) {
        View view = activity.getWindow().getDecorView();
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        View MainView = activity.getWindow().getDecorView();
        MainView.setDrawingCacheEnabled(true);
        MainView.buildDrawingCache();
        Bitmap MainBitmap = MainView.getDrawingCache();
        Rect frame = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        //to remove statusBar from the taken sc
        int statusBarHeight = frame.top;
        //using screen size to create bitmap
        int width = activity.getWindowManager().getDefaultDisplay().getWidth();
        int height = activity.getWindowManager().getDefaultDisplay().getHeight();
        Bitmap OutBitmap = Bitmap.createBitmap(MainBitmap, 0, statusBarHeight, width, height - statusBarHeight);
        MainView.destroyDrawingCache();

        return OutBitmap;


       /* Bitmap b1 = view.getDrawingCache();
        Rect frame = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        int statusBarHeight = frame.top;

        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;


        Bitmap b = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height - statusBarHeight);
        view.destroyDrawingCache();
        return b;*/
    }
    public static String changeDateToMDY(String input) {

        String outputDateStr ="";
        try {
            DateFormat outputFormat = new SimpleDateFormat("MM/dd/yyyy");
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            // String input="2013-06-24";
            Date date = inputFormat.parse(input);
            outputDateStr = outputFormat.format(date);
            Log.i("output", outputDateStr);

        } catch (ParseException p) {
            p.printStackTrace();

        }
        return outputDateStr;
    }
    public static String getDataDir(final Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).applicationInfo.dataDir;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String Millisecond(String input){
        String dtStart = input+"+05:30";
        Date date1 = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            //date1 = format.parse(dtStart);
            format.setTimeZone(TimeZone.getTimeZone("IST")); // missing line
             date1 = format.parse(dtStart);
            SimpleDateFormat writeDate = new SimpleDateFormat("dd.MM.yyyy,");
            writeDate.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
            String s = writeDate.format(date1);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        input  = String.valueOf(date1.getTime());
        return input;

    }
    public static int getOSVersion() {
        return Build.VERSION.SDK_INT;
    }

    public static String getNetworkOperator(Context ct) {
        TelephonyManager manager = (TelephonyManager) ct.getSystemService(Context.TELEPHONY_SERVICE);
        return manager.getNetworkOperatorName();
    }

    public static String getMobileNumber(Context ct) {
        TelephonyManager tMgr = (TelephonyManager) ct.getSystemService(Context.TELEPHONY_SERVICE);
        return tMgr.getLine1Number();

    }

    public static String getVersionName(Context ct) {
        PackageManager manager = ct.getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(ct.getPackageName(), 0);
            return info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

   /* public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }*/


    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isAcceptingText())
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void setupUI(View view, final Context ct) {

        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard((Activity) ct);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView, ct);
            }
        }
    }

    public static String getTotalRAM() {

        RandomAccessFile reader = null;
        String load = null;
        DecimalFormat twoDecimalForm = new DecimalFormat("#.##");
        double totRam = 0;
        String lastValue = "";
        try {
            reader = new RandomAccessFile("/proc/meminfo", "r");
            load = reader.readLine();

            // Get the Number value from the string
            Pattern p = Pattern.compile("(\\d+)");
            Matcher m = p.matcher(load);
            String value = "";
            while (m.find()) {
                value = m.group(1);
                // System.out.println("Ram : " + value);
            }
            reader.close();

            totRam = Double.parseDouble(value);
            // totRam = totRam / 1024;

            double mb = totRam / 1024.0;
            double gb = totRam / 1048576.0;
            double tb = totRam / 1073741824.0;

            if (tb > 1) {
                lastValue = twoDecimalForm.format(tb).concat(" TB");
            } else if (gb > 1) {
                lastValue = twoDecimalForm.format(gb).concat(" GB");
            } else if (mb > 1) {
                lastValue = twoDecimalForm.format(mb).concat(" MB");
            } else {
                lastValue = twoDecimalForm.format(totRam).concat(" KB");
            }


        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            // Streams.close(reader);
        }

        return lastValue;
    }

    public static String getDeviceName() {
      /*  String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }*/

        return capitalize(Build.MANUFACTURER);
    }


    public static String getDeviceModel() {
      /*  String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }*/

        return capitalize(Build.MODEL);
    }


    public static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

   /* public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElement s();) {
                NetworkInterface intf = en.nextElement ();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElement s();) {
                    InetAddress inetAddress = enumIpAddr.nextElement ();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i(TAG, "***** IP="+ ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e(TAG, ex.toString());
        }
        return null;
    }*/


    public static String getIPAddress(Context ct) {
        if (checkWifiConnection(ct)) {
            WifiManager wm = (WifiManager) ct.getSystemService(Context.WIFI_SERVICE);
            return Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());

        } else {
            return "";
        }
    }

    public static String getMACAddress(Context ct) {
        if (checkWifiConnection(ct)) {
            WifiManager wifiManager = (WifiManager) ct.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wInfo = wifiManager.getConnectionInfo();
            return wInfo.getMacAddress();
        } else {
            return "";
        }
    }

    public static String getValue(EditText et) {
        return et.getText().toString();
    }

    public static boolean isEmpty(EditText et) {
        return et.getText().toString().equalsIgnoreCase("");
    }



   /* public static void showProgressDialog(Context context) {

        try {
            if (dialog == null) {
                dialog = new Dialog(context, R.style.AlertDialog);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.custom_progressbar);


                progressView = (CircularProgressView) dialog.findViewById(R.id.progress_view);

                // Test the listener with logcat messages
                progressView.addListener(new CircularProgressViewAdapter() {
                    @Override
                    public void onProgressUpdate(float currentProgress) {
                        Log.d("CPV", "onProgressUpdate: " + currentProgress);
                    }

                    @Override
                    public void onProgressUpdateEnd(float currentProgress) {
                        Log.d("CPV", "onProgressUpdateEnd: " + currentProgress);
                    }

                    @Override
                    public void onAnimationReset() {
                        Log.d("CPV", "onAnimationReset");
                    }

                    @Override
                    public void onModeChanged(boolean isIndeterminate) {
                        Log.d("CPV", "onModeChanged: " + (isIndeterminate ? "indeterminate" : "determinate"));
                    }
                });

                // Test loading animation
                startAnimationThreadStuff(1000);
            }
            if (!dialog.isShowing())
                dialog.show();
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/



    public void sendError() {

    }

    public static Thread updateThread;
    public static Runnable myRun;
    public static Handler handler = new Handler();

  /*  public static void startAnimationThreadStuff(long delay) {
        if (updateThread != null && updateThread.isAlive())
            updateThread.interrupt();


        myRun = new Runnable() {
            @Override
            public void run() {
                if (!progressView.isIndeterminate()) {
                    progressView.setProgress(0f);
                    // Run thread to update progress every quarter second until full
                    updateThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            while (progressView.getProgress() < progressView.getMaxProgress() && !Thread.interrupted()) {
                                // Must set progress in UI thread
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressView.setProgress(progressView.getProgress() + 10);
                                    }
                                });
                                SystemClock.sleep(250);
                            }
                        }
                    });
                    updateThread.start();
                }
                // Alias for resetAnimation, it's all the same
                progressView.startAnimation();
            }
        };
        // Start animation after a delay so there's no missed frames while the app loads up
        handler.postDelayed(myRun, delay);

    }*/
    public static void showProgressDialog(Context context, String msg) {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.cancel();
            }
            if (dialog == null) {
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);


                dialog.setContentView(R.layout.custom_progress_dialog);
                TextView txtView = (TextView) dialog.findViewById(R.id.textView);
                txtView.setText(msg);


            }


            if (!dialog.isShowing()) {
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        dialog.show();
                    }
                };
                r.run();
            }

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showProgressDialog(Context context) {
        if (!((Activity) context).isFinishing()) {
            //show dialog

            showProgressDialog(context, "Please Wait..");




        }
    }
    public static void hideProgressDialog() {
        try {

            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                    dialog = null;
                }
            }

/*
            if (mProgressDialog != null && mProgressDialog.isShowing() ) {
                mProgressDialog.cancel();
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }*/
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
   /* public static void hideProgressDialog() {
        try {
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                    handler.removeCallbacks(myRun);
                    dialog = null;
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public static String getUpperCase(String name) {
        StringBuilder sb = new StringBuilder(name); // one StringBuilder object
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        return sb.toString(); // one String object
    }

    public static void addValueToEditor(SharedPreferences.Editor et, Object o) {
        Class c;
        try {
            c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());

            Field[] fields = c.getFields();
            for (Field f : fields) {
                f.setAccessible(true);
                if (f.getType() == String.class) {
                    et.putString(f.getName(), f.get(o).toString());

                } else if (f.getType() == Integer.class) {
                    et.putInt(f.getName(), Integer.parseInt(f.get(o).toString()));

                } else if (f.getType() == Boolean.class) {
                    et.putBoolean(f.getName(), Boolean.valueOf(f.get(o).toString()));
                }
            }

            et.commit();


        } catch (Exception e) {
            e.printStackTrace();


        }

    }

    public static void showCancelableSpinProgressDialog(Context context, String message) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    public static void removeSimpleSpinProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.cancel();
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public static float getDisplayMetricsDensity(Context context) {
        density = context.getResources().getDisplayMetrics().density;

        return density;
    }

    public static int getPixel(Context context, int p) {
        if (density != 1) {
            return (int) (p * density + 0.5);
        }
        return p;
    }

    public static Animation FadeAnimation(float nFromFade, float nToFade) {
        Animation fadeAnimation = new AlphaAnimation(nToFade, nToFade);

        return fadeAnimation;
    }

    public static String getDeviceId(Context context) {
        TelephonyManager tManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String uuid = tManager.getDeviceId();
        return uuid;
    }

    public static Animation inFromRightAnimation() {
        Animation inFromRight = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                0.0f);

        return inFromRight;
    }

    public static Animation inFromLeftAnimation() {
        Animation inFromLeft = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                0.0f);

        return inFromLeft;
    }

    public static Animation inFromBottomAnimation() {
        Animation inFromBottom = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, +1.0f, Animation.RELATIVE_TO_PARENT,
                0.0f);

        return inFromBottom;
    }

    public static Animation outToLeftAnimation() {
        Animation outToLeft = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                -1.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f);
        return outToLeft;
    }

    public static Animation outToRightAnimation() {
        Animation outToRight = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                +1.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f);

        return outToRight;
    }

    public static Animation outToBottomAnimation() {
        Animation outToBottom = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                +1.0f);

        return outToBottom;
    }

    public static String create_imgage_view(String imageURL, int width, int height) {
        StringBuffer html = new StringBuffer(
                "<html lang='en'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head>");
        html.append("<body style='width:" + width + "; height:" + height + "; padding:0; margin:0;'>");

        html.append("<div style='width:" + width + "; height:" + height + "; overflow:hidden;'>");
        html.append("<img src='" + imageURL + "' style='width:" + width + ";' border=0 />");
        html.append("</div>");

        html.append("</body>");
        html.append("</html>");

        return html.toString();
    }

    // public static String convert24HoursTo12HoursFormat(String twelveHourTime)
    // throws ParseException {
    // return outputformatter.format(inputformatter.parse(twelveHourTime));
    // }

    public static boolean isNetworkAvailable(Context activity) {
        ConnectivityManager connectivity = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            showToast("Please connect to Internet", activity);
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        showToast("Please connect to Internet", activity);
        return false;
    }


    public static boolean checkWifiConnection(Context ct) {
        ConnectivityManager connManager = (ConnectivityManager) ct.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return mWifi.isConnected();
    }

    public static void showToast(String msg, Context ctx) {

       Toast toast = Toast.makeText(ctx, msg, Toast.LENGTH_LONG);
       toast.show();
       toast.setGravity(Gravity.CENTER, 0, 0);


     /*   new AlertDialog.Builder(ctx)
                .setMessage(msg)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
*/

    }


    public static void showToastAlert(String msg, Context ct) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ct);

        if (msg.contains("Please") || msg.contains("Sorry")) {
            alertDialogBuilder.setTitle(Html.fromHtml("<font color=\"red\">Warning</font>"));
        }
        alertDialogBuilder.setMessage(msg);
        // set positive button: Yes message
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                dialog.dismiss();
            }
        });
        // set negative button: No message

        // set neutral button: Exit the app message


        AlertDialog alertDialog = alertDialogBuilder.create();
        // show alert
        alertDialog.show();
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static boolean Passwordvalidate(final String password){

        return PASSWORD_PATTERN.matches(password);
    }

    public static String getIMEInumber(Context ct) {
        try {
            TelephonyManager mngr = (TelephonyManager) ct.getSystemService(Context.TELEPHONY_SERVICE);
            return mngr.getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    /**
     * You can integrate interstitials in any placement in your app, but for
     * testing purposes we present integration tied to a button click
     */
    public static Typeface setNormalFontFace(Context context) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/DroidSans.ttf");
        return font;
    }

    public static Typeface setBoldFontFace(Context context) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/DroidSans-Bold.ttf");
        return font;
    }

    public static View setTabCaption() {

        return null;
    }

    public static String manageDecimal(String temp) {
        String displayRiskDecimal = null, before = null, after = null;
        if (temp.contains(".")) {
            int in = temp.indexOf(".") + 1;
            before = temp.substring(0, in);
            after = temp.substring(in, temp.length());
            if (after.length() == 1) {
                displayRiskDecimal = before + after + "0";
            } else {
                displayRiskDecimal = before + after;
            }
            return displayRiskDecimal;
        } else {
            displayRiskDecimal = temp + ".00";
            return displayRiskDecimal;
        }
    }

    public static String manageDecimalPointForGetLines(String temp) {
        String displayRiskDecimal = null, before = null, after = null;
        int in = temp.indexOf(".") + 1;
        before = temp.substring(0, in);
        after = temp.substring(in, temp.length());
        if (after.equals("00")) {
            displayRiskDecimal = before.substring(0, before.length() - 1);
        } else if (after.length() == 2) {
            if (after.startsWith("0", 1)) {
                displayRiskDecimal = after.substring(0, after.length() - 1);
                displayRiskDecimal = before + displayRiskDecimal;
            }
        }
        return displayRiskDecimal;
    }

    public static String manageSign(String temp) {
        String displayRiskDecimal = null;
        if (temp.contains("-") || temp.contains("+")) {
            displayRiskDecimal = temp;
        } else {
            displayRiskDecimal = "+" + temp;
        }
        return displayRiskDecimal;
    }

    public static SpannableStringBuilder displayNameValue(String battingTeamName, String displayBaseValue) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString WordtoSpan = new SpannableString(displayBaseValue);
        WordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#1298DE")), 0, displayBaseValue.length(), 0);
        builder.append(battingTeamName);
        builder.append(WordtoSpan);
        return builder;
    }

    private static String convertStreamToString(InputStream is) {
        /*
         * To convert the InputStream to String we use the
		 * BufferedReader.readLine() method. We iterate until the BufferedReader
		 * return null which means there's no more data to read. Each line will
		 * appended to a StringBuilder and returned as String.
		 */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    public static String removeSpecialChar(String value) {
        try {
           /* if (value.contains("("))
                value = value.replaceAll("'(", "");
            if (value.contains(")"))
                value = value.replaceAll(")", "");*/
            if (value.contains("-"))
                value = value.replaceAll("-", "");
            if (value.contains(" "))
                value = value.replaceAll(" ", "");
            if (value.contains("."))
                value = value.replaceAll(".", "");
            if (value.contains(":"))
                value = value.replaceAll(":", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }


    public static void hideKeyboard(Activity context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static void setDyanamicHight(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static String getcurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("KK:mm a");
        return dateFormat.format(new Date());
    }

    public static String getCurrentDateInYYYYMMDD() {

        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static String getCurrentDateInDDMMYYYY() {

        return new SimpleDateFormat("dd-MM-yyyy").format(new Date());
    }

    public static String getNextDateInYYYYMMDD() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        return new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
    }

    public static int getCurrentOffsetInGMT() {
        TimeZone tz = TimeZone.getDefault();
        Date now = new Date();
        double offsetFromUtc = (tz.getOffset(now.getTime()) / 1000) / 60;
        return (int) offsetFromUtc;
    }

    public static String findDeviceID(Context cnt) {
        return Secure.getString(cnt.getContentResolver(), Secure.ANDROID_ID);
    }

    public static Typeface getTypfeFace(Context c, String assetPath) {
        synchronized (cache) {
            if (!cache.containsKey(assetPath)) {
                try {
                    Typeface t = Typeface.createFromAsset(c.getAssets(),
                            assetPath);
                    cache.put(assetPath, t);
                } catch (Exception e) {
                    Log.e(TAG, "Could not get typeface '" + assetPath
                            + "' because " + e.getMessage());
                    return null;
                }
            }
            return cache.get(assetPath);
        }
    }


    public static StateListDrawable getSelectorDrawable(Context ct, int pressed, int normal) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_pressed},
                ct.getResources().getDrawable(pressed));
        states.addState(new int[]{android.R.attr.state_focused},
                ct.getResources().getDrawable(pressed));
        states.addState(new int[]{},
                ct.getResources().getDrawable(normal));

        return states;

    }

    public static StateListDrawable getSelectorDrawableForCheckbox(Context ct, int pressed, int normal) {
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{android.R.attr.state_checked},
                ct.getResources().getDrawable(pressed));
        states.addState(new int[]{android.R.attr.state_activated},
                ct.getResources().getDrawable(pressed));

        states.addState(new int[]{},
                ct.getResources().getDrawable(normal));

        return states;

    }
    public static void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods


        Crashlytics.setUserIdentifier("Khateeb Classes");
        Crashlytics.setUserEmail("jkwebmails@gmail.com");
        Crashlytics.setUserName("Khateeb Classes");
    }
}
