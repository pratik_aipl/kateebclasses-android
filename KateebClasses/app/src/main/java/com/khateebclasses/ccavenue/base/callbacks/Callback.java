package com.khateebclasses.ccavenue.base.callbacks;

public interface Callback {

    void onPaymentDeclined();

    void onPaymentSuccess(String transactionID);

    void onPaymentAborted();

    void onPaymentAbortedByUser();

    void onPaymentUnknown();
}