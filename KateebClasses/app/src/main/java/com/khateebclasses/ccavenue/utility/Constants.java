package com.khateebclasses.ccavenue.utility;


public class Constants {
    public static final String PARAMETER_SEP = "&";
    public static final String PARAMETER_EQUALS = "=";
    public static final String TRANS_URL = "https://secure.ccavenue.com/transaction/initTrans";//"https://secure.ccavenue.com/transaction/transaction.do?";//"https://secure.ccavenue.com/transaction/initTrans";

    public enum REQUESTS {
        getRSA

    }

    public static final String SOAP_ACTION = "SOAP_ACTION", METHOD_NAME = "METHOD_NAME";

    public enum POST_TYPE {
        GET, POST, POST_WITH_IMAGE, Soap, POST_WITH_JSON, POST_NEW;
    }
}
