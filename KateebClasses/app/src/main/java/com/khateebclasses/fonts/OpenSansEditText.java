package com.khateebclasses.fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by empiere-php on 10/31/2017.
 */

@SuppressLint("AppCompatCustomView")
public class OpenSansEditText extends EditText {

    public OpenSansEditText(Context context) {
        super(context);
        this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/OpenSans-Regular.ttf")));
        this.setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    public OpenSansEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/OpenSans-Regular.ttf")));
        this.setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    public OpenSansEditText(Context context, AttributeSet attrs, int defStyle) {

        super(context, attrs, defStyle);
        this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/OpenSans-Regular.ttf")));
        this.setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);

    }


}
